# Replaces: all query

SET time_zone = '-7:00';

DROP TABLE IF EXISTS `rpt_paid_transactions`;

CREATE TABLE rpt_paid_transactions 
(
  id int(10) NOT NULL AUTO_INCREMENT COMMENT 'Unique id',
  user_id int(10) unsigned NOT NULL COMMENT 'drupal user id',
  mail varchar(254) NOT NULL COMMENT 'email',
  order_id int(10) unsigned NOT NULL COMMENT 'order id',
  create_date date NOT NULL COMMENT 'date created',
  create_year int(4) NOT NULL COMMENT 'created year',
  create_month int(2) NOT NULL COMMENT 'created month',
  create_day int(2) NOT NULL COMMENT 'creatd day',
  graduated varchar(13) DEFAULT '0' COMMENT 'graduation date',
  grad_date date NOT NULL COMMENT 'date created',
  grad_year int(4) NOT NULL COMMENT 'created year',
  grad_month int(2) NOT NULL COMMENT 'created year',
  country varchar(44),
  state varchar(50),
  st varchar(4) COMMENT 'Abbr State Name',
  market varchar(50) COMMENT 'East , West',
  region varchar(50) COMMENT 'N East, S West',
  zip varchar(50),
  ship_state varchar(50) COMMENT 'Shipping state',
  ship_zip varchar(50) COMMENT 'Shipping zipcode',
  partner_nid int(15) COMMENT 'Partner NID',
  partner_title varchar(255) COMMENT 'Firm Name or group that it can be contributed',
  intacct varchar(255) COMMENT 'Customer number for tax data related to orders',
  college_state_tid int(10) unsigned COMMENT 'college taononomy tid',
  college_name varchar(200) COMMENT 'college name based on tid',
  college_state_abbrv varchar(10) COMMENT 'college state name abbreviated CA, CT',
  college_state varchar(200) COMMENT 'college state full name',
  promotion_type varchar(255) COMMENT 'no promo, coupon, discount',
  promotion varchar(255) COMMENT 'code used for promo',
  promo_description longtext COMMENT 'description of promo',
  free_trial varchar(11) COMMENT 'marks if it is a FT Order',
  ft_time_diff bigint(21) COMMENT 'diff in times',
  ft_time_group varchar(10) COMMENT 'time group',
  channel varchar(50) COMMENT 'channel like B2B, B2C',
  channel3 varchar(50) COMMENT 'channel like B2B, B2C',
  lead_status_all varchar(50) COMMENT 'lead status all: FT_Lead, FSL_Lead',
  package varchar(25) COMMENT 'package type',
  payment_method varchar(25) COMMENT 'payment method',
  sub_total decimal(15,2) NOT NULL COMMENT 'order sub-total',
  promo_total decimal(15,2) NOT NULL COMMENT 'order promo total',
  partner_discount_total decimal(15,2) NOT NULL COMMENT 'order partner discount total',
  shipping decimal(15,2) NOT NULL COMMENT 'order shipping',
  taxes decimal(15,2) NOT NULL COMMENT 'order taxes',
  order_total decimal(15,2) NOT NULL COMMENT 'order total',

  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`),
  KEY `user_id` (`user_id`),
  KEY `order_id` (`order_id`),
  KEY `create_date` (`create_date`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 COMMENT='all order transactions';

INSERT INTO rpt_paid_transactions (
user_id,
mail,
order_id,
create_date,
create_year,
create_month,
create_day,
graduated,
grad_date,
grad_year,
grad_month,
country,
state,
st,
market,
region,
zip,
ship_state,
ship_zip,
partner_nid,
partner_title,
intacct,
college_state_tid,
college_name,
college_state_abbrv,
college_state,
promotion_type,
promotion,
promo_description,
free_trial,
ft_time_diff,
ft_time_group,
channel,
channel3,
lead_status_all,
package,
payment_method,
sub_total,
promo_total,
partner_discount_total,
shipping,
taxes,
order_total
)
SELECT
  u.uid AS user_id,
  u.mail,
  o.order_id AS order_id,
  FROM_UNIXTIME(trans.changed, '%Y-%m-%d') AS create_date,
  YEAR(FROM_UNIXTIME(trans.changed)) AS create_year,
  MONTH(FROM_UNIXTIME(trans.changed)) AS create_month,
  DAY(FROM_UNIXTIME(trans.changed)) AS create_day,
  IF(field_did_you_graduate_college_value = 1, "Graduated", "Not Graduated") AS graduated,
  DATE(graddate.field_graduation_month_and_year_value) as grad_date,
  YEAR(graddate.field_graduation_month_and_year_value) as grad_year,
  MONTH(graddate.field_graduation_month_and_year_value) as grad_month,
  country_olaf.COUNTRY AS country,
  IF(state_olaf.State is null, 'International', state_olaf.State) AS state,
  IF(state_olaf.State is null, 'Intl', state_olaf.`State Abbrev`) AS st,
  IF(territory_olaf.Market is null, "International", territory_olaf.Market) AS market,
  IF(territory_olaf.Region is null, "International", territory_olaf.Region) AS region,
  address.commerce_customer_address_postal_code AS zip,
  IF(shipstate.State is null, 'International', shipstate.State) AS ship_state,
  shipaddress.commerce_customer_address_postal_code AS ship_zip,
  partner.nid AS partner_nid,
  partner.title AS partner_title,
  field_intacct_customer_id_value AS intacct,
  cs.field_college_state_tid AS college_state_tid,
  IF(tdaa.taxonomy_term_data_name is null, fcsd.field_college_name_value, tdaa.taxonomy_term_data_name) AS university,
  csl.field_college_state_list_value as college_st,
  so.State as college_state,
  #IF(line_total.commerce_total_amount/100 > 0, (trans.amount - line_total.commerce_total_amount)/100,trans.amount/100)  AS total_amount2, #is Net Amount
  IF(cct.label is null, "No Promotion", cct.label) AS promotion_type,
  IF(fdccc.commerce_coupon_code_value is null, "No Promotion", fdccc.commerce_coupon_code_value) AS promotion,
  IF(fdfd.field_description_value is null, "", fdfd.field_description_value) AS promo_description,
  IF(ft2p.user_id = o.uid,o.uid,"") AS free_trial,
  ft2p.ft_time_diff,
  ft2p.ft_time_group,
  CASE concat_ws('-', pt.field_partner_type_value, trans.payment_method, pbt.field_billing_type_value)
    when ('1-rcpar_partners_payment_freetrial-freetrial') then 'B2B'
    when ('1-rcpar_partners_payment-freetrial') then 'B2B'
    when ('1-rcpar_partners_payment-directbill') then 'B2B'
    when ('1-authnet_aim-creditcard') then 'B2B'
    when ('1-rcpar_partners_payment-creditcard') then 'B2B'
    when ('1-rcpar_partners_payment_affirm') then 'B2B'
    when ('1-affirm-creditcard') then 'B2B'
    when ('2-authnet_aim-freetrial') then 'B2C'
    when ('2-rcpar_partners_payment_freetrial-freetrial') then 'B2C'
    when ('2-authnet_aim-freetrial') then 'B2C'
    when ('3-rcpar_partners_payment_freetrial-freetrial') then 'B2B'
    when ('3-rcpar_partners_payment-freetrial') then 'B2B'
    when ('3-rcpar_partners_payment-directbill') then 'B2B'
    when ('3-authnet_aim-creditcard') then 'B2B'
    when ('3-rcpar_partners_payment-creditcard') then 'B2B'
    when ('3-rcpar_partners_payment_affirm') then 'B2B'
    when ('3-affirm-creditcard') then 'B2B'
    when ('3-rcpar_partners_payment_prepaid_debit-prepaid') then 'B2B PPD DB'
    when ('3-rcpar_partners_payment_prepaid_credit-prepaid') then 'B2B PPD CR'
    when ('4-rcpar_partners_payment_freetrial-freetrial') then 'B2C'
    when ('4-rcpar_partners_payment_freetrial') then 'B2C'
    when ('4-rcpar_partners_payment-directbill') then 'B2C'
    when ('4-authnet_aim-creditcard') then 'B2C'
    when ('4-rcpar_partners_payment-creditcard') then 'B2C'
    when ('4-rcpar_partners_payment_affirm') then 'B2C'
    else 'B2C'
  END AS channel,
  CASE concat_ws('-', pt.field_partner_type_value, trans.payment_method, pbt.field_billing_type_value)
    when ('1-rcpar_partners_payment_freetrial-freetrial') then 'B2B'
    when ('1-rcpar_partners_payment-freetrial') then 'B2B'
    when ('1-rcpar_partners_payment-directbill') then 'B2B'
    when ('1-authnet_aim-creditcard') then 'B2B'
    when ('1-rcpar_partners_payment-creditcard') then 'B2B'
    when ('1-rcpar_partners_payment_affirm') then 'B2B'
    when ('1-affirm-creditcard') then 'B2B'
    when ('2-authnet_aim-freetrial') then 'B2C'
    when ('2-rcpar_partners_payment_freetrial-freetrial') then 'B2C'
    when ('2-authnet_aim-freetrial') then 'B2C'
    when ('3-rcpar_partners_payment_freetrial-freetrial') then 'B2B'
    when ('3-rcpar_partners_payment-freetrial') then 'B2B'
    when ('3-rcpar_partners_payment-directbill') then 'B2B'
    when ('3-authnet_aim-creditcard') then 'B2B'
    when ('3-rcpar_partners_payment-creditcard') then 'B2B'
    when ('3-rcpar_partners_payment_affirm') then 'B2B'
    when ('3-affirm-creditcard') then 'B2B'
    when ('3-rcpar_partners_payment_prepaid_debit-prepaid') then 'B2B'
    when ('3-rcpar_partners_payment_prepaid_credit-prepaid') then 'B2B'
    when ('4-rcpar_partners_payment_freetrial-freetrial') then 'B2B'
    when ('4-rcpar_partners_payment_freetrial') then 'B2C'
    when ('4-rcpar_partners_payment-directbill') then 'B2C'
    when ('4-authnet_aim-creditcard') then 'B2C'
    when ('4-rcpar_partners_payment-creditcard') then 'B2C'
    when ('4-rcpar_partners_payment_affirm') then 'B2C'
    else 'B2C'
  END AS channel3,
  CASE concat_ws('-', pt.field_partner_type_value, trans.payment_method, pbt.field_billing_type_value) 
   when ('1-rcpar_partners_payment_freetrial-freetrial') then 'FSL Lead'
   when ('1-rcpar_partners_payment-freetrial') then 'FSL Lead'
   when ('1-rcpar_partners_payment-directbill') then 'FDB Lead'
   when ('1-authnet_aim-creditcard') then 'FCC Lead'
   when ('1-rcpar_partners_payment-creditcard') then 'FCC Lead'
   when ('1-rcpar_partners_payment_affirm') then 'FFIN Lead'
   when ('1-affirm-creditcard') then 'FFIN Lead'
   when ('2-authnet_aim-freetrial') then 'FreeTrial Lead'
   when ('2-rcpar_partners_payment_freetrial-freetrial') then 'FreeTrial Lead'
   when ('2-authnet_aim-freetrial') then 'FreeTrial Lead'
   when ('3-rcpar_partners_payment_freetrial-freetrial') then 'ASL Lead'
   when ('3-rcpar_partners_payment-freetrial') then 'ASL Lead'
   when ('3-rcpar_partners_payment-directbill') then 'ADB Lead'
   when ('3-authnet_aim-creditcard') then 'ACC Lead'
   when ('3-rcpar_partners_payment-creditcard') then 'ACC Lead'
   when ('3-rcpar_partners_payment_affirm') then 'AFIN Lead'
   when ('3-affirm-creditcard') then 'AFIN Lead'
   when ('4-rcpar_partners_payment_freetrial-freetrial') then 'PAL Lead'
   when ('4-rcpar_partners_payment_freetrial') then 'PAL Lead'
   when ('4-rcpar_partners_payment-directbill') then 'PDB Lead'
   when ('4-authnet_aim-creditcard') then 'PCC Lead'
   when ('4-rcpar_partners_payment-creditcard') then 'PCC Lead'
   when ('4-rcpar_partners_payment_affirm') then 'PFIN Lead'
   ELSE 'Organic'
  END AS lead_status_all,
  CASE 
   WHEN (package_line.line_item_label = 'FULL-ELITE-DIS') then 'Elite'
   WHEN (package_line.line_item_label = 'FULL-PREM-DIS') then 'Premier' 
   WHEN (package_line.line_item_label = 'FULL-PREM-EFC-DIS') then 'Premier' 
   WHEN (package_line.line_item_label = 'FULL-PREM-AUD-DIS') then 'Premier' 
   WHEN (package_line.line_item_label = 'FULL-DIS') then 'Select'
   WHEN (package_line_singles.line_item_label IN ('AUD','BEC','FAR','REG','AUD-CRAM','BEC-CRAM','FAR-CRAM','REG-CRAM')) then 'Individual'
   ELSE 'Other'
  END AS package,
  CASE
    WHEN (trans.payment_method = 'authnet_aim') then 'CC'
    WHEN (trans.payment_method = 'rcpar_partners_payment') then 'DB'
    WHEN (trans.payment_method = 'affirm') then 'Affirm'
    WHEN (trans.payment_method = 'rcpar_pay_by_check') then 'Pay by Check'
    WHEN (trans.payment_method = 'rcpar_partners_payment_prepaid_credit') then 'PPD CR'
    WHEN (trans.payment_method = 'rcpar_partners_payment_prepaid_debit') then 'PPD DR'
    ELSE ''
  END as `payment_method`,
  ROUND((ot.commerce_order_total_amount - shipping_line_total.commerce_total_amount - tax_line_total.commerce_total_amount)/100, 2) AS sub_total,
  ROUND(promo_line_total.commerce_total_amount/100, 2) as promo_total,
  ROUND(pdisc_line_total.commerce_total_amount/100, 2) as partner_discount_total,
  ROUND(shipping_line_total.commerce_total_amount/100, 2) as shipping,
  ROUND(tax_line_total.commerce_total_amount/100, 2) as taxes,
  ROUND(ot.commerce_order_total_amount/100, 2) AS order_total

FROM commerce_order o

INNER JOIN field_data_commerce_customer_billing billing_prof ON o.order_id = billing_prof.entity_id AND billing_prof.entity_type = 'commerce_order'
INNER JOIN commerce_customer_profile prof ON prof.profile_id = billing_prof.commerce_customer_billing_profile_id AND prof.type = 'billing'
LEFT JOIN users u ON prof.uid = u.uid
LEFT JOIN field_data_commerce_customer_address address ON address.entity_id = prof.profile_id AND address.entity_type = 'commerce_customer_profile'

INNER JOIN field_data_commerce_customer_shipping shipping_prof ON o.order_id = shipping_prof.entity_id AND shipping_prof.entity_type = 'commerce_order'
INNER JOIN commerce_customer_profile sprof ON sprof.profile_id = shipping_prof.commerce_customer_shipping_profile_id AND sprof.type = 'shipping'
LEFT JOIN field_data_commerce_customer_address shipaddress ON shipaddress.entity_id = sprof.profile_id AND shipaddress.entity_type = 'commerce_customer_profile'

LEFT JOIN commerce_payment_transaction trans ON trans.order_id = o.order_id
LEFT JOIN field_data_field_did_you_graduate_college grad ON grad.entity_type = 'user' AND grad.entity_id = u.uid
LEFT JOIN field_data_field_graduation_month_and_year graddate ON graddate.entity_id = o.uid

LEFT JOIN field_data_commerce_order_total ot ON ot.entity_id = o.order_id
LEFT JOIN commerce_line_item package_line ON package_line.order_id = o.order_id AND package_line.type = 'product' AND package_line.line_item_label IN ('FULL-DIS','FULL-PREM-DIS','FULL-ELITE-DIS','FULL-PREM-EFC-DIS','FULL-PREM-AUD-DIS')
LEFT JOIN commerce_line_item package_line_singles ON package_line_singles.order_id = o.order_id AND package_line_singles.type = 'product' AND package_line_singles.line_item_label NOT IN ('FULL-DIS','FULL-PREM-DIS','FULL-ELITE-DIS','FULL-PREM-EFC-DIS','FULL-PREM-AUD-DIS') AND package_line_singles.line_item_label IN ('AUD','BEC','FAR','REG','AUD-CRAM','BEC-CRAM','FAR-CRAM','REG-CRAM')

LEFT JOIN commerce_line_item tax_line ON tax_line.line_item_id = (SELECT line_item_id FROM commerce_line_item temp_li WHERE temp_li.order_id = trans.order_id AND temp_li.type = 'avatax' order by line_item_id desc LIMIT 1)
LEFT JOIN field_data_commerce_total tax_line_total ON tax_line_total.entity_type = 'commerce_line_item' AND tax_line_total.entity_id = tax_line.line_item_id

LEFT JOIN commerce_line_item shipping_line ON shipping_line.order_id = o.order_id AND shipping_line.type = 'shipping'
LEFT JOIN field_data_commerce_total shipping_line_total ON shipping_line_total.entity_type = 'commerce_line_item' AND shipping_line_total.entity_id = shipping_line.line_item_id

LEFT JOIN commerce_line_item promo_line ON promo_line.line_item_id = (SELECT line_item_id FROM commerce_line_item temp_li WHERE temp_li.order_id = trans.order_id AND temp_li.type = 'commerce_coupon' order by line_item_id desc LIMIT 1)
LEFT JOIN field_data_commerce_total promo_line_total ON promo_line_total.entity_type = 'commerce_line_item' AND promo_line_total.entity_id = promo_line.line_item_id
                                                                      
LEFT JOIN commerce_line_item pdisc ON pdisc.line_item_id = (SELECT line_item_id FROM commerce_line_item temp_li WHERE temp_li.order_id = trans.order_id AND temp_li.type = 'product' AND temp_li.line_item_label = 'PARTNER-DIS' order by line_item_id desc LIMIT 1)
LEFT JOIN field_data_commerce_total pdisc_line_total ON pdisc_line_total.entity_type = 'commerce_line_item' AND pdisc_line_total.entity_id = pdisc.line_item_id

LEFT JOIN field_data_commerce_total line_total ON line_total.entity_type = 'commerce_line_item' AND line_total.entity_id = tax_line.line_item_id
LEFT JOIN country_olaf ON address.commerce_customer_address_country = country_olaf.`A2 (ISO)`
LEFT JOIN state_olaf ON address.commerce_customer_address_administrative_area = state_olaf.`State Abbrev`
LEFT JOIN state_olaf shipstate ON shipaddress.commerce_customer_address_administrative_area = shipstate.`State Abbrev`
LEFT JOIN territory_olaf ON address.commerce_customer_address_administrative_area = territory_olaf.State

LEFT JOIN field_data_field_partner_profile partner_ref ON partner_ref.entity_id = o.order_id AND partner_ref.entity_type = 'commerce_order'
LEFT JOIN node partner ON partner.nid = partner_ref.field_partner_profile_target_id
LEFT JOIN field_data_field_intacct_customer_id intacct ON partner.nid = intacct.entity_id AND intacct.entity_type = 'node'
LEFT JOIN field_data_field_partner_type pt ON pt.entity_id = partner_ref.field_partner_profile_target_id AND pt.entity_type = 'node'
LEFT JOIN field_data_field_billing_type pbt ON pbt.entity_id = partner_ref.field_partner_profile_target_id

INNER JOIN field_data_commerce_order_total order_total ON order_total.entity_id = o.order_id AND order_total.entity_type = 'commerce_order'

LEFT JOIN field_data_commerce_coupon_order_reference fdccor ON fdccor.entity_id = o.order_id
LEFT JOIN field_data_commerce_coupon_code fdccc ON fdccc.entity_id = fdccor.commerce_coupon_order_reference_target_id
LEFT JOIN commerce_coupon_type cct ON cct.`type` = fdccc.bundle
LEFT JOIN field_data_field_description fdfd ON fdccc.entity_id = fdfd.entity_id

LEFT JOIN rpt_free_trial_2purchase ft2p ON ft2p.user_id = o.uid

LEFT JOIN field_data_field_college_name fcsd ON u.uid = fcsd.entity_id AND fcsd.entity_type = 'user'

LEFT JOIN field_data_field_when_do_you_plan_to_start s ON o.uid = s.entity_id
LEFT JOIN field_data_field_college_state cs ON cs.entity_id = s.entity_id
LEFT JOIN field_data_field_college_state_list csl ON csl.entity_id = u.uid
LEFT JOIN state_olaf so ON csl.field_college_state_list_value = so.`State Abbrev`
LEFT JOIN (
  SELECT taxonomy_term_data.name AS taxonomy_term_data_name, taxonomy_term_data.vid AS taxonomy_term_data_vid, taxonomy_term_data.tid AS tid, taxonomy_vocabulary.machine_name AS taxonomy_vocabulary_machine_name, 'taxonomy_term' AS field_data_field_fice_code_taxonomy_term_entity_type
  FROM taxonomy_term_data taxonomy_term_data
  LEFT JOIN taxonomy_vocabulary taxonomy_vocabulary ON taxonomy_term_data.vid = taxonomy_vocabulary.vid
    WHERE taxonomy_vocabulary.machine_name IN  ('state_college_selection')
  ) tdaa ON (tdaa.tid = cs.field_college_state_tid)

WHERE trans.status = 'Success' AND
  trans.payment_method <> 'rcpar_partners_payment_freetrial' AND
  o.status IN ('Pending', 'Processing','completed', 'shipped') AND
  u.uid NOT IN (157631,0,2625,4035,77631,175056,34289,176716,24768,114306,114521,1,37,392,406,649,650,878,899,925,1159,2023,2024,2025,2026,2027,2034,2035,2808,2930,3294,3295,3296,3297,3443,3893,4780,4942,5173,5752,5783,6533,7159,7270,7742,8063,8065,8707,8710,8809,9283,9432,9433,9434,9435,9436,9437,9438,10160,10190,10685,10926,11125,11127,11425,11866,12743,13001,13017,13167,13270,13271,13774,13922,14146,14252,14373,14796,14945,15051,15053,15236,15405,15610,15734,15913,16269,16312,16807,16924,16925,16926,16927,16928,16951,16952,16953,16954,16957,16962,16969,16990,16991,16995,16996,16997,16998,16999,17022,17023,17024,17025,17154,17246,17511,17793,17797,17815,18525,18654,18990,20092,20176,20179,20336,20337,20566,21618,21619,21632,21837,21848,21882,22384,22599,22781,23022,23101,23388,23497,23795,23805,23975,24133,25182,25550,25829,27559,28576,28714,29092,29264,29266,30749,30763,31026,31156,32178,33317,33739,35046,35096,35367,36142,36199,36567,37896,39777,43941,44035,46356,48095,48392,50017,50264,50595,51514,51923,53289,53732,57191,58966,63721,64671,64981,67611,75811,78921,82881,85196,95026,95941,112051,114241,116281,117671,120716,123781,126581,131221,131391,132141,137581,139011,144346,145326,151456,3385,9090,13354,6702,39677,95536,19,622,852,854,928,929,941,946,947,988,30740,30751,30764,30769,30770,30771,30772,30777,30778,31028,31055,31121,32304,32347,33589,34005,52886,52889,33472,55801,73801,30968,30969,31034,31040,31071,31102,32055,34769,52066,111626,120391,129081,49713,145371,5981,74011,114236,126136,31081,33483,153221,23538,30093,1606,15610,2850,46543,22262,44177,34694,15660,12658,16064,10092,30952,30974,31001,160526,546,648,940,941,944,945,206,235,647,860,863,905)
  AND EXISTS(SELECT * FROM commerce_payment_transaction t WHERE t.order_id = o.order_id AND t.status = 'Success' AND t.payment_method <> 'rcpar_partners_payment_freetrial')
#ORDER BY o.order_id
GROUP BY o.order_id;
