/* Replaces: free_trial_3 */

SET time_zone = '-7:00';

DROP TABLE IF EXISTS `rpt_free_trial_2purchase`;

CREATE TABLE rpt_free_trial_2purchase 
(
  id int(10) NOT NULL AUTO_INCREMENT COMMENT 'Unique id',
  user_id int(10) unsigned NOT NULL COMMENT 'drupal user id',
  mail varchar(254) NOT NULL COMMENT 'email',
  order_id int(10) unsigned NOT NULL COMMENT 'order id',
  total_purchased_order_count int(10) NOT NULL COMMENT 'counts for that day',
  date_first_ft varchar(13) DEFAULT '0' COMMENT 'first free trial date',
  first_purchase_date varchar(13) DEFAULT '0' COMMENT 'first purchase date',
  ft_time_diff bigint(21) COMMENT 'diff in times',
  ft_time_group varchar(10) COMMENT 'time group',
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 COMMENT='all FT orders daily';

INSERT INTO rpt_free_trial_2purchase (
  user_id,
  mail,
  order_id,
  total_purchased_order_count,
  first_purchase_date,
  date_first_ft,
  ft_time_diff,
  ft_time_group
)

SELECT 
	u.uid AS user_id,
	u.mail AS mail,
    o.order_id AS order_id,
	COUNT(o.order_id) AS total_purchased_order_count, 
    MIN(DATE(FROM_UNIXTIME(trans.changed))) AS first_purchase_date,
	ftu.date_first_ft,
	TIMESTAMPDIFF(DAY, ftu.date_first_ft,	FROM_UNIXTIME(trans.changed)) AS ft_timediff,
    IF((TIMESTAMPDIFF(DAY,ftu.date_first_ft, MIN(FROM_UNIXTIME(trans.changed))) BETWEEN 0 AND 7), '1st Week', 
    IF((TIMESTAMPDIFF(DAY,ftu.date_first_ft, MIN(FROM_UNIXTIME(trans.changed))) BETWEEN 8 AND 14),'2nd Week',
	IF((TIMESTAMPDIFF(DAY,ftu.date_first_ft, MIN(FROM_UNIXTIME(trans.changed))) BETWEEN 15 AND 21),'3rd Week',
	IF((TIMESTAMPDIFF(DAY,ftu.date_first_ft, MIN(FROM_UNIXTIME(trans.changed))) BETWEEN 22 AND 31),'4th Week','5th Week +')))) AS ft_time_group 

FROM commerce_order o 

LEFT JOIN field_data_commerce_customer_billing billing_prof ON (o.order_id = billing_prof.entity_id) AND (billing_prof.entity_type = 'commerce_order')
LEFT JOIN commerce_customer_profile prof ON (prof.profile_id = billing_prof.commerce_customer_billing_profile_id) AND (prof.type = 'billing')
LEFT JOIN users u ON prof.uid = u.uid
LEFT JOIN field_data_commerce_customer_address address ON (address.entity_id = prof.profile_id) AND (address.entity_type = 'commerce_customer_profile')
LEFT JOIN commerce_payment_transaction trans ON (trans.order_id = o.order_id)
LEFT JOIN rpt_free_trial_users ftu ON (ftu.user_id = o.uid)

WHERE timestampdiff(DAY,ftu.date_first_ft, CAST(from_unixtime(trans.changed) as date)) >= 0
	AND trans.payment_method <> 'rcpar_partners_payment_freetrial'
	AND o.status IN ('Pending','Processing','completed','shipped') 
	AND u.uid NOT IN (157631,0,2625,4035,24768,114306,114521,1,37,392,406,649,650,878,899,925,1159,2023,2024,2025,2026,2027,2034,2035,2808,2930,3294,3295,3296,3297,3443,3893,4780,4942,5173,5752,5783,6533,7159,7270,7742,8063,8065,8707,8710,8809,9283,9432,9433,9434,9435,9436,9437,9438,10160,10190,10685,10926,11125,11127,11425,11866,12743,13001,13017,13167,13270,13271,13774,13922,14146,14252,14373,14796,14945,15051,15053,15236,15405,15610,15734,15913,16269,16312,16807,16924,16925,16926,16927,16928,16951,16952,16953,16954,16957,16962,16969,16990,16991,16995,16996,16997,16998,16999,17022,17023,17024,17025,17154,17246,17511,17793,17797,17815,18525,18654,18990,20092,20176,20179,20336,20337,20566,21618,21619,21632,21837,21848,21882,22384,22599,22781,23022,23101,23388,23497,23795,23805,23975,24133,25182,25550,25829,27559,28576,28714,29092,29264,29266,30749,30763,31026,31156,32178,33317,33739,35046,35096,35367,36142,36199,36567,37896,39777,43941,44035,46356,48095,48392,50017,50264,50595,51514,51923,53289,53732,57191,58966,63721,64671,64981,67611,75811,78921,82881,85196,95026,95941,112051,114241,116281,117671,120716,123781,126581,131221,131391,132141,137581,139011,144346,145326,151456,3385,9090,13354,6702,39677,95536,19,622,852,854,928,929,941,946,947,988,30740,30751,30764,30769,30770,30771,30772,30777,30778,31028,31055,31121,32304,32347,33589,34005,52886,52889,33472,55801,73801,30968,30969,31034,31040,31071,31102,32055,34769,52066,111626,120391,129081,49713,145371,5981,74011,114236,126136,31081,33483,153221,23538,30093,1606,15610,2850,46543,22262,44177,34694,15660,12658,16064,10092,30952,30974,31001,160526,546,648,940,941,944,945,206,235,647,860,863,905)

GROUP BY u.uid 
ORDER BY u.uid;
