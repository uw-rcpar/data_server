# README #

Data Server Nightly Rebuild.

### What is this repository for? ###

This repo contains scripts used for nightly rebuild of the data server
See documentation in Confluence for more information https://confluence.rogercpareview.com/pages/viewpage.action?pageId=20875189



To manually re-import data from last day:

     ./cron-import-manually.sh

To manually re-import data from custom 'days ago':
For example: 5 days ago

/mnt/gfs/home/rogercpa/scripts/sync_csv_scripts/extract_and_send_csv.sh  5  2>&1 | tee ~/etl/logs/extract-and-send_2019-05-22.log