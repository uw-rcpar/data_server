#!/bin/sh
# Remove backup db files from dev environments older than 2 days. If this script is ran daily it will limit the number of backups to 2 thus saving space
find /mnt/gfs/rogercpadev/backups/ /mnt/gfs/rogercpadev2/backups/ /mnt/gfs/rogercpadev3/backups/ /mnt/gfs/rogercpaintegrate/backups/ /mnt/gfs/rogercpastg/backups/ /mnt/gfs/rogercpastg2/backups/ /mnt/gfs/rogercpastg3/backups/ -type f -mtime +1 -delete
