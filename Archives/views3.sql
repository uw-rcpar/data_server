
-- MySQL dump 10.13  Distrib 5.6.32-78.1, for debian-linux-gnu (x86_64)
--
-- Host: staging-18487    Database: rogercpadev2
-- ------------------------------------------------------
-- Server version       5.6.32-78.1-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Temporary view structure for view `all`
--

DROP TABLE IF EXISTS `all`;
/*!50001 DROP VIEW IF EXISTS `all`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE VIEW `all` AS SELECT
 1 AS `User`,
 1 AS `Order ID`,
 1 AS `Date`,
 1 AS `Year`,
 1 AS `Month`,
 1 AS `Day`,
 1 AS `Graduated`,
 1 AS `Payment Method`,
 1 AS `Channel`,
 1 AS `Promotion Type`,
 1 AS `Promotion`,
 1 AS `Country`,
 1 AS `State`,
 1 AS `St`,
 1 AS `Market`,
 1 AS `Region`,
 1 AS `Zip`,
 1 AS `title`,
 1 AS `intacct`,
 1 AS `Total Amount`*/;
SET character_set_client = @saved_cs_client;

--
-- Temporary view structure for view `all_no_asl`
--

DROP TABLE IF EXISTS `all_no_asl`;
/*!50001 DROP VIEW IF EXISTS `all_no_asl`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE VIEW `all_no_asl` AS SELECT
 1 AS `User`,
 1 AS `Order ID`,
 1 AS `Date`,
 1 AS `Year`,
 1 AS `Month`,
 1 AS `Day`,
 1 AS `Graduated`,
 1 AS `Payment Method`,
 1 AS `Channel`,
 1 AS `Promotion`,
 1 AS `Country`,
 1 AS `State`,
 1 AS `St`,
 1 AS `Market`,
 1 AS `Region`,
 1 AS `Zip`,
 1 AS `title`,
 1 AS `intacct`,
 1 AS `Total Amount`*/;
SET character_set_client = @saved_cs_client;

--
-- Temporary view structure for view `attempts`
--

DROP TABLE IF EXISTS `attempts`;
/*!50001 DROP VIEW IF EXISTS `attempts`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE VIEW `attempts` AS SELECT
 1 AS `UID`,
 1 AS `First Exam Taken`,
 1 AS `Last Exam Taken`,
 1 AS `Avg Score`,
 1 AS `Attempts`*/;
SET character_set_client = @saved_cs_client;

--
-- Temporary view structure for view `free_trials`
--

DROP TABLE IF EXISTS `free_trials`;
/*!50001 DROP VIEW IF EXISTS `free_trials`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE VIEW `free_trials` AS SELECT
 1 AS `uid`,
 1 AS `order_id`,
 1 AS `payment_method`,
 1 AS `date(from_Unixtime(changed))`,
 1 AS `Year(from_Unixtime(changed))`,
 1 AS `Month(from_Unixtime(changed))`*/;
SET character_set_client = @saved_cs_client;

--
-- Temporary view structure for view `full-course`
--

DROP TABLE IF EXISTS `full-course`;
/*!50001 DROP VIEW IF EXISTS `full-course`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE VIEW `full-course` AS SELECT
 1 AS `User`,
 1 AS `Order ID`,
 1 AS `Date`,
 1 AS `Graduated`,
 1 AS `Payment Method`,
 1 AS `Net Price`,
 1 AS `Country`,
 1 AS `State`,
 1 AS `St`,
 1 AS `Market`,
 1 AS `Region`,
 1 AS `Net Price2`,
 1 AS `Label`,
 1 AS `Item line total`*/;
SET character_set_client = @saved_cs_client;

--
-- Temporary view structure for view `legacy_data`
--

DROP TABLE IF EXISTS `legacy_data`;
/*!50001 DROP VIEW IF EXISTS `legacy_data`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE VIEW `legacy_data` AS SELECT
 1 AS `ContactId`,
 1 AS `<feff>OrderID`,
 1 AS `Date`,
 1 AS `Year`,
 1 AS `Month`,
 1 AS `Select`,
 1 AS `Premium`,
 1 AS `Elite`,
 1 AS `AUD`,
 1 AS `FAR`,
 1 AS `BEC`,
 1 AS `REG`,
 1 AS `AUD-6MEXT`,
 1 AS `FAR-6MEXT`,
 1 AS `BEC-6MEXT`,
 1 AS `REG-6MEXT`,
 1 AS `AUD-AUD`,
 1 AS `FAR-AUD`,
 1 AS `BEC-AUD`,
 1 AS `REG-AUD`,
 1 AS `AUD-CRAM`,
 1 AS `FAR-CRAM`,
 1 AS `BEC-CRAM`,
 1 AS `REG-CRAM`,
 1 AS `AUD-EFC`,
 1 AS `FAR-EFC`,
 1 AS `BEC-EFC`,
 1 AS `REG-EFC`,
 1 AS `AUD-OFF-LEC`,
 1 AS `FAR-OFF-LEC`,
 1 AS `BEC-OFF-LEC`,
 1 AS `REG-OFF-LEC`,
 1 AS `Channel`,
 1 AS `taxes`,
 1 AS `shipping`,
 1 AS `OrderTotal`*/;
SET character_set_client = @saved_cs_client;

--
-- Temporary view structure for view `pass4`
--

DROP TABLE IF EXISTS `pass4`;
/*!50001 DROP VIEW IF EXISTS `pass4`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE VIEW `pass4` AS SELECT
 1 AS `UID`,
 1 AS `First Exam Taken`,
 1 AS `Last Exam Taken`,
 1 AS `Avg Score`,
 1 AS `Attempts`*/;
SET character_set_client = @saved_cs_client;

--
-- Temporary view structure for view `products`
--

DROP TABLE IF EXISTS `products`;
/*!50001 DROP VIEW IF EXISTS `products`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE VIEW `products` AS SELECT
 1 AS `UserId`,
 1 AS `OrderId`,
 1 AS `Date`,
 1 AS `Year`,
 1 AS `Month`,
 1 AS `graduated`,
 1 AS `Payment Method`,
 1 AS `Channel`,
 1 AS `Select`,
 1 AS `Premier`,
 1 AS `Elite`,
 1 AS `AUD`,
 1 AS `FAR`,
 1 AS `BEC`,
 1 AS `REG`,
 1 AS `AUD-6MEXT`,
 1 AS `FAR-6MEXT`,
 1 AS `BEC-6MEXT`,
 1 AS `REG-6MEXT`,
 1 AS `FULL-6MEXT`,
 1 AS `AUD-AUD`,
 1 AS `FAR-AUD`,
 1 AS `BEC-AUD`,
 1 AS `REG-AUD`,
 1 AS `AUD-CRAM`,
 1 AS `FAR-CRAM`,
 1 AS `BEC-CRAM`,
 1 AS `REG-CRAM`,
 1 AS `AUD-EFC`,
 1 AS `FAR-EFC`,
 1 AS `BEC-EFC`,
 1 AS `REG-EFC`,
 1 AS `AUD-OFF-LEC`,
 1 AS `FAR-OFF-LEC`,
 1 AS `BEC-OFF-LEC`,
 1 AS `REG-OFF-LEC`,
 1 AS `CRAM-CERT`,
 1 AS `PARTNER-DIS`,
 1 AS `Promotion Type`,
 1 AS `Promotion`,
 1 AS `taxes`,
 1 AS `shipping`,
 1 AS `OrderTotal`,
 1 AS `Country`,
 1 AS `State`,
 1 AS `St`,
 1 AS `Zip`,
 1 AS `Market`,
 1 AS `Region`*/;
SET character_set_client = @saved_cs_client;

--
 1 AS `Section`,
 1 AS `Country`,
 1 AS `State`,
 1 AS `St`,
 1 AS `Market`,
 1 AS `Region`,
 1 AS `US College`,
 1 AS `Campus State`,
 1 AS `College Name`,
 1 AS `Exam Type`*/;
SET character_set_client = @saved_cs_client;

--
-- Final view structure for view `all`
--

/*!50001 DROP VIEW IF EXISTS `all`*/;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `all_no_asl`
--

/*!50001 DROP VIEW IF EXISTS `all_no_asl`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET character_set_client      = utf8mb4 */;
/*!50001 SET character_set_results     = utf8mb4 */;
/*!50001 SET collation_connection      = utf8mb4_general_ci */;
/*!50001 CREATE ALGORITHM=TEMPTABLE */
/*!50013  SQL SECURITY DEFINER */
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8mb4 */;
/*!50001 SET character_set_results     = utf8mb4 */;
/*!50001 SET collation_connection      = utf8mb4_general_ci */;
/*!50001 CREATE ALGORITHM=TEMPTABLE */
/*!50013  SQL SECURITY DEFINER */
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `legacy_data`
--
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8mb4 */;
/*!50001 SET character_set_results     = utf8mb4 */;
/*!50001 SET collation_connection      = utf8mb4_general_ci */;
/*!50001 CREATE ALGORITHM=TEMPTABLE */
/*!50013  SQL SECURITY DEFINER */
/*!50001 SET character_set_results     = utf8mb4 */;
/*!50001 SET collation_connection      = utf8mb4_general_ci */;
/*!50001 CREATE ALGORITHM=TEMPTABLE */
/*!50013  SQL SECURITY DEFINER */
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `revenue`
--

/*!50001 DROP VIEW IF EXISTS `revenue`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8mb4 */;
/*!50001 SET character_set_results     = utf8mb4 */;
/*!50001 SET collation_connection      = utf8mb4_general_ci */;
/*!50001 CREATE ALGORITHM=TEMPTABLE */
/*!50013  SQL SECURITY DEFINER */
/*!50001 VIEW `revenue` AS (select `cli_data`.`entity_id` AS `entity_id`,`revenue`.`revenue` AS `revenue`,`cli`.`line_item_label` AS `line_item_label` from ((`field_data_commerce_line_items` `cli_data` join `commerce_line_item` `cli` on((`cli`.`line_item_id` = `cli_data`.`commerce_line_items_line_item_id`))) join `rcpar_data_analysis_helper_item_line_revenue` `revenue` on((`revenue`.`line_item_id` = `cli_data`.`commerce_line_items_line_item_id`))) where (`cli_data`.`entity_type` = 'commerce_order')) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `scores`
--

/*!50001 DROP VIEW IF EXISTS `scores`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8mb4 */;
/*!50001 SET character_set_results     = utf8mb4 */;
/*!50001 SET collation_connection      = utf8mb4_general_ci */;
/*!50001 CREATE ALGORITHM=TEMPTABLE */
/*!50013  SQL SECURITY DEFINER */
/*!50001 VIEW `scores` AS select `eck_exam_scores`.`id` AS `id`,`eck_exam_scores`.`attempt` AS `Attempt`,`eck_exam_scores`.`uid` AS `UID`,cast(from_unixtime(`eck_exam_scores`.`created`) as date) AS `Score Entered`,cast(from_unixtime(`eck_exam_scores`.`exam_date`) as date) AS `Exam Taken`,year(from_unixtime(`eck_exam_scores`.`exam_date`)) AS `Exam Year`,`eck_exam_scores`.`score` AS `Score`,if((`eck_exam_scores`.`score` > 74),'Pass','Fail') AS `Pass/Fail`,`eck_exam_scores`.`section` AS `Section`,`country_olaf`.`COUNTRY` AS `Country`,if(isnull(`state_olaf`.`State`),'International',`state_olaf`.`State`) AS `State`,if(isnull(`state_olaf`.`State`),'Intl',`state_olaf`.`State Abbrev`) AS `St`,if(isnull(`territory_olaf`.`Market`),'International',`territory_olaf`.`Market`) AS `Market`,if(isnull(`territory_olaf`.`Region`),'International',`territory_olaf`.`Region`) AS `Region`,`did_grad`.`field_did_you_graduate_college_value` AS `US College`,`coll_state_olaf`.`State` AS `Campus State`,`t`.`name` AS `College Name`,if((from_unixtime(`eck_exam_scores`.`exam_date`) >= '2017-04-01'),'New Exam','Old Exam') AS `Exam Type` from (((((((((((((`eck_exam_scores` join `users` `u` on((`u`.`uid` = `eck_exam_scores`.`uid`))) left join `commerce_order` `o` on((`o`.`order_id` = (select max(`o2`.`order_id`) from `commerce_order` `o2` where ((`o2`.`uid` = `u`.`uid`) and (`o2`.`status` in ('completed','shipped'))))))) left join `field_data_commerce_customer_billing` `billing_prof` on(((`o`.`order_id` = `billing_prof`.`entity_id`) and (`billing_prof`.`entity_type` = 'commerce_order')))) left join `commerce_customer_profile` `prof` on(((`prof`.`profile_id` = `billing_prof`.`commerce_customer_billing_profile_id`) and (`prof`.`type` = 'billing')))) left join `field_data_commerce_customer_address` `address` on(((`address`.`entity_id` = `prof`.`profile_id`) and (`address`.`entity_type` = 'commerce_customer_profile')))) left join `country_olaf` on((`address`.`commerce_customer_address_country` = `country_olaf`.`A2 (ISO)`))) left join `state_olaf` on((`address`.`commerce_customer_address_administrative_area` = `state_olaf`.`State Abbrev`))) left join `territory_olaf` on((`address`.`commerce_customer_address_administrative_area` = convert(`territory_olaf`.`State` using utf8)))) left join `field_data_field_did_you_graduate_college` `did_grad` on(((`did_grad`.`entity_id` = `u`.`uid`) and (`did_grad`.`entity_type` = 'user')))) left join `field_data_field_college_state_list` `coll_state` on(((`coll_state`.`entity_id` = `u`.`uid`) and (`coll_state`.`entity_type` = 'user')))) left join `state_olaf` `coll_state_olaf` on((`coll_state`.`field_college_state_list_value` = `coll_state_olaf`.`State Abbrev`))) left join `field_data_field_college_state` `coll_name_tid` on(((`coll_name_tid`.`entity_id` = `u`.`uid`) and (`coll_name_tid`.`entity_type` = 'user')))) left join `taxonomy_term_data` `t` on((`t`.`tid` = `coll_name_tid`.`field_college_state_tid`))) where ((not(exists(select 1 from (`users_roles` `ur` join `role` `r` on((`r`.`rid` = `ur`.`rid`))) where ((`ur`.`uid` = `u`.`uid`) and (`r`.`name` in ('administrator','customer support','editor','field member','IPQ editor','moderator')))))) and (from_unixtime(`eck_exam_scores`.`exam_date`) >= '2014-01-01')) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;


