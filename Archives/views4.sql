-- MySQL dump 10.13  Distrib 5.7.17, for Win64 (x86_64)
--
-- Host: 127.0.0.1    Database: rogercpadev2
-- ------------------------------------------------------
-- Server version5.6.32-78.1-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Temporary view structure for view `all`
--

DROP TABLE IF EXISTS `all`;
/*!50001 DROP VIEW IF EXISTS `all`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE VIEW `all` AS SELECT 
 1 AS `User`,
 1 AS `Order ID`,
 1 AS `Date`,
 1 AS `Year`,
 1 AS `Month`,
 1 AS `Day`,
 1 AS `Graduated`,
 1 AS `Payment Method`,
 1 AS `Channel`,
 1 AS `Promotion Type`,
 1 AS `Promotion`,
 1 AS `Country`,
 1 AS `State`,
 1 AS `St`,
 1 AS `Market`,
 1 AS `Region`,
 1 AS `Zip`,
 1 AS `title`,
 1 AS `intacct`,
 1 AS `Total Amount`*/;
SET character_set_client = @saved_cs_client;

--
-- Temporary view structure for view `all_no_asl`
--

DROP TABLE IF EXISTS `all_no_asl`;
/*!50001 DROP VIEW IF EXISTS `all_no_asl`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE VIEW `all_no_asl` AS SELECT 
 1 AS `User`,
 1 AS `Order ID`,
 1 AS `Date`,
 1 AS `Year`,
 1 AS `Month`,
 1 AS `Day`,
 1 AS `Graduated`,
 1 AS `Payment Method`,
 1 AS `Channel`,
 1 AS `Promotion`,
 1 AS `Country`,
 1 AS `State`,
 1 AS `St`,
 1 AS `Market`,
 1 AS `Region`,
 1 AS `Zip`,
 1 AS `title`,
 1 AS `intacct`,
 1 AS `Total Amount`*/;
SET character_set_client = @saved_cs_client;

--
-- Temporary view structure for view `attempts`
--

DROP TABLE IF EXISTS `attempts`;
/*!50001 DROP VIEW IF EXISTS `attempts`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE VIEW `attempts` AS SELECT 
 1 AS `UID`,
 1 AS `First Exam Taken`,
 1 AS `Last Exam Taken`,
 1 AS `Avg Score`,
 1 AS `Attempts`*/;
SET character_set_client = @saved_cs_client;

--
-- Temporary view structure for view `free_trials`
--

DROP TABLE IF EXISTS `free_trials`;
/*!50001 DROP VIEW IF EXISTS `free_trials`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE VIEW `free_trials` AS SELECT 
 1 AS `uid`,
 1 AS `order_id`,
 1 AS `payment_method`,
 1 AS `date(from_Unixtime(changed))`,
 1 AS `Year(from_Unixtime(changed))`,
 1 AS `Month(from_Unixtime(changed))`*/;
SET character_set_client = @saved_cs_client;

--
-- Temporary view structure for view `full-course`
--

DROP TABLE IF EXISTS `full-course`;
/*!50001 DROP VIEW IF EXISTS `full-course`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE VIEW `full-course` AS SELECT 
 1 AS `User`,
 1 AS `Order ID`,
 1 AS `Date`,
 1 AS `Graduated`,
 1 AS `Payment Method`,
 1 AS `Net Price`,
 1 AS `Country`,
 1 AS `State`,
 1 AS `St`,
 1 AS `Market`,
 1 AS `Region`,
 1 AS `Net Price2`,
 1 AS `Label`,
 1 AS `Item line total`*/;
SET character_set_client = @saved_cs_client;

--
-- Temporary view structure for view `legacy_data`
--

DROP TABLE IF EXISTS `legacy_data`;
/*!50001 DROP VIEW IF EXISTS `legacy_data`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE VIEW `legacy_data` AS SELECT 
 1 AS `ContactId`,
 1 AS `OrderID`,
 1 AS `Date`,
 1 AS `Year`,
 1 AS `Month`,
 1 AS `Select`,
 1 AS `Premium`,
 1 AS `Elite`,
 1 AS `AUD`,
 1 AS `FAR`,
 1 AS `BEC`,
 1 AS `REG`,
 1 AS `AUD-6MEXT`,
 1 AS `FAR-6MEXT`,
 1 AS `BEC-6MEXT`,
 1 AS `REG-6MEXT`,
 1 AS `AUD-AUD`,
 1 AS `FAR-AUD`,
 1 AS `BEC-AUD`,
 1 AS `REG-AUD`,
 1 AS `AUD-CRAM`,
 1 AS `FAR-CRAM`,
 1 AS `BEC-CRAM`,
 1 AS `REG-CRAM`,
 1 AS `AUD-EFC`,
 1 AS `FAR-EFC`,
 1 AS `BEC-EFC`,
 1 AS `REG-EFC`,
 1 AS `AUD-OFF-LEC`,
 1 AS `FAR-OFF-LEC`,
 1 AS `BEC-OFF-LEC`,
 1 AS `REG-OFF-LEC`,
 1 AS `Channel`,
 1 AS `taxes`,
 1 AS `shipping`,
 1 AS `OrderTotal`*/;
SET character_set_client = @saved_cs_client;

--
-- Temporary view structure for view `pass4`
--

DROP TABLE IF EXISTS `pass4`;
/*!50001 DROP VIEW IF EXISTS `pass4`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE VIEW `pass4` AS SELECT 
 1 AS `UID`,
 1 AS `First Exam Taken`,
 1 AS `Last Exam Taken`,
 1 AS `Avg Score`,
 1 AS `Attempts`*/;
SET character_set_client = @saved_cs_client;

--
-- Temporary view structure for view `products`
--

DROP TABLE IF EXISTS `products`;
/*!50001 DROP VIEW IF EXISTS `products`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE VIEW `products` AS SELECT 
 1 AS `UserId`,
 1 AS `OrderId`,
 1 AS `Date`,
 1 AS `Year`,
 1 AS `Month`,
 1 AS `graduated`,
 1 AS `Payment Method`,
 1 AS `Channel`,
 1 AS `Select`,
 1 AS `Premier`,
 1 AS `Elite`,
 1 AS `AUD`,
 1 AS `FAR`,
 1 AS `BEC`,
 1 AS `REG`,
 1 AS `AUD-6MEXT`,
 1 AS `FAR-6MEXT`,
 1 AS `BEC-6MEXT`,
 1 AS `REG-6MEXT`,
 1 AS `FULL-6MEXT`,
 1 AS `AUD-AUD`,
 1 AS `FAR-AUD`,
 1 AS `BEC-AUD`,
 1 AS `REG-AUD`,
 1 AS `AUD-CRAM`,
 1 AS `FAR-CRAM`,
 1 AS `BEC-CRAM`,
 1 AS `REG-CRAM`,
 1 AS `AUD-EFC`,
1 AS `FAR-EFC`,
1 AS `BEC-EFC`,
1 AS `REG-EFC`,
 1 AS `AUD-SSD`,
 1 AS `FAR-SSD`,
 1 AS `BEC-SSD`,
 1 AS `REG-SSD`,
 1 AS `AUD-OFF-LEC`,
 1 AS `FAR-OFF-LEC`,
 1 AS `BEC-OFF-LEC`,
 1 AS `REG-OFF-LEC`,
 1 AS `CRAM-CERT`,
 1 AS `PARTNER-DIS`,
 1 AS `Promotion Type`,
 1 AS `Promotion`,
 1 AS `taxes`,
 1 AS `shipping`,
 1 AS `OrderTotal`,
 1 AS `Country`,
 1 AS `State`,
 1 AS `St`,
 1 AS `Zip`,
 1 AS `Market`,
 1 AS `Region`*/;
SET character_set_client = @saved_cs_client;

--
-- Temporary view structure for view `revenue`
--

DROP TABLE IF EXISTS `revenue`;
/*!50001 DROP VIEW IF EXISTS `revenue`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE VIEW `revenue` AS SELECT 
 1 AS `entity_id`,
 1 AS `revenue`,
 1 AS `line_item_label`*/;
SET character_set_client = @saved_cs_client;

--
-- Temporary view structure for view `scores`
--

DROP TABLE IF EXISTS `scores`;
/*!50001 DROP VIEW IF EXISTS `scores`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE VIEW `scores` AS SELECT 
 1 AS `id`,
 1 AS `Attempt`,
 1 AS `UID`,
 1 AS `Score Entered`,
 1 AS `Exam Taken`,
 1 AS `Exam Year`,
 1 AS `Score`,
 1 AS `Pass/Fail`,
 1 AS `Section`,
 1 AS `Country`,
 1 AS `State`,
 1 AS `St`,
 1 AS `Market`,
 1 AS `Region`,
 1 AS `US College`,
 1 AS `Campus State`,
 1 AS `College Name`,
 1 AS `Exam Type`*/;
SET character_set_client = @saved_cs_client;

--
-- Temporary view structure for view `fsl_conversions`
--

DROP TABLE IF EXISTS `fsl_conversions`;
/*!50001 DROP VIEW IF EXISTS `fsl_conversions`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE VIEW `fsl_conversions` AS SELECT 
 1 AS `User`,
 1 AS `Email`,
 1 AS `Firm`,
 1 AS `Enroll Date`,
 1 AS `Order ID`,
 1 AS `Package`,
 1 AS `Conv`,
 1 AS `Conv Date`,
 1 AS `Conv Time`,
 1 AS `Conv Period`*/;
SET character_set_client = @saved_cs_client;

--
-- Temporary view structure for view `fsl_enrollees`
--

DROP TABLE IF EXISTS `fsl_enrollees`;
/*!50001 DROP VIEW IF EXISTS `fsl_enrollees`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE VIEW `fsl_enrollees` AS SELECT 
 1 AS `User`,
 1 AS `Email`,
 1 AS `Order ID`,
 1 AS `Firm`,
 1 AS `Enroll Date`*/;
SET character_set_client = @saved_cs_client;

--
-- Temporary view structure for view `hhc`
--

DROP TABLE IF EXISTS `hhc`;
/*!50001 DROP VIEW IF EXISTS `hhc`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE VIEW `hhc` AS SELECT 
 1 AS `uid`,
 1 AS `User`,
 1 AS `package`,
 1 AS `User Group`,
 1 AS `cid`,
 1 AS `Moderator`,
 1 AS `Date`,
 1 AS `Answer Date`,
 1 AS `thread`,
 1 AS `Year`,
 1 AS `Month`,
 1 AS `Day`,
 1 AS `Timediff`,
 1 AS `title`,
 1 AS `Section/Chapter`,
 1 AS `Section`,
 1 AS `Moderator Question`,
 1 AS `comment_body_value`*/;
SET character_set_client = @saved_cs_client;

--
-- Temporary view structure for view `hhc_user_groups`
--

DROP TABLE IF EXISTS `hhc_user_groups`;
/*!50001 DROP VIEW IF EXISTS `hhc_user_groups`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE VIEW `hhc_user_groups` AS SELECT 
 1 AS `uid`,
 1 AS `Count`,
 1 AS `User Group`*/;
SET character_set_client = @saved_cs_client;

--
-- Temporary view structure for view `user_package`
--

DROP TABLE IF EXISTS `user_package`;
/*!50001 DROP VIEW IF EXISTS `user_package`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE VIEW `user_package` AS SELECT 
 1 AS `uid`,
 1 AS `order_id`,
 1 AS `Package`*/;
SET character_set_client = @saved_cs_client;


--
-- Final view structure for view `all`
--

/*!50001 DROP VIEW IF EXISTS `all`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8mb4 */;
/*!50001 SET character_set_results     = utf8mb4 */;
/*!50001 SET collation_connection      = utf8mb4_general_ci */;
/*!50001 CREATE ALGORITHM=TEMPTABLE */
/*!50013  SQL SECURITY DEFINER */
/*!50001 VIEW `all` AS select `u`.`uid` AS `User`,`o`.`order_id` AS `Order ID`,date(from_Unixtime(trans.changed))  AS `Date`,year(from_unixtime(`trans`.`changed`)) AS `Year`,month(from_unixtime(`trans`.`changed`)) AS `Month`,dayofmonth(from_unixtime(`trans`.`changed`)) AS `Day`,`grad`.`field_did_you_graduate_college_value` AS `Graduated`,if((`trans`.`payment_method` = 'authnet_aim'),'Direct',if((`trans`.`payment_method` = 'rcpar_partners_payment'),'Partner','Affirm')) AS `Payment Method`,if((`trans`.`payment_method` = 'authnet_aim'),'B2C',if((`trans`.`payment_method` = 'rcpar_partners_payment'),'B2B','B2C')) AS `Channel`,`cct`.`label` AS `Promotion Type`,`fdccc`.`commerce_coupon_code_value` AS `Promotion`,`country_olaf`.`COUNTRY` AS `Country`,if(isnull(`state_olaf`.`State`),'International',`state_olaf`.`State`) AS `State`,if(isnull(`state_olaf`.`State`),'Intl',`state_olaf`.`State Abbrev`) AS `St`,if(isnull(`territory_olaf`.`Market`),'International',`territory_olaf`.`Market`) AS `Market`,if(isnull(`territory_olaf`.`Region`),'International',`territory_olaf`.`Region`) AS `Region`,`address`.`commerce_customer_address_postal_code` AS `Zip`,`partner`.`title` AS `title`,`intacct`.`field_intacct_customer_id_value` AS `intacct`,if(((`line_total`.`commerce_total_amount` / 100) > 0),((`trans`.`amount` - `line_total`.`commerce_total_amount`) / 100),(`trans`.`amount` / 100)) AS `Total Amount` from ((((((((((((((((((`commerce_order` `o` join `field_data_commerce_customer_billing` `billing_prof` on(((`o`.`order_id` = `billing_prof`.`entity_id`) and (`billing_prof`.`entity_type` = 'commerce_order')))) join `commerce_customer_profile` `prof` on(((`prof`.`profile_id` = `billing_prof`.`commerce_customer_billing_profile_id`) and (`prof`.`type` = 'billing')))) left join `users` `u` on((`prof`.`uid` = `u`.`uid`))) left join `field_data_commerce_customer_address` `address` on(((`address`.`entity_id` = `prof`.`profile_id`) and (`address`.`entity_type` = 'commerce_customer_profile')))) left join `commerce_payment_transaction` `trans` on((`trans`.`order_id` = `o`.`order_id`))) left join `field_data_field_did_you_graduate_college` `grad` on(((`grad`.`entity_type` = 'user') and (`grad`.`entity_id` = `u`.`uid`)))) left join `commerce_line_item` `tax_line` on(((`tax_line`.`line_item_id` = (select `temp_li`.`line_item_id` from `commerce_line_item` `temp_li` where ((`temp_li`.`order_id` = `trans`.`order_id`) and (`temp_li`.`type` = 'avatax')) order by `temp_li`.`line_item_id` desc limit 1)) and (`trans`.`transaction_id` = (select min(`commerce_payment_transaction`.`transaction_id`) from `commerce_payment_transaction` where ((`trans`.`status` = 'Success') and (`trans`.`payment_method` <> 'rcpar_partners_payment_freetrial'))))))) left join `field_data_commerce_total` `line_total` on(((`line_total`.`entity_type` = 'commerce_line_item') and (`line_total`.`entity_id` = `tax_line`.`line_item_id`)))) left join `country_olaf` on((`address`.`commerce_customer_address_country` = `country_olaf`.`A2 (ISO)`))) left join `state_olaf` on((`address`.`commerce_customer_address_administrative_area` = `state_olaf`.`State Abbrev`))) left join `territory_olaf` on((`address`.`commerce_customer_address_administrative_area` = convert(`territory_olaf`.`State` using utf8)))) left join `field_data_field_partner_profile` `partner_ref` on(((`partner_ref`.`entity_id` = `o`.`order_id`) and (`partner_ref`.`entity_type` = 'commerce_order')))) left join `node` `partner` on((`partner`.`nid` = `partner_ref`.`field_partner_profile_target_id`))) left join `field_data_field_intacct_customer_id` `intacct` on(((`partner`.`nid` = `intacct`.`entity_id`) and (`intacct`.`entity_type` = 'node')))) join `field_data_commerce_order_total` `order_total` on(((`order_total`.`entity_id` = `o`.`order_id`) and (`order_total`.`entity_type` = 'commerce_order')))) left join `field_data_commerce_coupon_order_reference` `fdccor` on((`fdccor`.`entity_id` = `o`.`order_id`))) left join `field_data_commerce_coupon_code` `fdccc` on((`fdccc`.`entity_id` = `fdccor`.`commerce_coupon_order_reference_target_id`))) left join `commerce_coupon_type` `cct` on((`cct`.`type` = `fdccc`.`bundle`))) where ((`trans`.`status` = 'Success') and (`trans`.`payment_method` <> 'rcpar_partners_payment_freetrial') and (`o`.`status` in ('Pending','Processing','completed','shipped')) and (`u`.`uid` not in (0,2625,4035,24768,114306,114521,1,37,392,406,649,650,878,899,925,1159,2023,2024,2025,2026,2027,2034,2035,2808,2930,3294,3295,3296,3297,3443,3893,4780,4942,5173,5752,5783,6533,7159,7270,7742,8063,8065,8707,8710,8809,9283,9432,9433,9434,9435,9436,9437,9438,10160,10190,10685,10926,11125,11127,11425,11866,12743,13001,13017,13167,13270,13271,13774,13922,14146,14252,14373,14796,14945,15051,15053,15236,15405,15610,15734,15913,16269,16312,16807,16924,16925,16926,16927,16928,16951,16952,16953,16954,16957,16962,16969,16990,16991,16995,16996,16997,16998,16999,17022,17023,17024,17025,17154,17246,17511,17793,17797,17815,18525,18654,18990,20092,20176,20179,20336,20337,20566,21618,21619,21632,21837,21848,21882,22384,22599,22781,23022,23101,23388,23497,23795,23805,23975,24133,25182,25550,25829,27559,28576,28714,29092,29264,29266,30749,30763,31026,31156,32178,33317,33739,35046,35096,35367,36142,36199,36567,37896,39777,43941,44035,46356,48095,48392,50017,50264,50595,51514,51923,53289,53732,57191,58966,63721,64671,64981,67611,75811,78921,82881,85196,95026,95941,112051,114241,116281,117671,120716,123781,126581,131221,131391,132141,137581,139011,144346,145326,151456,3385,9090,13354,6702,39677,95536,19,622,852,854,928,929,941,946,947,988,30740,30751,30764,30769,30770,30771,30772,30777,30778,31028,31055,31121,32304,32347,33589,34005,52886,52889,33472,55801,73801,30968,30969,31034,31040,31071,31102,32055,34769,52066,111626,120391,129081,49713,145371,5981,74011,114236,126136,31081,33483,153221,23538,30093,1606,15610,2850,46543,22262,44177,34694,15660,12658,16064,10092,30952,30974,31001,160526,546,648,940,941,944,945,206,235,647,860,863,905))) order by (`u`.`uid` and exists(select 1 from `commerce_payment_transaction` `t` where ((`t`.`order_id` = `o`.`order_id`) and (`t`.`status` = 'Success') and (`t`.`payment_method` <> 'rcpar_partners_payment_freetrial')))) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `all_no_asl`
--

/*!50001 DROP VIEW IF EXISTS `all_no_asl`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8mb4 */;
/*!50001 SET character_set_results     = utf8mb4 */;
/*!50001 SET collation_connection      = utf8mb4_general_ci */;
/*!50001 CREATE ALGORITHM=TEMPTABLE */
/*!50013  SQL SECURITY DEFINER */
/*!50001 VIEW `all_no_asl` AS select `u`.`uid` AS `User`,`o`.`order_id` AS `Order ID`,cast(from_unixtime(`trans`.`changed`) as date) AS `Date`,year(from_unixtime(`trans`.`changed`)) AS `Year`,month(from_unixtime(`trans`.`changed`)) AS `Month`,dayofmonth(from_unixtime(`trans`.`changed`)) AS `Day`,`grad`.`field_did_you_graduate_college_value` AS `Graduated`,if((`trans`.`payment_method` = 'authnet_aim'),'Direct',if((`trans`.`payment_method` = 'rcpar_partners_payment'),'Partner','Affirm')) AS `Payment Method`,if((`trans`.`payment_method` = 'authnet_aim'),'B2C',if((`trans`.`payment_method` = 'rcpar_partners_payment'),'B2B','B2C')) AS `Channel`,`fdccc`.`commerce_coupon_code_value` AS `Promotion`,`country_olaf`.`COUNTRY` AS `Country`,if(isnull(`state_olaf`.`State`),'International',`state_olaf`.`State`) AS `State`,if(isnull(`state_olaf`.`State`),'Intl',`state_olaf`.`State Abbrev`) AS `St`,if(isnull(`territory_olaf`.`Market`),'International',`territory_olaf`.`Market`) AS `Market`,if(isnull(`territory_olaf`.`Region`),'International',`territory_olaf`.`Region`) AS `Region`,`address`.`commerce_customer_address_postal_code` AS `Zip`,`partner`.`title` AS `title`,`intacct`.`field_intacct_customer_id_value` AS `intacct`,if(((`line_total`.`commerce_total_amount` / 100) > 0),((`trans`.`amount` - `line_total`.`commerce_total_amount`) / 100),(`trans`.`amount` / 100)) AS `Total Amount` from ((((((((((((((((((`commerce_order` `o` join `field_data_commerce_customer_billing` `billing_prof` on(((`o`.`order_id` = `billing_prof`.`entity_id`) and (`billing_prof`.`entity_type` = 'commerce_order')))) join `commerce_customer_profile` `prof` on(((`prof`.`profile_id` = `billing_prof`.`commerce_customer_billing_profile_id`) and (`prof`.`type` = 'billing')))) left join `users` `u` on((`prof`.`uid` = `u`.`uid`))) left join `field_data_commerce_customer_address` `address` on(((`address`.`entity_id` = `prof`.`profile_id`) and (`address`.`entity_type` = 'commerce_customer_profile')))) left join `commerce_payment_transaction` `trans` on((`trans`.`order_id` = `o`.`order_id`))) left join `field_data_field_did_you_graduate_college` `grad` on(((`grad`.`entity_type` = 'user') and (`grad`.`entity_id` = `u`.`uid`)))) left join `commerce_line_item` `tax_line` on(((`tax_line`.`line_item_id` = (select `temp_li`.`line_item_id` from `commerce_line_item` `temp_li` where ((`temp_li`.`order_id` = `trans`.`order_id`) and (`temp_li`.`type` = 'avatax')) order by `temp_li`.`line_item_id` desc limit 1)) and (`trans`.`transaction_id` = (select min(`commerce_payment_transaction`.`transaction_id`) from `commerce_payment_transaction` where ((`trans`.`status` = 'Success') and (`trans`.`payment_method` <> 'rcpar_partners_payment_freetrial'))))))) left join `field_data_commerce_total` `line_total` on(((`line_total`.`entity_type` = 'commerce_line_item') and (`line_total`.`entity_id` = `tax_line`.`line_item_id`)))) left join `country_olaf` on((`address`.`commerce_customer_address_country` = `country_olaf`.`A2 (ISO)`))) left join `state_olaf` on((`address`.`commerce_customer_address_administrative_area` = `state_olaf`.`State Abbrev`))) left join `territory_olaf` on((`address`.`commerce_customer_address_administrative_area` = convert(`territory_olaf`.`State` using utf8)))) left join `field_data_field_partner_profile` `partner_ref` on(((`partner_ref`.`entity_id` = `o`.`order_id`) and (`partner_ref`.`entity_type` = 'commerce_order')))) left join `node` `partner` on((`partner`.`nid` = `partner_ref`.`field_partner_profile_target_id`))) left join `field_data_field_intacct_customer_id` `intacct` on(((`partner`.`nid` = `intacct`.`entity_id`) and (`intacct`.`entity_type` = 'node')))) left join `field_data_field_partner_type` `pt` on(((`pt`.`entity_id` = `partner_ref`.`field_partner_profile_target_id`) and (`pt`.`entity_type` = 'node')))) join `field_data_commerce_order_total` `order_total` on(((`order_total`.`entity_id` = `o`.`order_id`) and (`order_total`.`entity_type` = 'commerce_order')))) left join `field_data_commerce_coupon_order_reference` `fdccor` on((`fdccor`.`entity_id` = `o`.`order_id`))) left join `field_data_commerce_coupon_code` `fdccc` on((`fdccc`.`entity_id` = `fdccor`.`commerce_coupon_order_reference_target_id`))) where ((`trans`.`status` = 'Success') and (`trans`.`payment_method` <> 'rcpar_partners_payment_freetrial') and (`o`.`status` in ('completed','shipped')) and ((`pt`.`field_partner_type_value` <> 3) or isnull(`partner_ref`.`entity_id`)) and (`u`.`uid` not in (0,2625,4035,24768,114306,114521,1,37,392,406,649,650,878,899,925,1159,2023,2024,2025,2026,2027,2034,2035,2808,2930,3294,3295,3296,3297,3443,3893,4780,4942,5173,5752,5783,6533,7159,7270,7742,8063,8065,8707,8710,8809,9283,9432,9433,9434,9435,9436,9437,9438,10160,10190,10685,10926,11125,11127,11425,11866,12743,13001,13017,13167,13270,13271,13774,13922,14146,14252,14373,14796,14945,15051,15053,15236,15405,15610,15734,15913,16269,16312,16807,16924,16925,16926,16927,16928,16951,16952,16953,16954,16957,16962,16969,16990,16991,16995,16996,16997,16998,16999,17022,17023,17024,17025,17154,17246,17511,17793,17797,17815,18525,18654,18990,20092,20176,20179,20336,20337,20566,21618,21619,21632,21837,21848,21882,22384,22599,22781,23022,23101,23388,23497,23795,23805,23975,24133,25182,25550,25829,27559,28576,28714,29092,29264,29266,30749,30763,31026,31156,32178,33317,33739,35046,35096,35367,36142,36199,36567,37896,39777,43941,44035,46356,48095,48392,50017,50264,50595,51514,51923,53289,53732,57191,58966,63721,64671,64981,67611,75811,78921,82881,85196,95026,95941,112051,114241,116281,117671,120716,123781,126581,131221,131391,132141,137581,139011,144346,145326,151456,3385,9090,13354,6702,39677,95536,19,622,852,854,928,929,941,946,947,988,30740,30751,30764,30769,30770,30771,30772,30777,30778,31028,31055,31121,32304,32347,33589,34005,52886,52889,33472,55801,73801,30968,30969,31034,31040,31071,31102,32055,34769,52066,111626,120391,129081,49713,145371,5981,74011,114236,126136,31081,33483,153221,23538,30093,1606,15610,2850,46543,22262,44177,34694,15660,12658,16064,10092,30952,30974,31001,160526,546,648,940,941,944,945,206,235,647,860,863,905))) order by (`u`.`uid` and exists(select 1 from `commerce_payment_transaction` `t` where ((`t`.`order_id` = `o`.`order_id`) and (`t`.`status` = 'Success') and (`t`.`payment_method` <> 'rcpar_partners_payment_freetrial')))) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `attempts`
--

/*!50001 DROP VIEW IF EXISTS `attempts`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8mb4 */;
/*!50001 SET character_set_results     = utf8mb4 */;
/*!50001 SET collation_connection      = utf8mb4_general_ci */;
/*!50001 CREATE ALGORITHM=TEMPTABLE */
/*!50013  SQL SECURITY DEFINER */
/*!50001 VIEW `attempts` AS select `scores`.`UID` AS `UID`,cast(min(`scores`.`Exam Taken`) as date) AS `First Exam Taken`,cast(max(`scores`.`Exam Taken`) as date) AS `Last Exam Taken`,round((sum(`scores`.`Score`) / count(`scores`.`Attempt`)),1) AS `Avg Score`,count(`scores`.`Attempt`) AS `Attempts` from `scores` where (`scores`.`Pass/Fail` = 'Pass') group by `scores`.`UID` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `free_trials`
--

/*!50001 DROP VIEW IF EXISTS `free_trials`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8mb4 */;
/*!50001 SET character_set_results     = utf8mb4 */;
/*!50001 SET collation_connection      = utf8mb4_general_ci */;
/*!50001 CREATE ALGORITHM=TEMPTABLE */
/*!50013  SQL SECURITY DEFINER */
/*!50001 VIEW `free_trials` AS
  select

    Date(from_Unixtime(trans.changed)) as `Date`,
    count(o.uid)

  FROM commerce_order o

    LEFT JOIN commerce_payment_transaction trans ON trans.order_id = o.order_id
    LEFT JOIN field_data_field_partner_profile pprof ON pprof.entity_id = o.order_id
    LEFT JOIN field_data_field_partner_type pt ON pt.entity_id = pprof.field_partner_profile_target_id

  where trans.payment_method = 'rcpar_partners_payment_freetrial' and pt.field_partner_type_value <> 3 # 3 means 'University'
        and o.uid not in ("0","1","19")

  group by date(from_Unixtime(trans.changed))

  */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `full-course`
--

/*!50001 DROP VIEW IF EXISTS `full-course`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8mb4 */;
/*!50001 SET character_set_results     = utf8mb4 */;
/*!50001 SET collation_connection      = utf8mb4_general_ci */;
/*!50001 CREATE ALGORITHM=TEMPTABLE */
/*!50013  SQL SECURITY DEFINER */
/*!50001 VIEW `full-course` AS select `u`.`uid` AS `User`,`o`.`order_id` AS `Order ID`,from_unixtime(`trans`.`changed`) AS `Date`,`grad`.`field_did_you_graduate_college_value` AS `Graduated`,if((`trans`.`payment_method` = 'authnet_aim'),'Direct',if((`trans`.`payment_method` = 'rcpar_partners_payment'),'Partner','Affirm')) AS `Payment Method`,if(((`line_total`.`commerce_total_amount` / 100) > 0),((`trans`.`amount` - `line_total`.`commerce_total_amount`) / 100),(`trans`.`amount` / 100)) AS `Net Price`,`country_olaf`.`COUNTRY` AS `Country`,if(isnull(`state_olaf`.`State`),'International',`state_olaf`.`State`) AS `State`,if(isnull(`state_olaf`.`State`),'Intl',`state_olaf`.`State Abbrev`) AS `St`,if(isnull(`territory_olaf`.`Market`),'International',`territory_olaf`.`Market`) AS `Market`,if(isnull(`territory_olaf`.`Region`),'International',`territory_olaf`.`Region`) AS `Region`,if(((`line_total`.`commerce_total_amount` / 100) > 0),((`trans`.`amount` - `line_total`.`commerce_total_amount`) / 100),(`trans`.`amount` / 100)) AS `Net Price2`,`cli`.`line_item_label` AS `Label`,(`cli_line_total`.`commerce_total_amount` / 100) AS `Item line total` from ((((((((((((((`commerce_order` `o` join `field_data_commerce_line_items` `cli_data` on(((`cli_data`.`entity_type` = 'commerce_order') and (`cli_data`.`entity_id` = `o`.`order_id`)))) join `commerce_line_item` `cli` on((`cli`.`line_item_id` = `cli_data`.`commerce_line_items_line_item_id`))) join `field_data_commerce_total` `cli_line_total` on(((`cli_line_total`.`entity_type` = 'commerce_line_item') and (`cli_line_total`.`entity_id` = `cli`.`line_item_id`)))) join `field_data_commerce_customer_billing` `billing_prof` on(((`o`.`order_id` = `billing_prof`.`entity_id`) and (`billing_prof`.`entity_type` = 'commerce_order')))) join `commerce_customer_profile` `prof` on(((`prof`.`profile_id` = `billing_prof`.`commerce_customer_billing_profile_id`) and (`prof`.`type` = 'billing')))) left join `users` `u` on((`prof`.`uid` = `u`.`uid`))) left join `field_data_commerce_customer_address` `address` on(((`address`.`entity_id` = `prof`.`profile_id`) and (`address`.`entity_type` = 'commerce_customer_profile')))) left join `commerce_payment_transaction` `trans` on((`trans`.`order_id` = `o`.`order_id`))) left join `field_data_field_did_you_graduate_college` `grad` on(((`grad`.`entity_type` = 'user') and (`grad`.`entity_id` = `u`.`uid`)))) left join `commerce_line_item` `tax_line` on(((`tax_line`.`line_item_id` = (select `temp_li`.`line_item_id` from `commerce_line_item` `temp_li` where ((`temp_li`.`order_id` = `trans`.`order_id`) and (`temp_li`.`type` = 'avatax')) order by `temp_li`.`line_item_id` desc limit 1)) and (`trans`.`transaction_id` = (select min(`commerce_payment_transaction`.`transaction_id`) from `commerce_payment_transaction` where ((`trans`.`status` = 'Success') and (`trans`.`payment_method` <> 'rcpar_partners_payment_freetrial'))))))) left join `field_data_commerce_total` `line_total` on(((`line_total`.`entity_type` = 'commerce_line_item') and (`line_total`.`entity_id` = `tax_line`.`line_item_id`)))) left join `country_olaf` on((`address`.`commerce_customer_address_country` = `country_olaf`.`A2 (ISO)`))) left join `state_olaf` on((`address`.`commerce_customer_address_administrative_area` = `state_olaf`.`State Abbrev`))) left join `territory_olaf` on((`address`.`commerce_customer_address_administrative_area` = convert(`territory_olaf`.`State` using utf8)))) where ((`trans`.`status` = 'Success') and (`trans`.`payment_method` <> 'rcpar_partners_payment_freetrial') and (`o`.`status` in ('completed','shipped')) and (`cli`.`line_item_label` in ('FULL-DIS','FULL-PREM-DIS','FULL-ELITE-DIS')) and (`u`.`uid` is not null) and (`u`.`uid` not in (0,2625,4035,24768,114306,114521,1,37,392,406,649,650,878,899,925,1159,2023,2024,2025,2026,2027,2034,2035,2808,2930,3294,3295,3296,3297,3443,3893,4780,4942,5173,5752,5783,6533,7159,7270,7742,8063,8065,8707,8710,8809,9283,9432,9433,9434,9435,9436,9437,9438,10160,10190,10685,10926,11125,11127,11425,11866,12743,13001,13017,13167,13270,13271,13774,13922,14146,14252,14373,14796,14945,15051,15053,15236,15405,15610,15734,15913,16269,16312,16807,16924,16925,16926,16927,16928,16951,16952,16953,16954,16957,16962,16969,16990,16991,16995,16996,16997,16998,16999,17022,17023,17024,17025,17154,17246,17511,17793,17797,17815,18525,18654,18990,20092,20176,20179,20336,20337,20566,21618,21619,21632,21837,21848,21882,22384,22599,22781,23022,23101,23388,23497,23795,23805,23975,24133,25182,25550,25829,27559,28576,28714,29092,29264,29266,30749,30763,31026,31156,32178,33317,33739,35046,35096,35367,36142,36199,36567,37896,39777,43941,44035,46356,48095,48392,50017,50264,50595,51514,51923,53289,53732,57191,58966,63721,64671,64981,67611,75811,78921,82881,85196,95026,95941,112051,114241,116281,117671,120716,123781,126581,131221,131391,132141,137581,139011,144346,145326,151456,3385,9090,13354,6702,39677,95536,19,622,852,854,928,929,941,946,947,988,30740,30751,30764,30769,30770,30771,30772,30777,30778,31028,31055,31121,32304,32347,33589,34005,52886,52889,33472,55801,73801,30968,30969,31034,31040,31071,31102,32055,34769,52066,111626,120391,129081,49713,145371,5981,74011,114236,126136,31081,33483,153221,23538,30093,1606,15610,2850,46543,22262,44177,34694,15660,12658,16064,10092,30952,30974,31001,160526,546,648,940,941,944,945,206,235,647,860,863,905))) order by `o`.`uid` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `legacy_data`
--

-- /*!50001 DROP VIEW IF EXISTS `legacy_data`*/;
-- /*!50001 SET @saved_cs_client          = @@character_set_client */;
-- /*!50001 SET @saved_cs_results         = @@character_set_results */;
-- /*!50001 SET @saved_col_connection     = @@collation_connection */;
-- /*!50001 SET character_set_client      = utf8mb4 */;
-- /*!50001 SET character_set_results     = utf8mb4 */;
-- /*!50001 SET collation_connection      = utf8mb4_general_ci */;
-- /*!50001 CREATE ALGORITHM=TEMPTABLE */
-- /*!50013  SQL SECURITY DEFINER */
-- /*!50001 VIEW `legacy_data` AS select `lmo`.`ContactID` AS `ContactId`,`lmo`.`OrderID` AS `OrderID`,`lmo`.`OrderDate` AS `Date`,year(`lmo`.`OrderDate`) AS `Year`,month(`lmo`.`OrderDate`) AS `Month`,(select (`r`.`revenue` / 100) from `rcpar_data_analysis_helper_legacy_order_item_revenue` `r` where ((`r`.`order_id` = `lmo`.`OrderID`) and (`r`.`item_name` = 'SELECT')) limit 1) AS `Select`,(select (`r`.`revenue` / 100) from `rcpar_data_analysis_helper_legacy_order_item_revenue` `r` where ((`r`.`order_id` = `lmo`.`OrderID`) and (`r`.`item_name` = 'PREMIUM')) limit 1) AS `Premium`,NULL AS `Elite`,(select (`r`.`revenue` / 100) from `rcpar_data_analysis_helper_legacy_order_item_revenue` `r` where ((`r`.`order_id` = `lmo`.`OrderID`) and (`r`.`item_name` = 'AUD')) limit 1) AS `AUD`,(select (`r`.`revenue` / 100) from `rcpar_data_analysis_helper_legacy_order_item_revenue` `r` where ((`r`.`order_id` = `lmo`.`OrderID`) and (`r`.`item_name` = 'FAR')) limit 1) AS `FAR`,(select (`r`.`revenue` / 100) from `rcpar_data_analysis_helper_legacy_order_item_revenue` `r` where ((`r`.`order_id` = `lmo`.`OrderID`) and (`r`.`item_name` = 'BEC')) limit 1) AS `BEC`,(select (`r`.`revenue` / 100) from `rcpar_data_analysis_helper_legacy_order_item_revenue` `r` where ((`r`.`order_id` = `lmo`.`OrderID`) and (`r`.`item_name` = 'REG')) limit 1) AS `REG`,(select (`r`.`revenue` / 100) from `rcpar_data_analysis_helper_legacy_order_item_revenue` `r` where ((`r`.`order_id` = `lmo`.`OrderID`) and (`r`.`item_name` = 'AUD-6MEXT')) limit 1) AS `AUD-6MEXT`,(select (`r`.`revenue` / 100) from `rcpar_data_analysis_helper_legacy_order_item_revenue` `r` where ((`r`.`order_id` = `lmo`.`OrderID`) and (`r`.`item_name` = 'FAR-6MEXT')) limit 1) AS `FAR-6MEXT`,(select (`r`.`revenue` / 100) from `rcpar_data_analysis_helper_legacy_order_item_revenue` `r` where ((`r`.`order_id` = `lmo`.`OrderID`) and (`r`.`item_name` = 'BEC-6MEXT')) limit 1) AS `BEC-6MEXT`,(select (`r`.`revenue` / 100) from `rcpar_data_analysis_helper_legacy_order_item_revenue` `r` where ((`r`.`order_id` = `lmo`.`OrderID`) and (`r`.`item_name` = 'REG-6MEXT')) limit 1) AS `REG-6MEXT`,(select (`r`.`revenue` / 100) from `rcpar_data_analysis_helper_legacy_order_item_revenue` `r` where ((`r`.`order_id` = `lmo`.`OrderID`) and (`r`.`item_name` = 'AUD-AUD')) limit 1) AS `AUD-AUD`,(select (`r`.`revenue` / 100) from `rcpar_data_analysis_helper_legacy_order_item_revenue` `r` where ((`r`.`order_id` = `lmo`.`OrderID`) and (`r`.`item_name` = 'FAR-AUD')) limit 1) AS `FAR-AUD`,(select (`r`.`revenue` / 100) from `rcpar_data_analysis_helper_legacy_order_item_revenue` `r` where ((`r`.`order_id` = `lmo`.`OrderID`) and (`r`.`item_name` = 'BEC-AUD')) limit 1) AS `BEC-AUD`,(select (`r`.`revenue` / 100) from `rcpar_data_analysis_helper_legacy_order_item_revenue` `r` where ((`r`.`order_id` = `lmo`.`OrderID`) and (`r`.`item_name` = 'REG-AUD')) limit 1) AS `REG-AUD`,(select (`r`.`revenue` / 100) from `rcpar_data_analysis_helper_legacy_order_item_revenue` `r` where ((`r`.`order_id` = `lmo`.`OrderID`) and (`r`.`item_name` = 'AUD-CRAM')) limit 1) AS `AUD-CRAM`,(select (`r`.`revenue` / 100) from `rcpar_data_analysis_helper_legacy_order_item_revenue` `r` where ((`r`.`order_id` = `lmo`.`OrderID`) and (`r`.`item_name` = 'FAR-CRAM')) limit 1) AS `FAR-CRAM`,(select (`r`.`revenue` / 100) from `rcpar_data_analysis_helper_legacy_order_item_revenue` `r` where ((`r`.`order_id` = `lmo`.`OrderID`) and (`r`.`item_name` = 'BEC-CRAM')) limit 1) AS `BEC-CRAM`,(select (`r`.`revenue` / 100) from `rcpar_data_analysis_helper_legacy_order_item_revenue` `r` where ((`r`.`order_id` = `lmo`.`OrderID`) and (`r`.`item_name` = 'REG-CRAM')) limit 1) AS `REG-CRAM`,(select (`r`.`revenue` / 100) from `rcpar_data_analysis_helper_legacy_order_item_revenue` `r` where ((`r`.`order_id` = `lmo`.`OrderID`) and (`r`.`item_name` = 'AUD-EFC')) limit 1) AS `AUD-EFC`,(select (`r`.`revenue` / 100) from `rcpar_data_analysis_helper_legacy_order_item_revenue` `r` where ((`r`.`order_id` = `lmo`.`OrderID`) and (`r`.`item_name` = 'FAR-EFC')) limit 1) AS `FAR-EFC`,(select (`r`.`revenue` / 100) from `rcpar_data_analysis_helper_legacy_order_item_revenue` `r` where ((`r`.`order_id` = `lmo`.`OrderID`) and (`r`.`item_name` = 'BEC-EFC')) limit 1) AS `BEC-EFC`,(select (`r`.`revenue` / 100) from `rcpar_data_analysis_helper_legacy_order_item_revenue` `r` where ((`r`.`order_id` = `lmo`.`OrderID`) and (`r`.`item_name` = 'REG-EFC')) limit 1) AS `REG-EFC`,(select (`r`.`revenue` / 100) from `rcpar_data_analysis_helper_legacy_order_item_revenue` `r` where ((`r`.`order_id` = `lmo`.`OrderID`) and (`r`.`item_name` = 'AUD-OFF-LEC')) limit 1) AS `AUD-OFF-LEC`,(select (`r`.`revenue` / 100) from `rcpar_data_analysis_helper_legacy_order_item_revenue` `r` where ((`r`.`order_id` = `lmo`.`OrderID`) and (`r`.`item_name` = 'FAR-OFF-LEC')) limit 1) AS `FAR-OFF-LEC`,(select (`r`.`revenue` / 100) from `rcpar_data_analysis_helper_legacy_order_item_revenue` `r` where ((`r`.`order_id` = `lmo`.`OrderID`) and (`r`.`item_name` = 'BEC-OFF-LEC')) limit 1) AS `BEC-OFF-LEC`,(select (`r`.`revenue` / 100) from `rcpar_data_analysis_helper_legacy_order_item_revenue` `r` where ((`r`.`order_id` = `lmo`.`OrderID`) and (`r`.`item_name` = 'REG-OFF-LEC')) limit 1) AS `REG-OFF-LEC`,if((`lmo`.`PartnerID` > 0),'B2B','B2C') AS `Channel`,`o_total`.`Tax` AS `taxes`,`o_total`.`Shipping` AS `shipping`,`o_total`.`Total` AS `OrderTotal` from (`legacy_merchandiseorders` `lmo` join `legacy_merchandiseorderstotals` `o_total` on((`o_total`.`OrderID` = `lmo`.`OrderID`))) */;
-- /*!50001 SET character_set_client      = @saved_cs_client */;
-- /*!50001 SET character_set_results     = @saved_cs_results */;
-- /*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `pass4`
--

/*!50001 DROP VIEW IF EXISTS `pass4`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8mb4 */;
/*!50001 SET character_set_results     = utf8mb4 */;
/*!50001 SET collation_connection      = utf8mb4_general_ci */;
/*!50001 CREATE ALGORITHM=TEMPTABLE */
/*!50013  SQL SECURITY DEFINER */
/*!50001 VIEW `pass4` AS select `attempts`.`UID` AS `UID`,`attempts`.`First Exam Taken` AS `First Exam Taken`,`attempts`.`Last Exam Taken` AS `Last Exam Taken`,`attempts`.`Avg Score` AS `Avg Score`,`attempts`.`Attempts` AS `Attempts` from `attempts` where (`attempts`.`Attempts` >= 4) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `products`
--

/*!50001 DROP VIEW IF EXISTS `products`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8mb4 */;
/*!50001 SET character_set_results     = utf8mb4 */;
/*!50001 SET collation_connection      = utf8mb4_general_ci */;
/*!50001 CREATE ALGORITHM=TEMPTABLE */
/*!50013  SQL SECURITY DEFINER */
/*!50001 VIEW `products` AS SELECT
                              o.uid as UserId,
                              o.order_id as OrderId,
                              o.mail,
                              from_Unixtime(trans.changed) as `Date`,
                              Year(from_Unixtime(trans.changed)) as `Year`,
                              Month(from_Unixtime(trans.changed)) as `Month`,
                              graduated.field_did_you_graduate_college_value as graduated,
                              if(trans.payment_method = "authnet_aim", "Direct", if(trans.payment_method = "rcpar_partners_payment", "Partner", "Affirm")) as `Payment Method`,
                              if(trans.payment_method = "authnet_aim", "B2C", if(trans.payment_method = "rcpar_partners_payment", "B2B", "B2C")) as `Channel`,
                              (SELECT revenue / 100 FROM revenue WHERE revenue.entity_id = o.order_id AND revenue.line_item_label = 'FULL-DIS' ) as `Select`,
                              (SELECT revenue / 100 FROM revenue WHERE revenue.entity_id = o.order_id AND revenue.line_item_label = 'FULL-PREM-DIS' ) as `Premier`,
                              (SELECT revenue / 100 FROM revenue WHERE revenue.entity_id = o.order_id AND revenue.line_item_label = 'FULL-PREM-AUD-DIS' ) as `Premier-AUD`,
                              (SELECT revenue / 100 FROM revenue WHERE revenue.entity_id = o.order_id AND revenue.line_item_label = 'FULL-PREM-EFC-DIS' ) as `Premier-EFC`,
                              (SELECT revenue / 100 FROM revenue WHERE revenue.entity_id = o.order_id AND revenue.line_item_label = 'FULL-ELITE-DIS' ) as Elite,
                              (SELECT revenue / 100 FROM revenue WHERE revenue.entity_id = o.order_id AND revenue.line_item_label = 'AUD' ) as AUD,
                              (SELECT revenue / 100 FROM revenue WHERE revenue.entity_id = o.order_id AND revenue.line_item_label = 'FAR' ) as FAR,
                              (SELECT revenue / 100 FROM revenue WHERE revenue.entity_id = o.order_id AND revenue.line_item_label = 'BEC' ) as BEC,
                              (SELECT revenue / 100 FROM revenue WHERE revenue.entity_id = o.order_id AND revenue.line_item_label = 'REG' ) as REG,
                              (SELECT revenue / 100 FROM revenue WHERE revenue.entity_id = o.order_id AND revenue.line_item_label = 'AUD-6MEXT' ) as `AUD-6MEXT`,
                              (SELECT revenue / 100 FROM revenue WHERE revenue.entity_id = o.order_id AND revenue.line_item_label = 'FAR-6MEXT' ) as `FAR-6MEXT`,
                              (SELECT revenue / 100 FROM revenue WHERE revenue.entity_id = o.order_id AND revenue.line_item_label = 'BEC-6MEXT' ) as `BEC-6MEXT`,
                              (SELECT revenue / 100 FROM revenue WHERE revenue.entity_id = o.order_id AND revenue.line_item_label = 'REG-6MEXT' ) as `REG-6MEXT`,
                              (SELECT revenue / 100 FROM revenue WHERE revenue.entity_id = o.order_id AND revenue.line_item_label = 'FULL-6MEXT' ) as `FULL-6MEXT`,
                              (SELECT revenue / 100 FROM revenue WHERE revenue.entity_id = o.order_id AND revenue.line_item_label = 'AUD-AUD' ) as `AUD-AUD`,
                              (SELECT revenue / 100 FROM revenue WHERE revenue.entity_id = o.order_id AND revenue.line_item_label = 'FAR-AUD' ) as `FAR-AUD`,
                              (SELECT revenue / 100 FROM revenue WHERE revenue.entity_id = o.order_id AND revenue.line_item_label = 'BEC-AUD' ) as `BEC-AUD`,
                              (SELECT revenue / 100 FROM revenue WHERE revenue.entity_id = o.order_id AND revenue.line_item_label = 'REG-AUD' ) as `REG-AUD`,
                              (SELECT revenue / 100 FROM revenue WHERE revenue.entity_id = o.order_id AND revenue.line_item_label = 'AUD-CRAM' ) as `AUD-CRAM`,
                              (SELECT revenue / 100 FROM revenue WHERE revenue.entity_id = o.order_id AND revenue.line_item_label = 'FAR-CRAM' ) as `FAR-CRAM`,
                              (SELECT revenue / 100 FROM revenue WHERE revenue.entity_id = o.order_id AND revenue.line_item_label = 'BEC-CRAM' ) as `BEC-CRAM`,
                              (SELECT revenue / 100 FROM revenue WHERE revenue.entity_id = o.order_id AND revenue.line_item_label = 'REG-CRAM' ) as `REG-CRAM`,
                              (SELECT revenue / 100 FROM revenue WHERE revenue.entity_id = o.order_id AND revenue.line_item_label = 'AUD-SSD' ) as `AUD-SSD`,
                              (SELECT revenue / 100 FROM revenue WHERE revenue.entity_id = o.order_id AND revenue.line_item_label = 'FAR-SSD' ) as `FAR-SSD`,
                              (SELECT revenue / 100 FROM revenue WHERE revenue.entity_id = o.order_id AND revenue.line_item_label = 'BEC-SSD' ) as `BEC-SSD`,
                              (SELECT revenue / 100 FROM revenue WHERE revenue.entity_id = o.order_id AND revenue.line_item_label = 'REG-SSD' ) as `REG-SSD`,
                              (SELECT revenue / 100 FROM revenue WHERE revenue.entity_id = o.order_id AND revenue.line_item_label = 'AUD-EFC' ) as `AUD-EFC`,
                              (SELECT revenue / 100 FROM revenue WHERE revenue.entity_id = o.order_id AND revenue.line_item_label = 'FAR-EFC' ) as `FAR-EFC`,
                              (SELECT revenue / 100 FROM revenue WHERE revenue.entity_id = o.order_id AND revenue.line_item_label = 'BEC-EFC' ) as `BEC-EFC`,
                              (SELECT revenue / 100 FROM revenue WHERE revenue.entity_id = o.order_id AND revenue.line_item_label = 'REG-EFC' ) as `REG-EFC`,
                              (SELECT revenue / 100 FROM revenue WHERE revenue.entity_id = o.order_id AND revenue.line_item_label = 'AUD-OFF-LEC' ) as `AUD-OFF-LEC`,
                              (SELECT revenue / 100 FROM revenue WHERE revenue.entity_id = o.order_id AND revenue.line_item_label = 'FAR-OFF-LEC' ) as `FAR-OFF-LEC`,
                              (SELECT revenue / 100 FROM revenue WHERE revenue.entity_id = o.order_id AND revenue.line_item_label = 'BEC-OFF-LEC' ) as `BEC-OFF-LEC`,
                              (SELECT revenue / 100 FROM revenue WHERE revenue.entity_id = o.order_id AND revenue.line_item_label = 'REG-OFF-LEC' ) as `REG-OFF-LEC`,
                              (SELECT revenue / 100 FROM revenue WHERE revenue.entity_id = o.order_id AND revenue.line_item_label = 'CRAM-CERT' ) as `CRAM-CERT`,
                              partner_dis_line_total.commerce_total_amount/100 as `PARTNER-DIS`,
                              cct.label as `Promotion Type`,
                              fdccc.commerce_coupon_code_value as `Promotion`,
                              tax_line_total.commerce_total_amount/100 as taxes,
                              shipping_line_total.commerce_total_amount/100 as shipping,
                              (SELECT SUM(tr.amount)/100 FROM commerce_payment_transaction tr WHERE tr.order_id = o.order_id AND tr.status = 'Success' AND tr.payment_method <> 'rcpar_partners_payment_freetrial' AND tr.amount > 0) as OrderTotal,
                              country_olaf.COUNTRY as `Country`,
                              if(state_olaf.State is null, "International", state_olaf.State) as `State`,
                              if(state_olaf.State is null, "Intl", state_olaf.`State Abbrev`) as `St`,
                              address.commerce_customer_address_postal_code as `Zip`,
                              if(territory_olaf.Market is null, "International", territory_olaf.Market) as `Market`,
                              if(territory_olaf.Region is null, "International", territory_olaf.Region) as `Region`

                            FROM commerce_order o
                              INNER JOIN field_data_commerce_line_items cli_data ON cli_data.entity_type = 'commerce_order' AND cli_data.entity_id = o.order_id
                              INNER JOIN commerce_line_item cli ON cli.line_item_id = cli_data.commerce_line_items_line_item_id
                              INNER JOIN rcpar_data_analysis_helper_item_line_revenue oli ON oli.line_item_id = cli.line_item_id
                              INNER JOIN field_data_commerce_customer_billing billing_prof ON o.order_id = billing_prof.entity_id AND billing_prof.entity_type = 'commerce_order'
                              INNER JOIN commerce_customer_profile prof ON prof.profile_id = billing_prof.commerce_customer_billing_profile_id AND prof.type = 'billing'
                              LEFT JOIN users u ON prof.uid = u.uid
                              LEFT JOIN field_data_commerce_customer_address address ON address.entity_id = prof.profile_id AND address.entity_type = 'commerce_customer_profile'
                              LEFT JOIN field_data_field_did_you_graduate_college graduated ON graduated.entity_id = prof.profile_id AND graduated.entity_type = 'commerce_customer_profile'
                              LEFT JOIN commerce_line_item tax_line ON tax_line.order_id = o.order_id AND tax_line.type = 'avatax'
                                                                       AND EXISTS ( SELECT * FROM field_data_commerce_line_items cli_data WHERE cli_data.commerce_line_items_line_item_id = tax_line.line_item_id AND cli_data.entity_id = o.order_id )
                              LEFT JOIN field_data_commerce_total tax_line_total ON tax_line_total.entity_type = 'commerce_line_item' AND tax_line_total.entity_id = tax_line.line_item_id
                              LEFT JOIN commerce_line_item shipping_line ON shipping_line.order_id = o.order_id AND shipping_line.type = 'shipping'
                                                                            AND EXISTS ( SELECT * FROM field_data_commerce_line_items cli_data WHERE cli_data.commerce_line_items_line_item_id = shipping_line.line_item_id AND cli_data.entity_id = o.order_id )
                              LEFT JOIN field_data_commerce_total shipping_line_total ON shipping_line_total.entity_type = 'commerce_line_item' AND shipping_line_total.entity_id = shipping_line.line_item_id
                              LEFT JOIN commerce_line_item partner_dis_line ON partner_dis_line.order_id = o.order_id AND partner_dis_line.line_item_label = 'PARTNER-DIS'
                                                                               AND EXISTS ( SELECT * FROM field_data_commerce_line_items cli_data WHERE cli_data.commerce_line_items_line_item_id = partner_dis_line.line_item_id AND cli_data.entity_id = o.order_id )
                              LEFT JOIN field_data_commerce_total partner_dis_line_total ON partner_dis_line_total.entity_type = 'commerce_line_item' AND partner_dis_line_total.entity_id = partner_dis_line.line_item_id
                              INNER JOIN field_data_commerce_customer_shipping shipping_prof ON o.order_id = shipping_prof.entity_id AND shipping_prof.entity_type = 'commerce_order'
                              INNER JOIN commerce_customer_profile customer_prof ON customer_prof.profile_id = shipping_prof.commerce_customer_shipping_profile_id AND customer_prof.type = 'shipping'
                              LEFT JOIN field_data_commerce_customer_address shp_address ON shp_address.entity_id = prof.profile_id AND shp_address.entity_type = 'commerce_customer_profile'
                              LEFT JOIN country_olaf ON shp_address.commerce_customer_address_country = country_olaf.`A2 (ISO)`
                              LEFT JOIN state_olaf ON shp_address.commerce_customer_address_administrative_area = state_olaf.`State Abbrev`
                              LEFT JOIN territory_olaf ON shp_address.commerce_customer_address_administrative_area = territory_olaf.State
                              LEFT JOIN commerce_payment_transaction trans ON trans.order_id = o.order_id
                              left join field_data_commerce_coupon_order_reference fdccor on fdccor.entity_id = o.order_id
                              left join field_data_commerce_coupon_code fdccc on fdccc.entity_id = fdccor.commerce_coupon_order_reference_target_id
                              left join commerce_coupon_type cct on cct.`type` = fdccc.bundle
                            WHERE trans.status = 'Success' AND trans.payment_method <> 'rcpar_partners_payment_freetrial' AND trans.amount > 0
                            GROUP BY o.order_id
  # NOTE: we are grouping the rows by order id because we might have several transactions per order

  */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `revenue`
--

/*!50001 DROP VIEW IF EXISTS `revenue`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8mb4 */;
/*!50001 SET character_set_results     = utf8mb4 */;
/*!50001 SET collation_connection      = utf8mb4_general_ci */;
/*!50001 CREATE ALGORITHM=TEMPTABLE */
/*!50013  SQL SECURITY DEFINER */
/*!50001 VIEW `revenue` AS (select `cli_data`.`entity_id` AS `entity_id`,`revenue`.`revenue` AS `revenue`,`cli`.`line_item_label` AS `line_item_label` from ((`field_data_commerce_line_items` `cli_data` join `commerce_line_item` `cli` on((`cli`.`line_item_id` = `cli_data`.`commerce_line_items_line_item_id`))) join `rcpar_data_analysis_helper_item_line_revenue` `revenue` on((`revenue`.`line_item_id` = `cli_data`.`commerce_line_items_line_item_id`))) where (`cli_data`.`entity_type` = 'commerce_order')) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `scores`
--

/*!50001 DROP VIEW IF EXISTS `scores`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8mb4 */;
/*!50001 SET character_set_results     = utf8mb4 */;
/*!50001 SET collation_connection      = utf8mb4_general_ci */;
/*!50001 CREATE ALGORITHM=TEMPTABLE */
/*!50013  SQL SECURITY DEFINER */
/*!50001 VIEW `scores` AS select `eck_exam_scores`.`id` AS `id`,`eck_exam_scores`.`attempt` AS `Attempt`,`eck_exam_scores`.`uid` AS `UID`,cast(from_unixtime(`eck_exam_scores`.`created`) as date) AS `Score Entered`,cast(from_unixtime(`eck_exam_scores`.`exam_date`) as date) AS `Exam Taken`,year(from_unixtime(`eck_exam_scores`.`exam_date`)) AS `Exam Year`,`eck_exam_scores`.`score` AS `Score`,if((`eck_exam_scores`.`score` > 74),'Pass','Fail') AS `Pass/Fail`,`eck_exam_scores`.`section` AS `Section`,`country_olaf`.`COUNTRY` AS `Country`,if(isnull(`state_olaf`.`State`),'International',`state_olaf`.`State`) AS `State`,if(isnull(`state_olaf`.`State`),'Intl',`state_olaf`.`State Abbrev`) AS `St`,if(isnull(`territory_olaf`.`Market`),'International',`territory_olaf`.`Market`) AS `Market`,if(isnull(`territory_olaf`.`Region`),'International',`territory_olaf`.`Region`) AS `Region`,`did_grad`.`field_did_you_graduate_college_value` AS `US College`,`coll_state_olaf`.`State` AS `Campus State`,`t`.`name` AS `College Name`,if((from_unixtime(`eck_exam_scores`.`exam_date`) >= '2017-04-01'),'New Exam','Old Exam') AS `Exam Type` from (((((((((((((`eck_exam_scores` join `users` `u` on((`u`.`uid` = `eck_exam_scores`.`uid`))) left join `commerce_order` `o` on((`o`.`order_id` = (select max(`o2`.`order_id`) from `commerce_order` `o2` where ((`o2`.`uid` = `u`.`uid`) and (`o2`.`status` in ('completed','shipped'))))))) left join `field_data_commerce_customer_billing` `billing_prof` on(((`o`.`order_id` = `billing_prof`.`entity_id`) and (`billing_prof`.`entity_type` = 'commerce_order')))) left join `commerce_customer_profile` `prof` on(((`prof`.`profile_id` = `billing_prof`.`commerce_customer_billing_profile_id`) and (`prof`.`type` = 'billing')))) left join `field_data_commerce_customer_address` `address` on(((`address`.`entity_id` = `prof`.`profile_id`) and (`address`.`entity_type` = 'commerce_customer_profile')))) left join `country_olaf` on((`address`.`commerce_customer_address_country` = `country_olaf`.`A2 (ISO)`))) left join `state_olaf` on((`address`.`commerce_customer_address_administrative_area` = `state_olaf`.`State Abbrev`))) left join `territory_olaf` on((`address`.`commerce_customer_address_administrative_area` = convert(`territory_olaf`.`State` using utf8)))) left join `field_data_field_did_you_graduate_college` `did_grad` on(((`did_grad`.`entity_id` = `u`.`uid`) and (`did_grad`.`entity_type` = 'user')))) left join `field_data_field_college_state_list` `coll_state` on(((`coll_state`.`entity_id` = `u`.`uid`) and (`coll_state`.`entity_type` = 'user')))) left join `state_olaf` `coll_state_olaf` on((`coll_state`.`field_college_state_list_value` = `coll_state_olaf`.`State Abbrev`))) left join `field_data_field_college_state` `coll_name_tid` on(((`coll_name_tid`.`entity_id` = `u`.`uid`) and (`coll_name_tid`.`entity_type` = 'user')))) left join `taxonomy_term_data` `t` on((`t`.`tid` = `coll_name_tid`.`field_college_state_tid`))) where ((not(exists(select 1 from (`users_roles` `ur` join `role` `r` on((`r`.`rid` = `ur`.`rid`))) where ((`ur`.`uid` = `u`.`uid`) and (`r`.`name` in ('administrator','customer support','editor','field member','IPQ editor','moderator')))))) and (from_unixtime(`eck_exam_scores`.`exam_date`) >= '2014-01-01')) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

--
-- Final view structure for view `fsl_conversions`
--

/*!50001 DROP VIEW IF EXISTS `fsl_conversions`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8mb4 */;
/*!50001 SET character_set_results     = utf8mb4 */;
/*!50001 SET collation_connection      = utf8mb4_general_ci */;
/*!50001 CREATE ALGORITHM=TEMPTABLE */
/*!50013 SQL SECURITY DEFINER */
/*!50001 VIEW `fsl_conversions` AS select `fsl`.`User` AS `User`,`fsl`.`Email` AS `Email`,`fsl`.`Firm` AS `Firm`,`fsl`.`Enroll Date` AS `Enroll Date`,`fsl`.`Order ID` AS `Order ID`,if(isnull(`up`.`Package`),'Other',`up`.`Package`) AS `Package`,if((max(`all`.`Date`) > `fsl`.`Enroll Date`),max(`all`.`Order ID`),'') AS `Conv`,if((max(`all`.`Date`) > `fsl`.`Enroll Date`),max(`all`.`Date`),'') AS `Conv Date`,if(((to_days(max(`all`.`Date`)) - to_days(max(`fsl`.`Enroll Date`))) < 0),'',(to_days(max(`all`.`Date`)) - to_days(max(`fsl`.`Enroll Date`)))) AS `Conv Time`,(case when ((to_days(max(`all`.`Date`)) - to_days(max(`fsl`.`Enroll Date`))) between 1 and 14) then '2 Weeks' when ((to_days(max(`all`.`Date`)) - to_days(max(`fsl`.`Enroll Date`))) between 15 and 31) then '4 Weeks' when ((to_days(max(`all`.`Date`)) - to_days(max(`fsl`.`Enroll Date`))) between 32 and 46) then '6 Weeks' when ((to_days(max(`all`.`Date`)) - to_days(max(`fsl`.`Enroll Date`))) between 47 and 62) then '8 Weeks' when ((to_days(max(`all`.`Date`)) - to_days(max(`fsl`.`Enroll Date`))) > 63) then '9 Weeks+' else '' end) AS `Conv Period` from ((`fsl_enrollees` `fsl` left join `all` on((`all`.`User` = `fsl`.`User`))) left join `user_package` `up` on((`up`.`uid` = `fsl`.`User`))) where ((`fsl`.`Enroll Date` >= '2017-01-01') and (not((`fsl`.`Email` like '%test%'))) and (`fsl`.`User` not in (182216,252306))) group by `fsl`.`User` order by `fsl`.`Enroll Date` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `fsl_enrollees`
--

/*!50001 DROP VIEW IF EXISTS `fsl_enrollees`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8mb4 */;
/*!50001 SET character_set_results     = utf8mb4 */;
/*!50001 SET collation_connection      = utf8mb4_general_ci */;
/*!50001 CREATE ALGORITHM=TEMPTABLE */
/*!50013 SQL SECURITY DEFINER */
/*!50001 VIEW `fsl_enrollees` AS select `u`.`uid` AS `User`,`u`.`mail` AS `Email`,`o`.`order_id` AS `Order ID`,`p`.`title` AS `Firm`,cast(from_unixtime(`o`.`created`) as date) AS `Enroll Date` from (((((`commerce_order` `o` join `users` `u` on((`u`.`uid` = `o`.`uid`))) join `field_data_field_partner_profile` `pprof` on(((`pprof`.`entity_id` = `o`.`order_id`) and (`pprof`.`entity_type` = 'commerce_order')))) join `field_data_field_partner_type` `pt` on(((`pt`.`entity_id` = `pprof`.`field_partner_profile_target_id`) and (`pt`.`entity_type` = 'node')))) join `field_data_field_billing_type` `bt` on(((`bt`.`entity_id` = `pprof`.`field_partner_profile_target_id`) and (`bt`.`entity_type` = 'node')))) join `node` `p` on((`p`.`nid` = `pprof`.`field_partner_profile_target_id`))) where ((`o`.`status` in ('Pending','Processing','Completed','Shipped')) and (`u`.`uid` is not null) and (`pt`.`field_partner_type_value` = 1) and (`bt`.`field_billing_type_value` = 'freetrial')) group by `u`.`uid` order by `o`.`created` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `hhc`
--

/*!50001 DROP VIEW IF EXISTS `hhc`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=TEMPTABLE */
/*!50013 SQL SECURITY DEFINER */
/*!50001 VIEW `hhc` AS select `n`.`uid` AS `uid`,`u`.`mail` AS `User`,if(isnull(`up`.`Package`),'Individual',`up`.`Package`) AS `package`,if((`g`.`User Group` is not null),`g`.`User Group`,'No Questions') AS `User Group`,`c`.`cid` AS `cid`,(case when (`c`.`name` = 'Moderator 1') then 'Rick' when (`c`.`name` = 'Moderator 2') then 'Aaron' when (`c`.`name` = 'Moderator 3') then 'Jae' when (`c`.`name` = 'Moderator 5') then 'Wendy' when (`c`.`name` = 'Moderator 7') then 'Eva' when (`c`.`name` = 'HHC Administrator') then 'HHC Admin' else '' end) AS `Moderator`,cast(addtime(from_unixtime(`n`.`created`),'2:00') as date) AS `Date`,cast(addtime(from_unixtime(`c`.`changed`),'2:00') as date) AS `Answer Date`,`c`.`thread` AS `thread`,year(addtime(from_unixtime(`n`.`created`),'2:00')) AS `Year`,month(addtime(from_unixtime(`n`.`created`),'2:00')) AS `Month`,dayofmonth(addtime(from_unixtime(`n`.`created`),'2:00')) AS `Day`,timediff(addtime(from_unixtime(`c`.`changed`),'2:00'),addtime(from_unixtime(`n`.`created`),'2:00')) AS `Timediff`,`n`.`title` AS `title`,`prefix`.`field_prefix_value` AS `Section/Chapter`,left(`prefix`.`field_prefix_value`,3) AS `Section`,`q`.`body_value` AS `Moderator Question`,`b`.`comment_body_value` AS `comment_body_value` from ((((((((`node` `n` left join `comment` `c` on((`n`.`nid` = `c`.`nid`))) left join `field_data_comment_body` `b` on((`b`.`entity_id` = `c`.`cid`))) left join `field_data_body` `q` on((`q`.`entity_id` = `n`.`nid`))) left join `field_data_field_course_chapter_hhc` `ch` on((`n`.`nid` = `ch`.`entity_id`))) left join `field_data_field_prefix` `prefix` on((`prefix`.`entity_id` = `ch`.`field_course_chapter_hhc_value`))) left join `users` `u` on((`u`.`uid` = `n`.`uid`))) left join `user_package` `up` on((`up`.`uid` = `n`.`uid`))) left join `hhc_user_groups` `g` on((`g`.`uid` = `n`.`uid`))) where (`n`.`type` = 'helpcenter_question') order by `n`.`uid` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `hhc_user_groups`
--

/*!50001 DROP VIEW IF EXISTS `hhc_user_groups`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8mb4 */;
/*!50001 SET character_set_results     = utf8mb4 */;
/*!50001 SET collation_connection      = utf8mb4_general_ci */;
/*!50001 CREATE ALGORITHM=TEMPTABLE */
/*!50013 SQL SECURITY DEFINER */
/*!50001 VIEW `hhc_user_groups` AS select `n`.`uid` AS `uid`,count(`n`.`uid`) AS `Count`,(case when (count(`n`.`uid`) < 1) then 'No Questions' when (count(`n`.`uid`) between 1 and 49) then '1-49 Normal Users' when (count(`n`.`uid`) between 50 and 99) then '50-99 Light Users' when (count(`n`.`uid`) between 100 and 199) then '100-199 Heavy Users' when (count(`n`.`uid`) > 199) then '200+ Super Users' else '' end) AS `User Group` from (((`node` `n` left join `comment` `c` on((`n`.`nid` = `c`.`nid`))) left join `field_data_comment_body` `b` on((`b`.`entity_id` = `c`.`cid`))) left join `users` `u` on((`u`.`uid` = `n`.`uid`))) where (`n`.`type` = 'helpcenter_question') group by `n`.`uid` order by 'Count' desc */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `user_package`
--

/*!50001 DROP VIEW IF EXISTS `user_package`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8mb4 */;
/*!50001 SET character_set_results     = utf8mb4 */;
/*!50001 SET collation_connection      = utf8mb4_general_ci */;
/*!50001 CREATE ALGORITHM=TEMPTABLE */
/*!50013 SQL SECURITY DEFINER */
/*!50001 VIEW `user_package` AS select `co`.`uid` AS `uid`,`cli`.`order_id` AS `order_id`,(case when (`cli`.`line_item_label` = 'FULL-ELITE-DIS') then 'Elite' when (`cli`.`line_item_label` = 'FULL-PREM-DIS') then 'Premium' when (`cli`.`line_item_label` = 'FULL-PREM-EFC-DIS') then 'Premium' when (`cli`.`line_item_label` = 'FULL-PREM-AUD-DIS') then 'Premium' when (`cli`.`line_item_label` = 'FULL-DIS') then 'Select' end) AS `Package` from ((`commerce_line_item` `cli` left join `commerce_order` `co` on((`co`.`order_id` = `cli`.`order_id`))) left join `commerce_payment_transaction` `trans` on((`trans`.`order_id` = `co`.`order_id`))) where ((`cli`.`line_item_label` in ('FULL-ELITE-DIS','FULL-PREM-DIS','FULL-DIS','FULL-PREM-EFC-DIS','FULL-PREM-EFC-DIS')) and (`co`.`status` in ('Pending','Processing','completed','shipped')) and (`trans`.`payment_method` <> 'rcpar_partners_payment_freetrial')) group by `cli`.`order_id` desc */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;


/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-02-27 12:09:26

