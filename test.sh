#!/bin/bash


backup_file=$(date +"prod-rogercpa-rogercpa-%Y-%m-%d.sql.gz")

# Bring the most recent .sql.gz file in the acquia prod backups
echo ssh rogercpa.prod@web-18483.prod.hosting.acquia.com "find /mnt/gfs/home/rogercpa/prod/backups/ -type f -name $backup_file"
