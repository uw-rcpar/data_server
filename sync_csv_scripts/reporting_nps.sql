SELECT u.uid, 
  FROM_UNIXTIME(nps.`date`, '%Y-%m-%d') AS create_date,
  score score, purchaser_type, skus, IFNULL(comment, '') as comment
FROM rcpar_nps_log nps
LEFT join users u ON nps.uid = u.uid
WHERE 
 u.mail NOT LIKE "%rcparlabs.com"
#REMOVE_ON_FULL_EXPORT#
 AND nps.`date` >= '#FROM_DATE#'
#@REMOVE_ON_FULL_EXPORT#
