SET time_zone = '-7:00';

SELECT 
`n`.`uid` AS `uid`,
count(`n`.`uid`) AS `count`,
(
  case 
  when (count(`n`.`uid`) < 1) then 
    'No Questions' 
  when (count(`n`.`uid`) between 1 and 49) then 
    '1-49 Normal Users' 
  when (count(`n`.`uid`) between 50 and 99) then 
    '50-99 Light Users' 
  when (count(`n`.`uid`) between 100 and 199) then 
    '100-199 Heavy Users' 
  when (count(`n`.`uid`) > 199) then 
    '200+ Super Users' else '' end
) AS `user_group`
FROM
`node` `n` 
  LEFT JOIN `comment` `c` ON `n`.`nid` = `c`.`nid`
  LEFT JOIN `field_data_comment_body` `b` ON `b`.`entity_id` = `c`.`cid`
  LEFT JOIN `users` `u` ON `u`.`uid` = `n`.`uid`
WHERE
 `n`.`type` = 'helpcenter_question'
  AND EXISTS
  (
    SELECT * 
	FROM `node` n2 
	LEFT JOIN `comment` c2 ON n2.`nid` = c2.`nid` 
    WHERE n2.uid = `n`.`uid` AND n2.`type` = 'helpcenter_question'
#REMOVE_ON_FULL_EXPORT#
      AND	
    (c2.`created` >= '#FROM_DATE#' OR c2.`changed` >= '#FROM_DATE#')
      OR
    (n2.`created` >= '#FROM_DATE#' OR n2.`changed` >= '#FROM_DATE#')
#@REMOVE_ON_FULL_EXPORT#
  )
GROUP BY `n`.`uid`
ORDER BY 'Count' DESC;
