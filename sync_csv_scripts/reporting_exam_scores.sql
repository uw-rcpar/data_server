
SET time_zone = '-7:00';

SELECT 
  `eck_exam_scores`.`id` AS `exam_scores_id`,
  `eck_exam_scores`.`attempt` AS `attempt`,
  `eck_exam_scores`.`uid` AS `uid`,
  cast(from_unixtime(`eck_exam_scores`.`created`) as date) AS `score_entered`,
  cast(from_unixtime(`eck_exam_scores`.`exam_date`) as date) AS `exam_taken`,
  year(from_unixtime(`eck_exam_scores`.`exam_date`)) AS `exam_year`,
  `eck_exam_scores`.`score` AS `score`,
  if((`eck_exam_scores`.`score` > 74),'Pass','Fail') AS `pass_fail`,
  `eck_exam_scores`.`section` AS `section`,
  `country_olaf`.`COUNTRY` AS `country`,
  if(isnull(`state_olaf`.`State`),'International',`state_olaf`.`State`) AS `state`,
  if(isnull(`state_olaf`.`State`),'Intl',`state_olaf`.`State Abbrev`) AS `st`,
  if(isnull(`territory_olaf`.`Market`),'International',`territory_olaf`.`Market`) AS `market`,
  if(isnull(`territory_olaf`.`Region`),'International',`territory_olaf`.`Region`) AS `region`,
  `did_grad`.`field_did_you_graduate_college_value` AS `us_college`,
  `coll_state_olaf`.`State` AS `campus_state`,
  `t`.`name` AS `college_name`,
  if((from_unixtime(`eck_exam_scores`.`exam_date`) >= '2017-04-01'),'POST-04-2017','PRE-04-2017') AS `exam_type` 
FROM 
    `eck_exam_scores` 
    JOIN `users` `u` on(`u`.`uid` = `eck_exam_scores`.`uid`) 	 
    LEFT JOIN `commerce_order` `o` ON `o`.`order_id` = ( select max(`o2`.`order_id`)  FROM `commerce_order` `o2` WHERE `o2`.`uid` = `u`.`uid` AND `o2`.`status` IN('completed','shipped') )	
    LEFT JOIN `field_data_commerce_customer_billing` `billing_prof` ON `o`.`order_id` = `billing_prof`.`entity_id` AND `billing_prof`.`entity_type` = 'commerce_order'       
    LEFT JOIN `commerce_customer_profile` `prof` ON `prof`.`profile_id` = `billing_prof`.`commerce_customer_billing_profile_id` AND `prof`.`type` = 'billing'
    LEFT JOIN `field_data_commerce_customer_address` `address` ON `address`.`entity_id` = `prof`.`profile_id` AND `address`.`entity_type` = 'commerce_customer_profile'
    LEFT JOIN `country_olaf` ON `address`.`commerce_customer_address_country` = `country_olaf`.`A2 (ISO)`
    LEFT JOIN `state_olaf` ON `address`.`commerce_customer_address_administrative_area` = `state_olaf`.`State Abbrev`
    LEFT JOIN `territory_olaf` ON `address`.`commerce_customer_address_administrative_area` = convert(`territory_olaf`.`State` using utf8)
    LEFT JOIN `field_data_field_did_you_graduate_college` `did_grad` ON `did_grad`.`entity_id` = `u`.`uid` AND `did_grad`.`entity_type` = 'user'
    LEFT JOIN `field_data_field_college_state_list` `coll_state` ON `coll_state`.`entity_id` = `u`.`uid` AND `coll_state`.`entity_type` = 'user'
    LEFT JOIN `state_olaf` `coll_state_olaf` ON `coll_state`.`field_college_state_list_value` = `coll_state_olaf`.`State Abbrev`
    LEFT JOIN `field_data_field_college_state` `coll_name_tid` ON `coll_name_tid`.`entity_id` = `u`.`uid` AND `coll_name_tid`.`entity_type` = 'user'
    LEFT JOIN `taxonomy_term_data` `t` ON `t`.`tid` = `coll_name_tid`.`field_college_state_tid`
WHERE 
      NOT EXISTS 
      (
          SELECT 1 FROM (`users_roles` `ur` JOIN `role` `r` ON `r`.`rid` = `ur`.`rid` )
	  WHERE `ur`.`uid` = `u`.`uid` AND `r`.`name` IN('administrator','customer support','editor','field member','IPQ editor','moderator')
      )
      AND from_unixtime(`eck_exam_scores`.`exam_date`) >= '2014-01-01'
#REMOVE_ON_FULL_EXPORT#     
 AND (`eck_exam_scores`.`created` >= '#FROM_DATE#' OR `eck_exam_scores`.`changed` >= '#FROM_DATE#')
#@REMOVE_ON_FULL_EXPORT#
;
