SET time_zone = '-7:00';

SELECT u.uid, u.mail, 

first_name.field_first_name_value AS first_name, last_name.field_last_name_value AS last_name, 
archive_uid.field_archive_uid_value AS legacy_id, u.name AS username, DATE(FROM_UNIXTIME(u.created)) AS registration_date , u.status,
GROUP_CONCAT(role.name SEPARATOR ',') AS roles, phone.field_phone_number_value AS phone,
(SELECT GROUP_CONCAT(field_courses_taught_value SEPARATOR ',') 
 FROM field_data_field_courses_taught courses_taught WHERE courses_taught.entity_id = u.uid AND courses_taught.entity_type = 'user') AS courses_taught, 
IF(field_did_you_graduate_college_value = 1, "Graduated", "Not Graduated") AS graduated,
college_state_list.field_college_state_list_value AS college_state,
IFNULL(sfa.title, college_name_text.field_college_name_value) AS college_name,
grad_date.field_graduation_month_and_year_value AS grad_date, when_plan_first_sit_exam.field_when_do_you_plan_to_start_value  AS first_exam_sit,
country.field_country_value AS user_profile_country, state.field_student_state_value AS user_profile_state, campus.field_campus_value AS campus,
aud_exam_date.field_aud_exam_date_value AS aud_exam_date, bec_exam_date.field_bec_exam_date_value AS bec_exam_date,
far_exam_date.field_far_exam_date_value AS far_exam_date, reg_exam_date.field_reg_exam_date_value AS reg_exam_date,
archive_synced.field_archive_synced_value archive_synced, sync_stage.field_sync_stage_value AS sync_stage, uwuid.field_uwuid_value as uw_uid,

IFNULL(ship_country_olaf.COUNTRY,'') AS ship_country,
IF(ship_state_olaf.State is null, 'International', ship_state_olaf.State) AS ship_state,
IFNULL(ship_address.commerce_customer_address_locality, '') AS ship_city,
IFNULL(ship_address.commerce_customer_address_dependent_locality, '') AS ship_dependent_locality, 
IFNULL(ship_address.commerce_customer_address_postal_code, '') AS ship_zip, 
IFNULL(ship_address.commerce_customer_address_thoroughfare, '') AS ship_thoroughfare, 
IFNULL(ship_address.commerce_customer_address_premise, '') AS ship_premise,

IFNULL(billing_country_olaf.COUNTRY,'') AS billing_country,
IF(billing_state_olaf.State is null, 'International', billing_state_olaf.State) AS billing_state,
IFNULL(billing_address.commerce_customer_address_locality,'') AS billing_city,
IFNULL(billing_address.commerce_customer_address_dependent_locality,'') AS billing_dependent_locality, 
IFNULL(billing_address.commerce_customer_address_postal_code,'') AS billing_zip, 
IFNULL(billing_address.commerce_customer_address_thoroughfare,'') AS billing_thoroughfare, 
IFNULL(billing_address.commerce_customer_address_premise,'') AS billing_premise

FROM users u

LEFT JOIN (
  SELECT prof.profile_id, prof.uid, prof.created, prof.changed FROM commerce_customer_profile prof WHERE prof.type = 'shipping'
  AND 
    NOT EXISTS(
      SELECT profile_id FROM commerce_customer_profile prof2 WHERE prof2.uid = prof.uid AND prof2.type = 'shipping' 
	AND prof2.profile_id > prof.profile_id
    )
) AS ship_prof ON ship_prof.uid = u.uid
LEFT JOIN field_data_commerce_customer_address ship_address ON ship_address.entity_id = ship_prof.profile_id AND ship_address.entity_type = 'commerce_customer_profile'
LEFT JOIN country_olaf ship_country_olaf ON ship_address.commerce_customer_address_country = ship_country_olaf.`A2 (ISO)`
LEFT JOIN state_olaf ship_state_olaf ON ship_address.commerce_customer_address_administrative_area = ship_state_olaf.`State Abbrev`

LEFT JOIN (
  SELECT prof.profile_id, prof.uid, prof.created, prof.changed FROM commerce_customer_profile prof WHERE prof.type = 'billing'
  AND 
    NOT EXISTS(
      SELECT profile_id FROM commerce_customer_profile prof2 WHERE prof2.uid = prof.uid AND prof2.type = 'billing' 
	AND prof2.profile_id > prof.profile_id
    )
) as billing_prof ON billing_prof.uid = u.uid
LEFT JOIN field_data_commerce_customer_address billing_address ON billing_address.entity_id = billing_prof.profile_id AND billing_address.entity_type = 'commerce_customer_profile'
LEFT JOIN country_olaf billing_country_olaf ON billing_address.commerce_customer_address_country = billing_country_olaf.`A2 (ISO)`
LEFT JOIN state_olaf billing_state_olaf ON billing_address.commerce_customer_address_administrative_area = billing_state_olaf.`State Abbrev`

LEFT JOIN field_data_field_archive_uid archive_uid ON archive_uid.entity_id = u.uid AND archive_uid.entity_type = 'user'

LEFT JOIN field_data_field_first_name first_name ON first_name.entity_id = u.uid AND first_name.entity_type = 'user'
LEFT JOIN field_data_field_last_name last_name ON last_name.entity_id = u.uid AND last_name.entity_type = 'user'
LEFT JOIN users_roles u_r ON u_r.uid = u.uid
LEFT JOIN role ON role.rid = u_r.rid
LEFT JOIN field_data_field_phone_number phone ON phone.entity_id = u.uid AND phone.entity_type = 'user'
LEFT JOIN field_data_field_did_you_graduate_college did_you_graduate_college ON did_you_graduate_college.entity_id = u.uid AND 
did_you_graduate_college.entity_type = 'user'LEFT JOIN field_data_field_college_state_list college_state_list ON college_state_list.entity_id = u.uid AND college_state_list.entity_type = 'user'
LEFT JOIN field_data_field_college_name college_name_text ON college_name_text.entity_id = u.uid AND college_name_text.entity_type = 'user'
LEFT JOIN field_data_field_graduation_month_and_year grad_date ON grad_date.entity_id = u.uid AND grad_date.entity_type = 'user'
LEFT JOIN field_data_field_when_do_you_plan_to_start when_plan_first_sit_exam ON when_plan_first_sit_exam.entity_id = u.uid AND when_plan_first_sit_exam.entity_type = 'user'
LEFT JOIN field_data_field_country country ON country.entity_id = u.uid AND country.entity_type = 'user'
LEFT JOIN field_data_field_student_state state ON state.entity_id = u.uid AND state.entity_type = 'user'
LEFT JOIN field_data_field_campus campus ON campus.entity_id = u.uid AND campus.entity_type = 'user'
LEFT JOIN field_data_field_aud_exam_date aud_exam_date ON aud_exam_date.entity_id = u.uid AND aud_exam_date.entity_type = 'user'
LEFT JOIN field_data_field_far_exam_date far_exam_date ON far_exam_date.entity_id = u.uid AND far_exam_date.entity_type = 'user'
LEFT JOIN field_data_field_bec_exam_date bec_exam_date ON bec_exam_date.entity_id = u.uid AND bec_exam_date.entity_type = 'user'
LEFT JOIN field_data_field_reg_exam_date reg_exam_date ON reg_exam_date.entity_id = u.uid AND reg_exam_date.entity_type = 'user'
LEFT JOIN field_data_field_archive_synced archive_synced ON archive_synced.entity_id = u.uid AND archive_synced.entity_type = 'user'
LEFT JOIN field_data_field_sync_stage sync_stage ON sync_stage.entity_id = u.uid AND sync_stage.entity_type = 'user'
LEFT JOIN field_data_field_college_sf_account fcsa ON u.uid = fcsa.entity_id and  fcsa.entity_type='user' AND fcsa.bundle='user'
LEFT JOIN eck_sf_account sfa ON sfa.id = fcsa.field_college_sf_account_target_id
LEFT JOIN field_data_field_uwuid uwuid ON uwuid.entity_id = u.uid AND uwuid.entity_type = 'user'

WHERE u.uid <> 0 
 #REMOVE_ON_FULL_EXPORT#
  AND billing_prof.created >= '#FROM_DATE#' OR billing_prof.changed >= '#FROM_DATE#' OR ship_prof.created >= '#FROM_DATE#' OR ship_prof.changed >= '#FROM_DATE#'
 #@REMOVE_ON_FULL_EXPORT#
GROUP BY u.uid

