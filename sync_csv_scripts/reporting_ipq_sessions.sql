SET time_zone = '-7:00';

DROP TEMPORARY TABLE IF EXISTS new_ipq_sess_uids;
CREATE TEMPORARY TABLE new_ipq_sess_uids 
(INDEX uid (uid))
SELECT uid
FROM ipq_saved_sessions 
WHERE 1
#REMOVE_ON_FULL_EXPORT# 
 AND created_on >= '#FROM_DATE#'
#@REMOVE_ON_FULL_EXPORT#
;

DROP TEMPORARY TABLE IF EXISTS new_ipq_sess_meta_types;
DROP TABLE IF EXISTS new_ipq_sess_meta_types;
CREATE TABLE new_ipq_sess_meta_types
(

id INT,
uid INT,
session_meta_type CHAR(10), 
section_id INT,

INDEX mt_index (uid, id)

)
SELECT 
ipq_saved_sessions.uid, 
ipq_saved_sessions.id,
 section_id,
 CASE 
  WHEN (session_type = 'exam') THEN
   'exam'
  WHEN (session_config NOT LIKE '%smart_quiz%') THEN
   'quiz'
  ELSE
   'smart-quiz'
 END AS session_meta_type
FROM ipq_saved_sessions 
INNER JOIN new_ipq_sess_uids ON new_ipq_sess_uids.uid = ipq_saved_sessions.uid # Only apply to sessions created by users with modified data (new sessions)
;


SELECT
  ss.uid,
  sec_term.name AS section,
  session_meta_type AS quiz_type,

  SUM(IF(finished = 1, 1, 0)) AS total_quizes_finished,
  SUM(IF(finished = 1, average_time, 0)) / SUM(IF(finished = 1, 1, 0)) AS avg_time_finished,
  SUM(IF(finished = 1, percent_complete, 0)) / SUM(IF(finished = 1, 1, 0)) AS avg_perc_complete_finished,
  SUM(IF(finished = 1, percent_correct, 0)) / SUM(IF(finished = 1, 1, 0)) AS avg_perc_correct_finished,
  MIN(IF(finished = 1, DATE(FROM_UNIXTIME(created_on)), NOW())) AS first_date_finished,
  MAX(IF(finished = 1, DATE(FROM_UNIXTIME(created_on)), 0)) AS last_date_finished,

  SUM(IF(finished = 0, 1, 0)) AS total_quizes_unfinished,
  SUM(IF(finished = 0, average_time, 0)) / SUM(IF(finished = 0, 1, 0)) AS avg_time_unfinished,
  SUM(IF(finished = 0, percent_complete, 0)) / SUM(IF(finished = 0, 1, 0)) AS avg_perc_complete_unfinished,
  SUM(IF(finished = 0, percent_correct, 0)) / SUM(IF(finished = 0, 1, 0)) AS avg_perc_correct_unfinished,
  MIN(IF(finished = 0, DATE(FROM_UNIXTIME(created_on)), NOW())) AS first_date_unfinished,
  MAX(IF(finished = 0, DATE(FROM_UNIXTIME(created_on)), 0)) AS last_date_unfinished,

  entitlement_id as entitlement_id,

  (SELECT SUM(max_score) FROM ipq_saved_session_data sd

	WHERE sd.ipq_saved_sessions_id IN (
	    SELECT ss2.id from ipq_saved_sessions ss2
	    INNER JOIN new_ipq_sess_meta_types  ON new_ipq_sess_meta_types.uid = ss2.uid AND new_ipq_sess_meta_types.id = ss2.id
	    WHERE ss2.uid = ss.uid AND ss2.section_id = ss.section_id AND ss2.entitlement_id = ss.entitlement_id AND new_ipq_sess_meta_types.session_meta_type = quiz_type
    )
	) as total_question_points,

  (SELECT SUM(score) FROM ipq_saved_session_data sd
	WHERE sd.ipq_saved_sessions_id IN (
	    SELECT ss2.id from ipq_saved_sessions ss2
	    INNER JOIN new_ipq_sess_meta_types FORCE INDEX FOR JOIN (`mt_index`) ON  new_ipq_sess_meta_types.uid = ss2.uid AND new_ipq_sess_meta_types.id = ss2.id
	    WHERE ss2.uid = ss.uid AND ss2.section_id = ss.section_id AND ss2.entitlement_id = ss.entitlement_id AND new_ipq_sess_meta_types.session_meta_type = quiz_type
	  )
	) as total_obtained_points,

  (SELECT SUM(score)
	FROM ipq_saved_session_data sd
	INNER JOIN ipq_question_types qt ON sd.ipq_question_type_id = qt.id
	WHERE sd.ipq_saved_sessions_id IN (
	    SELECT ss2.id from ipq_saved_sessions ss2
	    INNER JOIN new_ipq_sess_meta_types FORCE INDEX FOR JOIN (`mt_index`) ON  new_ipq_sess_meta_types.uid = ss2.uid AND new_ipq_sess_meta_types.id = ss2.id
	    WHERE ss2.uid = ss.uid AND ss2.section_id = ss.section_id AND ss2.entitlement_id = ss.entitlement_id AND new_ipq_sess_meta_types.session_meta_type = quiz_type
	  )
	  AND qt.type = 'ipq_mcq_question'
  ) as mcq_question_score,

  (SELECT COUNT(*)
	FROM ipq_saved_session_data sd
	INNER JOIN ipq_question_types qt ON sd.ipq_question_type_id = qt.id
	WHERE sd.ipq_saved_sessions_id IN (
		    SELECT ss2.id from ipq_saved_sessions ss2
		    INNER JOIN new_ipq_sess_meta_types FORCE INDEX FOR JOIN (`mt_index`) ON  new_ipq_sess_meta_types.uid = ss2.uid AND new_ipq_sess_meta_types.id = ss2.id
		    WHERE ss2.uid = ss.uid AND ss2.section_id = ss.section_id AND ss2.entitlement_id = ss.entitlement_id AND new_ipq_sess_meta_types.session_meta_type = quiz_type
  )
	AND qt.type = 'ipq_mcq_question'
  ) as mcq_question_count,

  (SELECT COUNT(DISTINCT(sd.ipq_question_id))
	FROM ipq_saved_session_data sd
	INNER JOIN ipq_question_types qt ON sd.ipq_question_type_id = qt.id
	WHERE sd.ipq_saved_sessions_id IN (
		    SELECT ss2.id from ipq_saved_sessions ss2
		    INNER JOIN new_ipq_sess_meta_types FORCE INDEX FOR JOIN (`mt_index`) ON  new_ipq_sess_meta_types.uid = ss2.uid AND new_ipq_sess_meta_types.id = ss2.id
		    WHERE ss2.uid = ss.uid AND ss2.section_id = ss.section_id AND ss2.entitlement_id = ss.entitlement_id AND new_ipq_sess_meta_types.session_meta_type = quiz_type
  )
	AND qt.type = 'ipq_mcq_question'
  ) as mcq_distinct_question_count,

  (SELECT SUM(score)
	FROM ipq_saved_session_data sd
	INNER JOIN ipq_question_types qt ON sd.ipq_question_type_id = qt.id
	WHERE sd.ipq_saved_sessions_id IN (
		    SELECT ss2.id from ipq_saved_sessions ss2
		    INNER JOIN new_ipq_sess_meta_types FORCE INDEX FOR JOIN (`mt_index`) ON  new_ipq_sess_meta_types.uid = ss2.uid AND new_ipq_sess_meta_types.id = ss2.id
		    WHERE ss2.uid = ss.uid AND ss2.section_id = ss.section_id AND ss2.entitlement_id = ss.entitlement_id AND new_ipq_sess_meta_types.session_meta_type = quiz_type
  )
	AND qt.type = 'ipq_drs_question'
  ) as drs_question_score,

  (SELECT COUNT(*)
	FROM ipq_saved_session_data sd
	INNER JOIN ipq_question_types qt ON sd.ipq_question_type_id = qt.id
	WHERE sd.ipq_saved_sessions_id IN (
	    SELECT ss2.id from ipq_saved_sessions ss2
	    INNER JOIN new_ipq_sess_meta_types FORCE INDEX FOR JOIN (`mt_index`) ON  new_ipq_sess_meta_types.uid = ss2.uid AND new_ipq_sess_meta_types.id = ss2.id
	    WHERE ss2.uid = ss.uid AND ss2.section_id = ss.section_id AND ss2.entitlement_id = ss.entitlement_id AND new_ipq_sess_meta_types.session_meta_type = quiz_type
	)
	AND qt.type = 'ipq_drs_question'
  ) as drs_question_count,

  (SELECT COUNT(DISTINCT(sd.ipq_question_id))
	FROM ipq_saved_session_data sd
	INNER JOIN ipq_question_types qt ON sd.ipq_question_type_id = qt.id
	WHERE sd.ipq_saved_sessions_id IN (
	    SELECT ss2.id from ipq_saved_sessions ss2
	    INNER JOIN new_ipq_sess_meta_types FORCE INDEX FOR JOIN (`mt_index`) ON  new_ipq_sess_meta_types.uid = ss2.uid AND new_ipq_sess_meta_types.id = ss2.id
	    WHERE ss2.uid = ss.uid AND ss2.section_id = ss.section_id AND ss2.entitlement_id = ss.entitlement_id AND new_ipq_sess_meta_types.session_meta_type = quiz_type
  )
	AND qt.type = 'ipq_drs_question'
  ) as drs_distinct_question_count,

  (SELECT SUM(score)
	FROM ipq_saved_session_data sd
	INNER JOIN ipq_question_types qt ON sd.ipq_question_type_id = qt.id
	WHERE sd.ipq_saved_sessions_id IN (
		    SELECT ss2.id from ipq_saved_sessions ss2
		    INNER JOIN new_ipq_sess_meta_types FORCE INDEX FOR JOIN (`mt_index`) ON  new_ipq_sess_meta_types.uid = ss2.uid AND new_ipq_sess_meta_types.id = ss2.id
		    WHERE ss2.uid = ss.uid AND ss2.section_id = ss.section_id AND ss2.entitlement_id = ss.entitlement_id AND new_ipq_sess_meta_types.session_meta_type = quiz_type
  )
	AND qt.type = 'ipq_research_question'
  ) as research_question_score,

  (SELECT COUNT(*)
	FROM ipq_saved_session_data sd
	INNER JOIN ipq_question_types qt ON sd.ipq_question_type_id = qt.id
	WHERE sd.ipq_saved_sessions_id IN (
		    SELECT ss2.id from ipq_saved_sessions ss2
		    INNER JOIN new_ipq_sess_meta_types FORCE INDEX FOR JOIN (`mt_index`) ON  new_ipq_sess_meta_types.uid = ss2.uid AND new_ipq_sess_meta_types.id = ss2.id
		    WHERE ss2.uid = ss.uid AND ss2.section_id = ss.section_id AND ss2.entitlement_id = ss.entitlement_id AND new_ipq_sess_meta_types.session_meta_type = quiz_type
  )
	AND qt.type = 'ipq_research_question'
  ) as research_question_count,

  (SELECT COUNT(DISTINCT(sd.ipq_question_id))
	FROM ipq_saved_session_data sd
	INNER JOIN ipq_question_types qt ON sd.ipq_question_type_id = qt.id
	WHERE sd.ipq_saved_sessions_id IN (
		    SELECT ss2.id from ipq_saved_sessions ss2
		    INNER JOIN new_ipq_sess_meta_types FORCE INDEX FOR JOIN (`mt_index`) ON  new_ipq_sess_meta_types.uid = ss2.uid AND new_ipq_sess_meta_types.id = ss2.id
		    WHERE ss2.uid = ss.uid AND ss2.section_id = ss.section_id AND ss2.entitlement_id = ss.entitlement_id AND new_ipq_sess_meta_types.session_meta_type = quiz_type
  )
	AND qt.type = 'ipq_research_question'
  ) as research_distinct_question_count,

  (SELECT SUM(score)
	FROM ipq_saved_session_data sd
	INNER JOIN ipq_question_types qt ON sd.ipq_question_type_id = qt.id
	WHERE sd.ipq_saved_sessions_id IN (
			  SELECT ss2.id from ipq_saved_sessions ss2
		    INNER JOIN new_ipq_sess_meta_types FORCE INDEX FOR JOIN (`mt_index`) ON  new_ipq_sess_meta_types.uid = ss2.uid AND new_ipq_sess_meta_types.id = ss2.id
		    WHERE ss2.uid = ss.uid AND ss2.section_id = ss.section_id AND ss2.entitlement_id = ss.entitlement_id AND new_ipq_sess_meta_types.session_meta_type = quiz_type
	)
	AND qt.type = 'ipq_tbs_form_question'
  ) as tbs_form_question_score,

  (SELECT COUNT(*)
	FROM ipq_saved_session_data sd
	INNER JOIN ipq_question_types qt ON sd.ipq_question_type_id = qt.id
	WHERE sd.ipq_saved_sessions_id IN (
		    SELECT ss2.id from ipq_saved_sessions ss2
		    INNER JOIN new_ipq_sess_meta_types FORCE INDEX FOR JOIN (`mt_index`) ON  new_ipq_sess_meta_types.uid = ss2.uid AND new_ipq_sess_meta_types.id = ss2.id
		    WHERE ss2.uid = ss.uid AND ss2.section_id = ss.section_id AND ss2.entitlement_id = ss.entitlement_id AND new_ipq_sess_meta_types.session_meta_type = quiz_type
  )
	AND qt.type = 'ipq_tbs_form_question'
  ) as tbs_form_question_count,

  (SELECT COUNT(DISTINCT(sd.ipq_question_id))
	FROM ipq_saved_session_data sd
	INNER JOIN ipq_question_types qt ON sd.ipq_question_type_id = qt.id
	WHERE sd.ipq_saved_sessions_id IN (
	    SELECT ss2.id from ipq_saved_sessions ss2
	    INNER JOIN new_ipq_sess_meta_types FORCE INDEX FOR JOIN (`mt_index`) ON  new_ipq_sess_meta_types.uid = ss2.uid AND new_ipq_sess_meta_types.id = ss2.id
	    WHERE ss2.uid = ss.uid AND ss2.section_id = ss.section_id AND ss2.entitlement_id = ss.entitlement_id AND new_ipq_sess_meta_types.session_meta_type = quiz_type
	)
	AND qt.type = 'ipq_tbs_form_question'
  ) as tbs_form_distinct_question_count,

  (SELECT SUM(score)
	FROM ipq_saved_session_data sd
	INNER JOIN ipq_question_types qt ON sd.ipq_question_type_id = qt.id
	WHERE sd.ipq_saved_sessions_id IN (
	    SELECT ss2.id from ipq_saved_sessions ss2
	    INNER JOIN new_ipq_sess_meta_types FORCE INDEX FOR JOIN (`mt_index`) ON  new_ipq_sess_meta_types.uid = ss2.uid AND new_ipq_sess_meta_types.id = ss2.id
	    WHERE ss2.uid = ss.uid AND ss2.section_id = ss.section_id AND ss2.entitlement_id = ss.entitlement_id AND new_ipq_sess_meta_types.session_meta_type = quiz_type
	)
	AND qt.type = 'ipq_tbs_journal_question'
  ) as tbs_journal_question_score,

  (SELECT COUNT(*)
	FROM ipq_saved_session_data sd
	INNER JOIN ipq_question_types qt ON sd.ipq_question_type_id = qt.id
	WHERE sd.ipq_saved_sessions_id IN (
      SELECT ss2.id from ipq_saved_sessions ss2
      INNER JOIN new_ipq_sess_meta_types FORCE INDEX FOR JOIN (`mt_index`) ON  new_ipq_sess_meta_types.uid = ss2.uid AND new_ipq_sess_meta_types.id = ss2.id
      WHERE ss2.uid = ss.uid AND ss2.section_id = ss.section_id AND ss2.entitlement_id = ss.entitlement_id AND new_ipq_sess_meta_types.session_meta_type = quiz_type
  )
	AND qt.type = 'ipq_tbs_journal_question'
  ) as tbs_journal_question_count,

  (SELECT COUNT(DISTINCT(sd.ipq_question_id))
	FROM ipq_saved_session_data sd
	INNER JOIN ipq_question_types qt ON sd.ipq_question_type_id = qt.id
	WHERE sd.ipq_saved_sessions_id IN (
      SELECT ss2.id from ipq_saved_sessions ss2
      INNER JOIN new_ipq_sess_meta_types FORCE INDEX FOR JOIN (`mt_index`) ON  new_ipq_sess_meta_types.uid = ss2.uid AND new_ipq_sess_meta_types.id = ss2.id
      WHERE ss2.uid = ss.uid AND ss2.section_id = ss.section_id AND ss2.entitlement_id = ss.entitlement_id AND new_ipq_sess_meta_types.session_meta_type = quiz_type
  )
	AND qt.type = 'ipq_tbs_journal_question'
  ) as tbs_journal_distinct_question_count,

  (SELECT SUM(score)
	FROM ipq_saved_session_data sd
	INNER JOIN ipq_question_types qt ON sd.ipq_question_type_id = qt.id
	WHERE sd.ipq_saved_sessions_id IN (
      SELECT ss2.id from ipq_saved_sessions ss2
      INNER JOIN new_ipq_sess_meta_types FORCE INDEX FOR JOIN (`mt_index`) ON  new_ipq_sess_meta_types.uid = ss2.uid AND new_ipq_sess_meta_types.id = ss2.id
      WHERE ss2.uid = ss.uid AND ss2.section_id = ss.section_id AND ss2.entitlement_id = ss.entitlement_id AND new_ipq_sess_meta_types.session_meta_type = quiz_type
  )
	AND qt.type = 'ipq_tbs_question'
  ) as regular_tbs_question_score,

  (SELECT COUNT(*)
	FROM ipq_saved_session_data sd
	INNER JOIN ipq_question_types qt ON sd.ipq_question_type_id = qt.id
	WHERE sd.ipq_saved_sessions_id IN (
      SELECT ss2.id from ipq_saved_sessions ss2
      INNER JOIN new_ipq_sess_meta_types FORCE INDEX FOR JOIN (`mt_index`) ON  new_ipq_sess_meta_types.uid = ss2.uid AND new_ipq_sess_meta_types.id = ss2.id
      WHERE ss2.uid = ss.uid AND ss2.section_id = ss.section_id AND ss2.entitlement_id = ss.entitlement_id AND new_ipq_sess_meta_types.session_meta_type = quiz_type
  )
	AND qt.type = 'ipq_tbs_question'
  ) as regular_tbs_question_count,

  (SELECT COUNT(DISTINCT(sd.ipq_question_id))
	FROM ipq_saved_session_data sd
	INNER JOIN ipq_question_types qt ON sd.ipq_question_type_id = qt.id
	WHERE sd.ipq_saved_sessions_id IN (
      SELECT ss2.id from ipq_saved_sessions ss2
      INNER JOIN new_ipq_sess_meta_types FORCE INDEX FOR JOIN (`mt_index`) ON  new_ipq_sess_meta_types.uid = ss2.uid AND new_ipq_sess_meta_types.id = ss2.id
      WHERE ss2.uid = ss.uid AND ss2.section_id = ss.section_id AND ss2.entitlement_id = ss.entitlement_id AND new_ipq_sess_meta_types.session_meta_type = quiz_type
  )
	AND qt.type = 'ipq_tbs_question'
  ) as regular_tbs_distinct_question_count,

  (SELECT SUM(score)
	FROM ipq_saved_session_data sd
	INNER JOIN ipq_question_types qt ON sd.ipq_question_type_id = qt.id
	WHERE sd.ipq_saved_sessions_id IN (
      SELECT ss2.id from ipq_saved_sessions ss2
      INNER JOIN new_ipq_sess_meta_types FORCE INDEX FOR JOIN (`mt_index`) ON  new_ipq_sess_meta_types.uid = ss2.uid AND new_ipq_sess_meta_types.id = ss2.id
      WHERE ss2.uid = ss.uid AND ss2.section_id = ss.section_id AND ss2.entitlement_id = ss.entitlement_id AND new_ipq_sess_meta_types.session_meta_type = quiz_type
  )
	AND qt.type = 'ipq_wc_question'
  ) as wc_question_score,

  (SELECT COUNT(*)
	FROM ipq_saved_session_data sd
	INNER JOIN ipq_question_types qt ON sd.ipq_question_type_id = qt.id
	WHERE sd.ipq_saved_sessions_id IN (
      SELECT ss2.id from ipq_saved_sessions ss2
      INNER JOIN new_ipq_sess_meta_types FORCE INDEX FOR JOIN (`mt_index`) ON  new_ipq_sess_meta_types.uid = ss2.uid AND new_ipq_sess_meta_types.id = ss2.id
      WHERE ss2.uid = ss.uid AND ss2.section_id = ss.section_id AND ss2.entitlement_id = ss.entitlement_id AND new_ipq_sess_meta_types.session_meta_type = quiz_type
  )
	AND qt.type = 'ipq_wc_question'
  ) as wc_question_count,

  (SELECT COUNT(DISTINCT(sd.ipq_question_id))
	FROM ipq_saved_session_data sd
	INNER JOIN ipq_question_types qt ON sd.ipq_question_type_id = qt.id
	WHERE sd.ipq_saved_sessions_id IN (
      SELECT ss2.id from ipq_saved_sessions ss2
      INNER JOIN new_ipq_sess_meta_types FORCE INDEX FOR JOIN (`mt_index`) ON  new_ipq_sess_meta_types.uid = ss2.uid AND new_ipq_sess_meta_types.id = ss2.id
      WHERE ss2.uid = ss.uid AND ss2.section_id = ss.section_id AND ss2.entitlement_id = ss.entitlement_id AND new_ipq_sess_meta_types.session_meta_type = quiz_type
  )
	AND qt.type = 'ipq_wc_question'
  ) as wc_distinct_question_count


FROM ipq_saved_sessions ss
INNER JOIN taxonomy_term_data sec_term ON sec_term.tid = ss.section_id
INNER JOIN new_ipq_sess_meta_types FORCE INDEX FOR JOIN (`mt_index`) ON new_ipq_sess_meta_types.uid = ss.uid AND new_ipq_sess_meta_types.id = ss.id

WHERE
1
group by uid, ss.section_id, entitlement_id, session_meta_type
;


