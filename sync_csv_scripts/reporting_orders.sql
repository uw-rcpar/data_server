SET time_zone = '-7:00';

SELECT FROM_UNIXTIME(o.created) AS created_date, FROM_UNIXTIME(o.changed) AS modified_date, o.uid, o.order_id,
  co_total.commerce_order_total_amount AS order_total,
  SUM(shipping_amount.commerce_total_amount) AS shipping_total, SUM(tax_amount.commerce_total_amount) AS tax_total,
  o.status,
  pprof.field_partner_profile_target_id AS partner_id
FROM commerce_order o
LEFT JOIN field_data_field_partner_profile pprof ON pprof.entity_id = o.order_id AND pprof.entity_type = "commerce_order"
LEFT JOIN field_data_commerce_order_total co_total ON co_total.entity_id = o.order_id AND co_total.entity_type = 'commerce_order'
LEFT JOIN field_data_commerce_line_items fd_cli ON fd_cli.entity_id = o.order_id AND fd_cli.entity_type = 'commerce_order'

LEFT JOIN commerce_line_item shipping_line ON fd_cli.commerce_line_items_line_item_id = shipping_line.line_item_id AND shipping_line.type = 'shipping'
LEFT JOIN field_data_commerce_total shipping_amount ON shipping_line.line_item_id = shipping_amount.entity_id AND shipping_amount.entity_type = 'commerce_line_item'

LEFT JOIN commerce_line_item tax_line ON fd_cli.commerce_line_items_line_item_id = tax_line.line_item_id AND tax_line.type = 'avatax'
LEFT JOIN field_data_commerce_total tax_amount ON tax_line.line_item_id = tax_amount.entity_id AND tax_amount.entity_type = 'commerce_line_item'

WHERE 
  1
#REMOVE_ON_FULL_EXPORT#
 AND o.created >= '#FROM_DATE#' OR o.changed >= '#FROM_DATE#'
#@REMOVE_ON_FULL_EXPORT#
group by o.order_id
;

