SET time_zone = '-7:00';

SELECT
  o.order_id, cli.line_item_id AS line_item_id, prod.sku, cli.line_item_label AS name, cli.quantity, FROM_UNIXTIME(cli.created) AS created_date,
  FROM_UNIXTIME(cli.changed) AS modified_date, FLOOR(rcpar_tax.taxable) AS taxable, FLOOR(rcpar_tax.taxed) AS taxed, FLOOR(rcpar_tax.total) AS total, FLOOR(rcpar_tax.shipping_part) AS shipping_part, fd_total.commerce_total_amount AS revenue
FROM commerce_order o
INNER JOIN field_data_commerce_line_items fd_cli ON fd_cli.entity_id = o.order_id AND fd_cli.entity_type = 'commerce_order'
INNER JOIN commerce_line_item cli ON fd_cli.commerce_line_items_line_item_id = cli.line_item_id
INNER JOIN rcpar_commerce_reports_tax rcpar_tax ON rcpar_tax.order_line_id = cli.line_item_id

LEFT JOIN field_data_commerce_product fd_prod ON cli.line_item_id = fd_prod.entity_id AND fd_prod.entity_type = 'commerce_line_item' AND fd_prod.deleted = '0'
LEFT JOIN commerce_product prod ON fd_prod.commerce_product_product_id = prod.product_id

LEFT JOIN field_data_commerce_total fd_total ON fd_total.entity_id = cli.line_item_id AND fd_total.entity_type = 'commerce_line_item'

WHERE cli.type = "product"
#REMOVE_ON_FULL_EXPORT#
 AND o.created >= '#FROM_DATE#' OR o.changed >= '#FROM_DATE#';
#@REMOVE_ON_FULL_EXPORT#
