
ENV=/usr/bin/env


# Variables definitions.
CERT_PATH='/mnt/gfs/home/rogercpa/scripts/sync_csv_scripts/drupaldevkey.pem'
SERVER_ALIAS='@dev2'
LOCAL_FILES_DIR='/mnt/gfs/home/rogercpa/scripts/sync_csv_scripts/'
FILE='output.csv'
SSH_USER='ec2-user'
DST_SRV='reporting.rcparlabs.com'
DST_PATH='/var/www/projects/reports/storage/app/uploaded_csv/'

UW_SSH_USER='dw-admin'
UW_DST_SRV='52.177.120.243'
UW_DST_PATH='/var/www/projects/reports/storage/app/uploaded_csv/'


FILES_SRC=( "reporting_ft_daily.sql" "reporting_ft_users.sql" "reporting_ft_2_purchase.sql" "reporting_order_transactions.sql" "reporting_referral.sql" "reporting_discount_verifications.sql" "reporting_exam_scores.sql" "reporting_score_attempts.sql" "reporting_hhc_performance.sql" "reporting_hhc_performance_group.sql"  \
#"reporting_orders.sql" \
#"reporting_order_lines.sql" \
"reporting_partners.sql" "reporting_partner_emails.sql" "reporting_partner_emails_groups.sql" "reporting_user_profiles.sql" "reporting_ipq_sessions.sql" \
"reporting_nps.sql" "reporting_lectures_metadata.sql" "reporting_user_entitlements.sql" "reporting_lecture_history.sql" )
FILES_DST=( "FTDaily.csv" "FTUsers.csv" "FTPaid.csv" "OrderTransactions.csv" "Referral.csv" "DiscountVerification.csv" "ExamScores.csv" "ExamScoreAttempts.csv" "HHCPerformance.csv" "HHCPerformanceGroups.csv" \
#"Order.csv" \
#"OrderLine.csv" \
"PartnerProfile.csv" "PartnerEmails.csv" "PartnerEmailsGroups.csv" "UserProfile.csv" "IPQSessions.csv" \
"NetPromoterScore.csv" "LecturesMetadata.csv" "UserEntitlements.csv" "LectureHistory.csv")
DAYS_AGO="3"