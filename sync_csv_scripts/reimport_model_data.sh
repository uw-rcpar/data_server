#!/bin/bash

if (($# == "0"))  
then 
   echo 'Model name required.'
   exit
fi


while true; do
    read -p "This will overwrite the information already imported for the $1 model. Are you sure that you want to continue?" yn
    case $yn in
        [Yy]* ) break;;
        [Nn]* ) exit;;
        * ) echo "Please answer yes or no.";;
    esac
done

echo "Reimporting all data for $1" 

# Importing Constants
DIR="${BASH_SOURCE%/*}"
if [[ ! -d "$DIR" ]]; then DIR="$PWD"; fi
source "$DIR/exporting_commons.sh"

MODEL_DST_FILE="$1.csv"
# Search for the model index.
for i in ${!FILES_DST[@]}
do

  if [ "${MODEL_DST_FILE}" == "${FILES_DST[$i]}" ]
  then 
    date
    echo "E&S - Running ${FILES_SRC[$i]} query"
    $ENV cat ${LOCAL_FILES_DIR}${FILES_SRC[$i]} | $ENV sed "s/#REMOVE_ON_FULL_EXPORT#.*#@REMOVE_ON_FULL_EXPORT#//g" > tmp.sql
   $ENV drush ${SERVER_ALIAS} sqlc < tmp.sql | $ENV sed "s/\"/\\\\\"/g;s/'/\'/;s/\t/\",\"/g;s/^/\"/;s/$/\"/;s/\n//g" > ${LOCAL_FILES_DIR}${FILES_DST[$i]}

   echo "E&S - Creation Completed: ${FILES_DST[$i]}"
   date
   # Upload to UW DW server
   $ENV scp ${LOCAL_FILES_DIR}${FILES_DST[$i]} $UW_SSH_USER@${UW_DST_SRV}:${UW_DST_PATH}${FILES_DST[$i]}

   echo "E&S - Send Completed: ${FILES_DST[$i]}"   

   # Now that we have the file on the server, we can import it.
   echo "E&S - Running import command on DW server 'php artisan rcpar:import-csv $1'"   
   date
   echo "Exiting..."
   exit
  fi
done

echo "ERROR: Model not found."

