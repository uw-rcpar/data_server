SET time_zone = '-7:00';

DROP TEMPORARY TABLE IF EXISTS hhc_user_groups_tmp;
CREATE TEMPORARY TABLE hhc_user_groups_tmp (
`uid` int(11) NOT NULL DEFAULT '0' COMMENT 'Integer',
`count` int(11) NOT NULL DEFAULT '0' COMMENT 'Integer',
`user_group` varchar(255) NOT NULL DEFAULT '' COMMENT 'Text'
);

INSERT INTO hhc_user_groups_tmp(
`uid`,
`count`,
`user_group`
)
SELECT 
`n`.`uid` AS `uid`,
count(`n`.`uid`) AS `Count`,
(
  case 
  when (count(`n`.`uid`) < 1) then 
    'No Questions' 
  when (count(`n`.`uid`) between 1 and 49) then 
    '1-49 Normal Users' 
  when (count(`n`.`uid`) between 50 and 99) then 
    '50-99 Light Users' 
  when (count(`n`.`uid`) between 100 and 199) then 
    '100-199 Heavy Users' 
  when (count(`n`.`uid`) > 199) then 
    '200+ Super Users' else '' end
) AS `user_group`
FROM
`node` `n` 
  LEFT JOIN `comment` `c` ON `n`.`nid` = `c`.`nid`
  LEFT JOIN `field_data_comment_body` `b` ON `b`.`entity_id` = `c`.`cid`
  LEFT JOIN `users` `u` ON `u`.`uid` = `n`.`uid`
WHERE
 `n`.`type` = 'helpcenter_question' 
GROUP BY `n`.`uid`
ORDER BY 'Count' DESC;


DROP TEMPORARY TABLE IF EXISTS user_package_tmp;
CREATE TEMPORARY TABLE user_package_tmp (
  `uid` int(11) NOT NULL DEFAULT '0' COMMENT 'Integer',
  `order_id` int(11) NOT NULL DEFAULT '0' COMMENT 'Integer',
  `package` varchar(255) NOT NULL DEFAULT '' COMMENT 'Text'
);

INSERT INTO user_package_tmp(
  `uid`,
  `order_id`,
  `package`
)
SELECT
  `co`.`uid` AS `uid`,
  `cli`.`order_id` AS `order_id`,
  (
    case
      when (`cli`.`line_item_label` = 'FULL-ELITE-DIS') then
        'Elite'
      when (`cli`.`line_item_label` = 'FULL-PREM-DIS') then
        'Premium'
      when (`cli`.`line_item_label` = 'FULL-PREM-EFC-DIS') then
        'Premium'
      when (`cli`.`line_item_label` = 'FULL-PREM-AUD-DIS') then
        'Premium'
      when (`cli`.`line_item_label` = 'FULL-DIS') then
        'Select'
    end
  ) AS `package`
FROM
  `commerce_line_item` `cli`
  LEFT JOIN `commerce_order` `co` on `co`.`order_id` = `cli`.`order_id`
  LEFT JOIN `commerce_payment_transaction` `trans` on `trans`.`order_id` = `co`.`order_id`
WHERE
 `cli`.`line_item_label` IN('FULL-ELITE-DIS','FULL-PREM-DIS','FULL-DIS','FULL-PREM-EFC-DIS','FULL-PREM-EFC-DIS')
    AND
 `co`.`status` IN('Pending','Processing','completed','shipped')
    AND
 `trans`.`payment_method` <> 'rcpar_partners_payment_freetrial'
GROUP BY `cli`.`order_id`;


SELECT 
  `n`.`uid` AS `uid`,`u`.`mail` AS `user`,
  if(isnull(`up`.`Package`),'Individual',`up`.`Package`) AS `package`,
  if((`g`.`user_group` is not null),`g`.`user_group`,'No Questions') AS `user_group`,
  `c`.`cid` AS `cid`,
  (
   case
      when (`c`.`name` = 'Moderator 1') then
      'Jae'   
     when (`c`.`name` = 'Moderator 1') then
      'Rick'
     when (`c`.`name` = 'Moderator 2') then
      'Aaron'
     when (`c`.`name` = 'Moderator 3') then
      'Jae (Contractor)'
     when (`c`.`name` = 'Moderator 5') then
      'Wendy'
     when (`c`.`name` = 'Moderator 7') then
      'Eva'
     when (`c`.`name` = 'HHC Administrator') then
      'HHC Admin'
     when (`c`.`name` = 'Moderator 11') then 
      'AJ Cataldo'
     when (`c`.`name` = 'Moderator 10') then 
      'Wendy (Contractor)' 
     else ''
   end
  ) AS `moderator`,
  cast(addtime(from_unixtime(`n`.`created`),'2:00') as date) AS `date`,
  cast(addtime(from_unixtime(`c`.`changed`),'2:00') as date) AS `answer_date`,
  `c`.`thread` AS `thread`,
  year(addtime(from_unixtime(`n`.`created`),'2:00')) AS `year`,
  month(addtime(from_unixtime(`n`.`created`),'2:00')) AS `month`,
  dayofmonth(addtime(from_unixtime(`n`.`created`),'2:00')) AS `day`,
  timediff(addtime(from_unixtime(`c`.`changed`),'2:00'),addtime(from_unixtime(`n`.`created`),'2:00')) AS `timediff`,
  `n`.`title` AS `title`,
  `prefix`.`field_prefix_value` AS `section_chapter`,
  left(`prefix`.`field_prefix_value`,3) AS `section`,
  `q`.`body_value` AS `moderator_question`,
  `b`.`comment_body_value` AS `comment_body_value`
FROM 
 	`node` `n`
	LEFT JOIN `comment` `c` on `n`.`nid` = `c`.`nid`
  LEFT JOIN `field_data_comment_body` `b` on `b`.`entity_id` = `c`.`cid`
  LEFT JOIN `field_data_body` `q` on `q`.`entity_id` = `n`.`nid`
  LEFT JOIN `field_data_field_course_chapter_hhc` `ch` on `n`.`nid` = `ch`.`entity_id`
  LEFT JOIN `field_data_field_prefix` `prefix` on `prefix`.`entity_id` = `ch`.`field_course_chapter_hhc_value`
  LEFT JOIN `users` `u` on `u`.`uid` = `n`.`uid`
  LEFT JOIN `user_package_tmp` `up` on`up`.`uid` = `n`.`uid`
  LEFT JOIN `hhc_user_groups_tmp` `g` on `g`.`uid` = `n`.`uid`
WHERE
  `n`.`type` = 'helpcenter_question'
#REMOVE_ON_FULL_EXPORT#
   AND
  (
    (c.`created` >= '#FROM_DATE#' OR c.`changed` >= '#FROM_DATE#')
      OR
    (n.`created` >= '#FROM_DATE#' OR n.`changed` >= '#FROM_DATE#')
  )
#@REMOVE_ON_FULL_EXPORT#
ORDER BY `n`.`uid`
;
