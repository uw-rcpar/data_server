SELECT users.uid as uid, coupon_code, discount_type, uploaded_fid, previous_course, discount_verification.status as status, used, deleted, updated_at as drupal_updated_at, created_at as drupal_created_at
FROM discount_verification INNER JOIN users ON users.mail =  discount_verification.email
WHERE 1
#REMOVE_ON_FULL_EXPORT#
AND (UNIX_TIMESTAMP(discount_verification.created_at) >= '#FROM_DATE#' OR UNIX_TIMESTAMP(discount_verification.updated_at) >= '#FROM_DATE#')
#@REMOVE_ON_FULL_EXPORT#
GROUP BY id
;