# Replaces: all query

SET time_zone = '-7:00';

# Uncomment this line on some mysql setups:
# SET session sql_mode = 'STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION';
# REPLACE `#FROM_DATE#` with UNIX_TIMESTAMP(`2019-07-01`)

# alter table country_olaf add index A2_ISO(`A2 (ISO)`);
# alter table state_olaf add index StateAbbrev(`State Abbrev`);

DROP TEMPORARY TABLE IF EXISTS tmp_table_free_trial_users;
CREATE TEMPORARY TABLE tmp_table_free_trial_users
(
  id int(10) NOT NULL AUTO_INCREMENT COMMENT 'Unique id',
  user_id int(10) unsigned NOT NULL COMMENT 'drupal user id',
  mail varchar(254) NOT NULL COMMENT 'email',
  total_order_count int(10) NOT NULL COMMENT 'counts for that day',
  date_first_ft varchar(13) DEFAULT '0' COMMENT 'first free trial date',
  first_order_id int(10) unsigned NOT NULL COMMENT 'first FT order id',
  last_order_id int(10) unsigned NOT NULL COMMENT 'last FT order id',
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`),
  KEY `user_id` (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 COMMENT='all FT orders by user';

INSERT INTO tmp_table_free_trial_users (
  user_id,
  mail,
  total_order_count,
  date_first_ft,
  first_order_id,
  last_order_id
)

SELECT
	o.uid AS user_id,
	IFNULL(users.mail, o.mail) AS mail ,
	COUNT(o.uid) AS order_count,
	MIN(DATE(FROM_UNIXTIME(trans.changed))) AS date_first_ft,
    	MIN(o.order_id) AS first_order_id,
        MAX(o.order_id) AS last_order_id

FROM commerce_order o

LEFT JOIN users ON users.uid = o.uid
LEFT JOIN commerce_payment_transaction trans ON (trans.order_id = o.order_id)
LEFT JOIN field_data_field_partner_profile pprof ON pprof.entity_id = o.order_id
LEFT JOIN field_data_field_partner_type pt ON pt.entity_id = pprof.field_partner_profile_target_id

WHERE
	# Free trial condition
	EXISTS (
	      SELECT li.order_id
	      FROM commerce_line_item li
	      WHERE line_item_label = "FREETRIAL-BUNDLE" AND li.order_id = o.order_id
	) AND trans.payment_method IN ( "rcpar_partners_payment_freetrial", "enroll_flow_uworld_payment" )


	AND o.status IN ('Pending','Processing','completed','shipped')
	AND o.uid NOT IN (0,2625,4035,24768,114306,114521,1,37,392,406,649,650,878,899,925,1159,2023,2024,2025,2026,2027,2034,2035,2808,2930,3294,3295,3296,3297,3443,3893,4780,4942,5173,5752,5783,6533,7159,7270,7742,8063,8065,8707,8710,8809,9283,9432,9433,9434,9435,9436,9437,9438,10160,10190,10685,10926,11125,11127,11425,11866,12743,13001,13017,13167,13270,13271,13774,13922,14146,14252,14373,14796,14945,15051,15053,15236,15405,15610,15734,15913,16269,16312,16807,16924,16925,16926,16927,16928,16951,16952,16953,16954,16957,16962,16969,16990,16991,16995,16996,16997,16998,16999,17022,17023,17024,17025,17154,17246,17511,17793,17797,17815,18525,18654,18990,20092,20176,20179,20336,20337,20566,21618,21619,21632,21837,21848,21882,22384,22599,22781,23022,23101,23497,23795,23805,23975,24133,25182,25550,25829,27559,28576,28714,29092,29264,29266,30749,30763,31026,31156,32178,33317,33739,35046,35096,35367,36142,36199,36567,37896,39777,43941,44035,46356,48095,48392,50017,50264,50595,51514,51923,53289,53732,57191,58966,63721,64671,64981,67611,75811,78921,82881,85196,95026,95941,112051,114241,116281,117671,120716,123781,126581,131221,131391,132141,137581,139011,144346,145326,151456,3385,9090,13354,6702,39677,95536,19,622,852,854,928,929,941,946,947,988,30740,30751,30764,30769,30770,30771,30772,30777,30778,31028,31055,31121,32304,32347,33589,34005,52886,52889,33472,55801,73801,30968,30969,31034,31040,31071,31102,32055,34769,52066,111626,120391,129081,49713,145371,5981,74011,114236,126136,31081,33483,153221,23538,30093,1606,15610,2850,46543,22262,44177,34694,15660,12658,16064,10092,30952,30974,31001,160526,546,648,940,941,944,945,206,235,647,860,863,905)


GROUP BY o.uid
ORDER BY o.uid;


DROP TEMPORARY TABLE IF EXISTS tmp_table_free_trial_2purchase;
CREATE TEMPORARY TABLE tmp_table_free_trial_2purchase
(
  id int(10) NOT NULL AUTO_INCREMENT COMMENT 'Unique id',
  user_id int(10) unsigned NOT NULL COMMENT 'drupal user id',
  mail varchar(254) NOT NULL COMMENT 'email',
  order_id int(10) unsigned NOT NULL COMMENT 'order id',
  total_purchased_order_count int(10) NOT NULL COMMENT 'counts for that day',
  date_first_ft varchar(13) DEFAULT '0' COMMENT 'first free trial date',
  first_purchase_date varchar(13) DEFAULT '0' COMMENT 'first purchase date',
  ft_time_diff bigint(21) COMMENT 'diff in times',
  ft_time_group varchar(10) COMMENT 'time group',
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`),
  KEY `user_id` (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 COMMENT='all FT orders daily';

INSERT INTO tmp_table_free_trial_2purchase (
  user_id,
  mail,
  order_id,
  total_purchased_order_count,
  first_purchase_date,
  date_first_ft,
  ft_time_diff,
  ft_time_group
)

SELECT
	u.uid AS user_id,
	u.mail AS mail,
    o.order_id AS order_id,
	COUNT(o.order_id) AS total_purchased_order_count,
    MIN(DATE(FROM_UNIXTIME(trans.changed))) AS first_purchase_date,
	ftu.date_first_ft,
	TIMESTAMPDIFF(DAY, ftu.date_first_ft,	FROM_UNIXTIME(trans.changed)) AS ft_timediff,
    IF((TIMESTAMPDIFF(DAY,ftu.date_first_ft, MIN(FROM_UNIXTIME(trans.changed))) BETWEEN 0 AND 7), '1st Week',
    IF((TIMESTAMPDIFF(DAY,ftu.date_first_ft, MIN(FROM_UNIXTIME(trans.changed))) BETWEEN 8 AND 14),'2nd Week',
	IF((TIMESTAMPDIFF(DAY,ftu.date_first_ft, MIN(FROM_UNIXTIME(trans.changed))) BETWEEN 15 AND 21),'3rd Week',
	IF((TIMESTAMPDIFF(DAY,ftu.date_first_ft, MIN(FROM_UNIXTIME(trans.changed))) BETWEEN 22 AND 31),'4th Week','5th Week +')))) AS ft_time_group

FROM commerce_order o

LEFT JOIN users u ON o.uid = u.uid

LEFT JOIN field_data_commerce_customer_billing billing_prof ON (o.order_id = billing_prof.entity_id) AND (billing_prof.entity_type = 'commerce_order')
LEFT JOIN commerce_customer_profile prof ON (prof.profile_id = billing_prof.commerce_customer_billing_profile_id) AND (prof.type = 'billing')
LEFT JOIN field_data_commerce_customer_address address ON (address.entity_id = prof.profile_id) AND (address.entity_type = 'commerce_customer_profile')
LEFT JOIN commerce_payment_transaction trans ON (trans.order_id = o.order_id)
LEFT JOIN tmp_table_free_trial_users ftu ON (ftu.user_id = o.uid)

WHERE timestampdiff(DAY,ftu.date_first_ft, CAST(from_unixtime(trans.changed) as date)) >= 0
    # Free trial condition
	AND NOT EXISTS (
	      SELECT li.order_id
	      FROM commerce_line_item li
	      WHERE line_item_label = "FREETRIAL-BUNDLE" AND li.order_id = o.order_id
	) AND trans.payment_method IN ( "rcpar_partners_payment_freetrial", "enroll_flow_uworld_payment" )

	AND o.status IN ('Pending','Processing','completed','shipped')
	AND u.uid NOT IN (0,2625,4035,24768,114306,114521,1,37,392,406,649,650,878,899,925,1159,2023,2024,2025,2026,2027,2034,2035,2808,2930,3294,3295,3296,3297,3443,3893,4780,4942,5173,5752,5783,6533,7159,7270,7742,8063,8065,8707,8710,8809,9283,9432,9433,9434,9435,9436,9437,9438,10160,10190,10685,10926,11125,11127,11425,11866,12743,13001,13017,13167,13270,13271,13774,13922,14146,14252,14373,14796,14945,15051,15053,15236,15405,15610,15734,15913,16269,16312,16807,16924,16925,16926,16927,16928,16951,16952,16953,16954,16957,16962,16969,16990,16991,16995,16996,16997,16998,16999,17022,17023,17024,17025,17154,17246,17511,17793,17797,17815,18525,18654,18990,20092,20176,20179,20336,20337,20566,21618,21619,21632,21837,21848,21882,22384,22599,22781,23022,23101,23497,23795,23805,23975,24133,25182,25550,25829,27559,28576,28714,29092,29264,29266,30749,30763,31026,31156,32178,33317,33739,35046,35096,35367,36142,36199,36567,37896,39777,43941,44035,46356,48095,48392,50017,50264,50595,51514,51923,53289,53732,57191,58966,63721,64671,64981,67611,75811,78921,82881,85196,95026,95941,112051,114241,116281,117671,120716,123781,126581,131221,131391,132141,137581,139011,144346,145326,151456,3385,9090,13354,6702,39677,95536,19,622,852,854,928,929,941,946,947,988,30740,30751,30764,30769,30770,30771,30772,30777,30778,31028,31055,31121,32304,32347,33589,34005,52886,52889,33472,55801,73801,30968,30969,31034,31040,31071,31102,32055,34769,52066,111626,120391,129081,49713,145371,5981,74011,114236,126136,31081,33483,153221,23538,30093,1606,15610,2850,46543,22262,44177,34694,15660,12658,16064,10092,30952,30974,31001,160526,546,648,940,941,944,945,206,235,647,860,863,905)

GROUP BY u.uid
ORDER BY u.uid;

# Originally, we were obtaining this information from the query below.
# However, at some point the query started to take a really long time trying to figure out how to resolve
# (on the "statistics" phase, when running a "explain" command on mysql for that query)
# causing the query to take a really long time to finish.
# Moving some joins outside that query (to the orders_fields_data temporary table) seems to resolve the issue
# and now the query takes a relatively short time to complete.
DROP TEMPORARY TABLE IF EXISTS orders_fields_data;
CREATE TEMPORARY TABLE orders_fields_data
(
order_id INT,
graduated varchar(255),
grad_date date DEFAULT NULL ,
grad_year varchar(255),
grad_month varchar(255),
country varchar(255),
state varchar(255),
st varchar(255),
market varchar(255),
region varchar(255),
zip varchar(255),
ship_state varchar(255),
ship_zip varchar(255),

INDEX order_id (order_id)
);

INSERT INTO orders_fields_data (
  order_id,
  graduated,
  grad_date,
  grad_year,
  grad_month,
  country,
  state,
  st,
  market,
  region,
  zip,
  ship_state,
  ship_zip
)
SELECT
  o.order_id,
  IF(field_did_you_graduate_college_value = 1, "Graduated", "Not Graduated") AS graduated,
  DATE(graddate.field_graduation_month_and_year_value) as grad_date,
  YEAR(graddate.field_graduation_month_and_year_value) as grad_year,
  MONTH(graddate.field_graduation_month_and_year_value) as grad_month,
  country_olaf.COUNTRY AS country,
  IF(state_olaf.State is null, 'International', state_olaf.State) AS state,
  IF(state_olaf.State is null, 'Intl', state_olaf.`State Abbrev`) AS st,
  IF(territory_olaf.Market is null, "International", territory_olaf.Market) AS market,
  IF(territory_olaf.Region is null, "International", territory_olaf.Region) AS region,
  address.commerce_customer_address_postal_code AS zip,
  IF(shipstate.State is null, 'International', shipstate.State) AS ship_state,
  shipaddress.commerce_customer_address_postal_code AS ship_zip

 FROM commerce_order o

INNER JOIN commerce_payment_transaction trans ON trans.order_id = o.order_id

LEFT JOIN users u ON o.uid = u.uid

LEFT JOIN field_data_commerce_customer_billing billing_prof ON o.order_id = billing_prof.entity_id AND billing_prof.entity_type = 'commerce_order'
LEFT JOIN commerce_customer_profile prof ON prof.profile_id = billing_prof.commerce_customer_billing_profile_id AND prof.type = 'billing'
LEFT JOIN field_data_commerce_customer_address address ON address.entity_id = prof.profile_id AND address.entity_type = 'commerce_customer_profile'


LEFT JOIN field_data_commerce_customer_shipping shipping_prof ON o.order_id = shipping_prof.entity_id AND shipping_prof.entity_type = 'commerce_order'
LEFT JOIN commerce_customer_profile sprof ON sprof.profile_id = shipping_prof.commerce_customer_shipping_profile_id AND sprof.type = 'shipping'
LEFT JOIN field_data_commerce_customer_address shipaddress ON shipaddress.entity_id = sprof.profile_id AND shipaddress.entity_type = 'commerce_customer_profile'


LEFT JOIN field_data_field_did_you_graduate_college grad ON grad.entity_type = 'user' AND grad.entity_id = u.uid
LEFT JOIN field_data_field_graduation_month_and_year graddate ON graddate.entity_id = o.uid

LEFT JOIN country_olaf ON address.commerce_customer_address_country = country_olaf.`A2 (ISO)`
LEFT JOIN state_olaf ON address.commerce_customer_address_administrative_area = state_olaf.`State Abbrev`
LEFT JOIN state_olaf shipstate ON shipaddress.commerce_customer_address_administrative_area = shipstate.`State Abbrev`
LEFT JOIN territory_olaf ON address.commerce_customer_address_administrative_area = territory_olaf.State





WHERE trans.status LIKE 'Success' AND
  u.uid NOT IN (0,2625,4035,77631,175056,34289,176716,24768,114306,114521,1,37,392,406,649,650,878,899,925,1159,2023,2024,2025,2026,2027,2034,2035,2808,2930,3294,3295,3296,3297,3443,3893,4780,4942,5173,5752,5783,6533,7159,7270,7742,8063,8065,8707,8710,8809,9283,9432,9433,9434,9435,9436,9437,9438,10160,10190,10685,10926,11125,11127,11425,11866,12743,13001,13017,13167,13270,13271,13774,13922,14146,14252,14373,14796,14945,15051,15053,15236,15405,15610,15734,15913,16269,16312,16807,16924,16925,16926,16927,16928,16951,16952,16953,16954,16957,16962,16969,16990,16991,16995,16996,16997,16998,16999,17022,17023,17024,17025,17154,17246,17511,17793,17797,17815,18525,18654,18990,20092,20176,20179,20336,20337,20566,21618,21619,21632,21837,21848,21882,22384,22599,22781,23022,23101,23497,23795,23805,23975,24133,25182,25550,25829,27559,28576,28714,29092,29264,29266,30749,30763,31026,31156,32178,33317,33739,35046,35096,35367,36142,36199,36567,37896,39777,43941,44035,46356,48095,48392,50017,50264,50595,51514,51923,53289,53732,57191,58966,63721,64671,64981,67611,75811,78921,82881,85196,95026,95941,112051,114241,116281,117671,120716,123781,126581,131221,131391,132141,137581,139011,144346,145326,151456,3385,9090,13354,6702,39677,95536,19,622,852,854,928,929,941,946,947,988,30740,30751,30764,30769,30770,30771,30772,30777,30778,31028,31055,31121,32304,32347,33589,34005,52886,52889,33472,55801,73801,30968,30969,31034,31040,31071,31102,32055,34769,52066,111626,120391,129081,49713,145371,5981,74011,114236,126136,31081,33483,153221,23538,30093,1606,15610,2850,46543,22262,44177,34694,15660,12658,16064,10092,30952,30974,31001,160526,546,648,940,941,944,945,206,235,647,860,863,905)
#REMOVE_ON_FULL_EXPORT#
 AND ( trans.changed >= '#FROM_DATE#' OR o.created >= '#FROM_DATE#' OR o.changed >= '#FROM_DATE#' )
#@REMOVE_ON_FULL_EXPORT#
;

# Main query starts
SELECT
  u.uid AS user_id,
  u.mail,
  o.order_id AS order_id,
  trans.transaction_id AS transaction_id,
  FROM_UNIXTIME(trans.created, '%Y-%m-%d') AS create_date,
  YEAR(FROM_UNIXTIME(trans.created)) AS create_year,
  MONTH(FROM_UNIXTIME(trans.created)) AS create_month,
  DAY(FROM_UNIXTIME(trans.created)) AS create_day,
  o_data.graduated,
  o_data.grad_date,
  o_data.grad_year,
  o_data.grad_month,
  o_data.country,
  o_data.state,
  o_data.st,
  o_data.market,
  o_data.region,
  o_data.zip,
  o_data.ship_state,
  o_data.ship_zip,

  partner.nid AS partner_nid,
  partner.title AS partner_title,
  field_intacct_customer_id_value AS intacct,
  cs.field_college_state_tid AS college_state_tid,
  IFNULL(sfa.title, fcsd.field_college_name_value) AS college_name ,
  csl.field_college_state_list_value as college_state_abbrv,
  so.State as college_state,
  IF(cct.label is null, "No Promotion", cct.label) AS promotion_type,
  IF(fdccc.commerce_coupon_code_value is null, "No Promotion", fdccc.commerce_coupon_code_value) AS promotion,
  IF(fdfd.field_description_value is null, "", fdfd.field_description_value) AS promo_description,
  ftu.first_order_id AS free_trial,
  ft2p.ft_time_diff,
  ft2p.ft_time_group,

  IF (
    uw_oid.field_uw_order_id_value IS NULL,
    CASE concat_ws('-', pt.field_partner_type_value, trans.payment_method, pbt.field_billing_type_value)
        when ('1-rcpar_partners_payment_freetrial-freetrial') then 'B2B'
        when ('1-rcpar_partners_payment-freetrial') then 'B2B'
        when ('1-rcpar_partners_payment-directbill') then 'B2B'
        when ('1-authnet_aim-creditcard') then 'B2B'
        when ('1-rcpar_partners_payment-creditcard') then 'B2B'
        when ('1-rcpar_partners_payment_affirm') then 'B2B'
        when ('1-affirm-creditcard') then 'B2B'
        when ('2-authnet_aim-freetrial') then 'B2C'
        when ('2-rcpar_partners_payment_freetrial-freetrial') then 'B2C'
        when ('2-authnet_aim-freetrial') then 'B2C'
        when ('3-rcpar_partners_payment_freetrial-freetrial') then 'B2B'
        when ('3-rcpar_partners_payment-freetrial') then 'B2B'
        when ('3-rcpar_partners_payment-directbill') then 'B2B'
        when ('3-authnet_aim-creditcard') then 'B2B'
        when ('3-rcpar_partners_payment-creditcard') then 'B2B'
        when ('3-rcpar_partners_payment_affirm') then 'B2B'
        when ('3-affirm-creditcard') then 'B2B'
        when ('3-rcpar_partners_payment_prepaid_debit-prepaid') then 'B2B PPD DB'
        when ('3-rcpar_partners_payment_prepaid_credit-prepaid') then 'B2B PPD CR'
        when ('4-rcpar_partners_payment_freetrial-freetrial') then 'B2C'
        when ('4-rcpar_partners_payment_freetrial') then 'B2C'
        when ('4-rcpar_partners_payment-directbill') then 'B2C'
        when ('4-authnet_aim-creditcard') then 'B2C'
        when ('4-rcpar_partners_payment-creditcard') then 'B2C'
        when ('4-rcpar_partners_payment_affirm') then 'B2C'
        else 'B2C'
    END,
    IF (uw_pn.field_uw_partner_name_value LIKE 'UWorld%' OR uw_pn.field_uw_partner_name_value IS NULL, 'B2C', 'B2B' )
  ) AS channel,

  CASE concat_ws('-', pt.field_partner_type_value, trans.payment_method, pbt.field_billing_type_value)
    when ('1-rcpar_partners_payment_freetrial-freetrial') then 'B2B'
    when ('1-rcpar_partners_payment-freetrial') then 'B2B'
    when ('1-rcpar_partners_payment-directbill') then 'B2B'
    when ('1-authnet_aim-creditcard') then 'B2B'
    when ('1-rcpar_partners_payment-creditcard') then 'B2B'
    when ('1-rcpar_partners_payment_affirm') then 'B2B'
    when ('1-affirm-creditcard') then 'B2B'
    when ('2-authnet_aim-freetrial') then 'B2C'
    when ('2-rcpar_partners_payment_freetrial-freetrial') then 'B2C'
    when ('2-authnet_aim-freetrial') then 'B2C'
    when ('3-rcpar_partners_payment_freetrial-freetrial') then 'B2B'
    when ('3-rcpar_partners_payment-freetrial') then 'B2B'
    when ('3-rcpar_partners_payment-directbill') then 'B2B'
    when ('3-authnet_aim-creditcard') then 'B2B'
    when ('3-rcpar_partners_payment-creditcard') then 'B2B'
    when ('3-rcpar_partners_payment_affirm') then 'B2B'
    when ('3-affirm-creditcard') then 'B2B'
    when ('3-rcpar_partners_payment_prepaid_debit-prepaid') then 'B2B'
    when ('3-rcpar_partners_payment_prepaid_credit-prepaid') then 'B2B'
    when ('4-rcpar_partners_payment_freetrial-freetrial') then 'B2B'
    when ('4-rcpar_partners_payment_freetrial') then 'B2C'
    when ('4-rcpar_partners_payment-directbill') then 'B2C'
    when ('4-authnet_aim-creditcard') then 'B2C'
    when ('4-rcpar_partners_payment-creditcard') then 'B2C'
    when ('4-rcpar_partners_payment_affirm') then 'B2C'
    else 'B2C'
  END AS channel3,

  # lead_status_all calculation START
  IF (act.field_rcpar_cp_ic_access_value = 1 AND pt.field_partner_type_value = 3,
    CASE concat_ws('-', pt.field_partner_type_value, pbt.field_billing_type_value)
       when ('3-freetrial') then 'ACT Lead'
       when ('3-directbill') then 'ACTDB Lead'
       when ('3-creditcard') then 'ACTCC Lead'
    END
   ,
    # ELSE
    (
      IF (
          # Free-trial condition
          EXISTS (
              SELECT li.order_id
              FROM commerce_line_item li
              WHERE line_item_label = "FREETRIAL-BUNDLE" AND li.order_id = o.order_id
          ),

          # free trial lead
          'FreeTrial Lead'
          ,
          #uword no free trial
          (
              CASE concat_ws('-',
                             pt.field_partner_type_value,
                             IF(trans.payment_method = 'enroll_flow_uworld_payment',
                                SUBSTRING_INDEX(trans.instance_id, '|', -1), trans.payment_method),
                             pbt.field_billing_type_value
                  )
                  when ('1-rcpar_partners_payment_freetrial-freetrial') then 'FSL Lead'
                  when ('1-rcpar_partners_payment-freetrial') then 'FSL Lead'
                  when ('1-rcpar_partners_payment-directbill') then 'FDB Lead'
                  when ('1-authnet_aim-creditcard') then 'FCC Lead'
                  when ('1-rcpar_partners_payment-creditcard') then 'FCC Lead'
                  when ('1-rcpar_partners_payment_affirm') then 'FFIN Lead'
                  when ('1-affirm-creditcard') then 'FFIN Lead'
                  when ('2-authnet_aim-freetrial') then 'FreeTrial Lead'
                  when ('2-rcpar_partners_payment_freetrial-freetrial') then 'FreeTrial Lead'
                  when ('3-rcpar_partners_payment_freetrial-freetrial') then 'ASL Lead'
                  when ('3-rcpar_partners_payment-freetrial') then 'ASL Lead'
                  when ('3-rcpar_partners_payment-directbill') then 'ADB Lead'
                  when ('3-authnet_aim-creditcard') then 'ACC Lead'
                  when ('3-rcpar_partners_payment-creditcard') then 'ACC Lead'
                  when ('3-rcpar_partners_payment_affirm') then 'AFIN Lead'
                  when ('3-affirm-creditcard') then 'AFIN Lead'
                  when ('4-rcpar_partners_payment_freetrial-freetrial') then 'PAL Lead'
                  when ('4-rcpar_partners_payment_freetrial') then 'PAL Lead'
                  when ('4-rcpar_partners_payment-directbill') then 'PDB Lead'
                  when ('4-authnet_aim-creditcard') then 'PCC Lead'
                  when ('4-rcpar_partners_payment-creditcard') then 'PCC Lead'
                  when ('4-rcpar_partners_payment_affirm') then 'PFIN Lead'
                  ELSE 'Organic'
                  END
          )
          #END uworld no free trial
      )
    )
  )

  AS lead_status_all,
  # lead_status_all calculation END

  CASE
   WHEN (GROUP_CONCAT(package_line.line_item_label) LIKE '%FULL-ELITE-DISold%') then 'Elite'
   WHEN (GROUP_CONCAT(package_line.line_item_label) LIKE '%FULL-ELITE-DIS%') then 'Elite'
   WHEN (GROUP_CONCAT(package_line.line_item_label) LIKE '%FULL-PREM-DIS%') then 'Premier'
   WHEN (GROUP_CONCAT(package_line.line_item_label) LIKE '%FULL-PREM-EFC-DIS%') then 'Premier'
   WHEN (GROUP_CONCAT(package_line.line_item_label) LIKE '%FULL-PREM-AUD-DIS%') then 'Premier'
   WHEN (GROUP_CONCAT(package_line.line_item_label) LIKE '%FULL-DIS%') then 'Select'
   WHEN (GROUP_CONCAT(package_line.line_item_label) LIKE '%FULL-FS-DIS%') then 'Select'
   WHEN (GROUP_CONCAT(package_line.line_item_label) LIKE '%FULL-ST-DIS%') then 'Select'
   WHEN (package_line_singles.line_item_label IN ('AUD','BEC','FAR','REG','AUD-CRAM','BEC-CRAM','FAR-CRAM','REG-CRAM')) then 'Individual'
   ELSE 'Other'
  END AS package,

  GROUP_CONCAT( DISTINCT(
     CASE
      WHEN (bundle_package_line.line_item_label = 'FULL-AUD-DIS') then 'FullAUD'
      WHEN (bundle_package_line.line_item_label = 'FULL-CRAM-DIS') then 'FullCRAM'
      WHEN (bundle_package_line.line_item_label = 'FULL-EFC-DIS') then 'FullEFC'
      WHEN (bundle_package_line.line_item_label = 'FULL-OFF-LEC-DIS') then 'FullOffLect'
      WHEN (bundle_package_line.line_item_label = 'FULL-SSD-DIS') then 'FullSSD'
    END)
   ) AS bundles,

  CASE
    WHEN trans.payment_method = 'authnet_aim' OR
        (trans.payment_method = 'enroll_flow_uworld_payment' AND  trans.instance_id  LIKE '%|authnet_aim' )
        THEN 'CC'

    WHEN trans.payment_method = 'rcpar_partners_payment' OR
        (trans.payment_method = 'enroll_flow_uworld_payment' AND  trans.instance_id  LIKE '%|rcpar_partners_payment')
        THEN 'DB'

    WHEN trans.payment_method = 'affirm' OR
        (trans.payment_method = 'enroll_flow_uworld_payment' AND  trans.instance_id  LIKE '%|affirm' )
        THEN 'Affirm'

    WHEN trans.payment_method = 'rcpar_pay_by_check' OR
         (trans.payment_method = 'enroll_flow_uworld_payment' AND  trans.instance_id  LIKE '%|rcpar_pay_by_check' )
        THEN 'Pay by Check'

    WHEN trans.payment_method = 'rcpar_partners_payment_prepaid_credit' OR
         (trans.payment_method = 'enroll_flow_uworld_payment' AND  trans.instance_id  LIKE '%|rcpar_partners_payment_prepaid_credit' )
        THEN 'PPD CR'

    WHEN trans.payment_method = 'rcpar_partners_payment_prepaid_debit' OR
         (trans.payment_method = 'enroll_flow_uworld_payment' AND  trans.instance_id  LIKE '%|rcpar_partners_payment_prepaid_debit' )
        THEN 'PPD DR'

    WHEN
        # Free trial condition
        EXISTS (
              SELECT li.order_id
              FROM commerce_line_item li
              WHERE line_item_label = "FREETRIAL-BUNDLE" AND li.order_id = o.order_id
        ) AND trans.payment_method IN ( "rcpar_partners_payment_freetrial", "enroll_flow_uworld_payment" )
        THEN 'FT'

    ELSE ''
  END as `payment_method`,

  ROUND(
    (ot.commerce_order_total_amount
     - IFNULL(shipping_line_total.commerce_total_amount, 0)
     - IFNULL(tax_line_total.commerce_total_amount,0)
     # The coupons amounts applied to this order:
     + IFNULL(

       IF(coup.type = 'commerce_coupon_pct',
	 # Percentage coupon
         (ot.commerce_order_total_amount - IFNULL(tax_line_total.commerce_total_amount,0) - IFNULL(shipping_line_total.commerce_total_amount, 0))/(1-commerce_coupon_percent_amount_value/100)
         -
         (ot.commerce_order_total_amount - IFNULL(tax_line_total.commerce_total_amount,0) - IFNULL(shipping_line_total.commerce_total_amount, 0))

          , commerce_coupon_fixed_amount_amount
       )
       ,0)
  )/100, 2) AS sub_total,

  ROUND(
    IFNULL(
       IF(coup.type = 'commerce_coupon_pct',
	 # Percentage coupon
         (ot.commerce_order_total_amount - IFNULL(tax_line_total.commerce_total_amount,0) - IFNULL(shipping_line_total.commerce_total_amount, 0))/(1-commerce_coupon_percent_amount_value/100)
         -
         (ot.commerce_order_total_amount - IFNULL(tax_line_total.commerce_total_amount,0) - IFNULL(shipping_line_total.commerce_total_amount, 0))

          , commerce_coupon_fixed_amount_amount
       )
       ,0)/100
  , 2) as promo_total,
  ROUND(pdisc_line_total.commerce_total_amount/100, 2) as partner_discount_total,
  ROUND(IFNULL(shipping_line_total.commerce_total_amount,0)/100, 2) as shipping,
  ROUND(IFNULL(tax_line_total.commerce_total_amount,0)/100, 2) as taxes,
  ROUND(ot.commerce_order_total_amount/100, 2) AS order_total,
  ROUND(IFNULL(refund_trans.amount/100,0), 2) AS refund_amount,
  IFNULL(FROM_UNIXTIME(refund_trans.created, '%Y-%m-%d'), 0) AS refund_date,

  IF(ot.commerce_order_total_amount + IFNULL(refund_trans.amount,0) - IFNULL(tax_line_total.commerce_total_amount,0) - IFNULL(shipping_line_total.commerce_total_amount, 0) > 0,
  ROUND((ot.commerce_order_total_amount + IFNULL(refund_trans.amount,0) - IFNULL(tax_line_total.commerce_total_amount,0) - IFNULL(shipping_line_total.commerce_total_amount,0))/100, 2), 0)  AS net_order_revenue,

  o.status AS order_status,
  sf_account.id AS account_id,
  sf_account.title AS account_name,
  field_office_location_value AS office_location,
  shipping_line.line_item_label AS shipping_type,
  emp.field_employer_value AS employer,
  uw_oid.field_uw_order_id_value as uw_order_id,
  IFNULL(uw_oid.field_uw_order_id_value, o.order_id) AS order_id_both,
  uw_sfid.field_uw_sf_id_value AS uw_sf_id,
  uw_pn.field_uw_partner_name_value AS uw_partner_name,
  fice.field_fice_code_value as fice,
  opeid.field_opeid_value as opeid


FROM commerce_order o

INNER JOIN orders_fields_data o_data ON o_data.order_id = o.order_id

LEFT JOIN users u ON o.uid = u.uid

LEFT JOIN commerce_payment_transaction trans ON trans.order_id = o.order_id
LEFT JOIN commerce_payment_transaction refund_trans ON refund_trans.order_id = o.order_id AND refund_trans.status LIKE 'success' AND refund_trans.message LIKE '%credited to%'


LEFT JOIN field_data_commerce_order_total ot ON ot.entity_id = o.order_id

LEFT JOIN field_data_commerce_line_items fd_cli ON fd_cli.entity_id = o.order_id AND fd_cli.entity_type = 'commerce_order'

LEFT JOIN commerce_line_item package_line ON package_line.line_item_id = fd_cli.commerce_line_items_line_item_id AND package_line.type = 'product' AND package_line.line_item_label IN ('FULL-DIS','FULL-PREM-DIS','FULL-ELITE-DIS','FULL-PREM-EFC-DIS','FULL-PREM-AUD-DIS','FULL-FS-DIS','FULL-ST-DIS','FULL-ELITE-DISold')
LEFT JOIN commerce_line_item package_line_singles ON package_line_singles.line_item_id = fd_cli.commerce_line_items_line_item_id AND package_line_singles.type = 'product' AND package_line_singles.line_item_label IN ('AUD','BEC','FAR','REG','AUD-CRAM','BEC-CRAM','FAR-CRAM','REG-CRAM')

LEFT JOIN commerce_line_item bundle_package_line ON bundle_package_line.line_item_id = fd_cli.commerce_line_items_line_item_id AND bundle_package_line.type = 'product' AND bundle_package_line.line_item_label IN ('FULL-AUD-DIS','FULL-CRAM-DIS','FULL-EFC-DIS','FULL-OFF-LEC-DIS','FULL-SSD-DIS')


# TODO: Shouldnt we need to join with fd_cli here too!!?

LEFT JOIN commerce_line_item tax_line ON tax_line.line_item_id = (SELECT line_item_id FROM commerce_line_item temp_li WHERE temp_li.order_id = trans.order_id AND temp_li.type = 'avatax' order by line_item_id desc LIMIT 1)
LEFT JOIN field_data_commerce_total tax_line_total ON tax_line_total.entity_type = 'commerce_line_item' AND tax_line_total.entity_id = tax_line.line_item_id

LEFT JOIN commerce_line_item shipping_line ON shipping_line.order_id = o.order_id AND shipping_line.type = 'shipping'
LEFT JOIN field_data_commerce_total shipping_line_total ON shipping_line_total.entity_type = 'commerce_line_item' AND shipping_line_total.entity_id = shipping_line.line_item_id

LEFT JOIN field_data_field_office_location ol ON ol.entity_id = o.order_id AND ol.entity_type = 'commerce_order'

LEFT JOIN commerce_line_item pdisc ON pdisc.line_item_id = (SELECT line_item_id FROM commerce_line_item temp_li WHERE temp_li.order_id = trans.order_id AND temp_li.type = 'product' AND temp_li.line_item_label = 'PARTNER-DIS' order by line_item_id desc LIMIT 1)
LEFT JOIN field_data_commerce_total pdisc_line_total ON pdisc_line_total.entity_type = 'commerce_line_item' AND pdisc_line_total.entity_id = pdisc.line_item_id

LEFT JOIN field_data_commerce_total line_total ON line_total.entity_type = 'commerce_line_item' AND line_total.entity_id = tax_line.line_item_id


LEFT JOIN field_data_field_partner_profile partner_ref ON partner_ref.entity_id = o.order_id AND partner_ref.entity_type = 'commerce_order'
LEFT JOIN node partner ON partner.nid = partner_ref.field_partner_profile_target_id
LEFT JOIN field_data_field_intacct_customer_id intacct ON partner.nid = intacct.entity_id AND intacct.entity_type = 'node'
LEFT JOIN field_data_field_partner_type pt ON pt.entity_id = partner_ref.field_partner_profile_target_id AND pt.entity_type = 'node'
LEFT JOIN field_data_field_billing_type pbt ON pbt.entity_id = partner_ref.field_partner_profile_target_id

LEFT JOIN field_data_field_partner_account fp_acc ON fp_acc.entity_id = partner.nid AND fp_acc.entity_type = 'node'
LEFT JOIN eck_sf_account sf_account ON sf_account.id = fp_acc.field_partner_account_target_id

INNER JOIN field_data_commerce_order_total order_total ON order_total.entity_id = o.order_id AND order_total.entity_type = 'commerce_order'

LEFT JOIN field_data_commerce_coupon_order_reference fdccor ON fdccor.entity_id = o.order_id AND fdccor.entity_type = 'commerce_order'
LEFT JOIN field_data_commerce_coupon_code fdccc ON fdccc.entity_id = fdccor.commerce_coupon_order_reference_target_id
LEFT JOIN commerce_coupon_type cct ON cct.`type` = fdccc.bundle

LEFT join commerce_coupon coup ON coup.coupon_id = fdccor.commerce_coupon_order_reference_target_id
LEFT join field_data_commerce_coupon_percent_amount percent_cpn ON percent_cpn.bundle = coup.type AND percent_cpn.entity_id = coup.coupon_id
LEFT join field_data_commerce_coupon_fixed_amount fixed_amt_cpn ON fixed_amt_cpn.bundle = coup.type AND fixed_amt_cpn.entity_id = coup.coupon_id

LEFT JOIN field_data_field_description fdfd ON fdccc.entity_id = fdfd.entity_id

LEFT JOIN tmp_table_free_trial_2purchase ft2p ON ft2p.user_id = o.uid
LEFT JOIN tmp_table_free_trial_users ftu ON ftu.user_id = o.uid

LEFT JOIN field_data_field_college_name fcsd ON u.uid = fcsd.entity_id AND fcsd.entity_type = 'user'
LEFT JOIN field_data_field_college_sf_account fcsa ON u.uid = fcsa.entity_id and  fcsa.entity_type='user' AND fcsa.bundle='user'
LEFT JOIN eck_sf_account sfa ON sfa.id = fcsa.field_college_sf_account_target_id

LEFT JOIN field_data_field_rcpar_cp_ic_access act ON act.entity_id = partner.nid

LEFT JOIN field_data_field_when_do_you_plan_to_start s ON o.uid = s.entity_id
LEFT JOIN field_data_field_college_state_list csl ON csl.entity_id = u.uid
LEFT JOIN field_data_field_college_state cs ON cs.entity_id = csl.entity_id
LEFT JOIN field_data_field_uw_order_id uw_oid ON o.order_id = uw_oid.entity_id AND uw_oid.entity_type = 'commerce_order'
LEFT JOIN field_data_field_uw_sf_id uw_sfid ON o.order_id = uw_sfid.entity_id AND uw_sfid.entity_type = 'commerce_order'
LEFT JOIN field_data_field_uw_partner_name uw_pn ON o.order_id = uw_pn.entity_id AND uw_pn.entity_type = 'commerce_order'

LEFT JOIN state_olaf so ON csl.field_college_state_list_value = so.`State Abbrev`

LEFT JOIN field_data_field_employer emp ON emp.entity_id = o.order_id

LEFT JOIN field_data_field_fice_code fice ON fice.entity_id = sfa.id AND fice.entity_type = 'sf_account'
LEFT JOIN field_data_field_opeid opeid ON opeid.entity_id = sfa.id AND opeid.entity_type = 'sf_account'

WHERE trans.status LIKE 'Success' AND
  u.uid NOT IN (0,2625,4035,77631,175056,34289,176716,24768,114306,114521,1,37,392,406,649,650,878,899,925,1159,2023,2024,2025,2026,2027,2034,2035,2808,2930,3294,3295,3296,3297,3443,3893,4780,4942,5173,5752,5783,6533,7159,7270,7742,8063,8065,8707,8710,8809,9283,9432,9433,9434,9435,9436,9437,9438,10160,10190,10685,10926,11125,11127,11425,11866,12743,13001,13017,13167,13270,13271,13774,13922,14146,14252,14373,14796,14945,15051,15053,15236,15405,15610,15734,15913,16269,16312,16807,16924,16925,16926,16927,16928,16951,16952,16953,16954,16957,16962,16969,16990,16991,16995,16996,16997,16998,16999,17022,17023,17024,17025,17154,17246,17511,17793,17797,17815,18525,18654,18990,20092,20176,20179,20336,20337,20566,21618,21619,21632,21837,21848,21882,22384,22599,22781,23022,23101,23497,23795,23805,23975,24133,25182,25550,25829,27559,28576,28714,29092,29264,29266,30749,30763,31026,31156,32178,33317,33739,35046,35096,35367,36142,36199,36567,37896,39777,43941,44035,46356,48095,48392,50017,50264,50595,51514,51923,53289,53732,57191,58966,63721,64671,64981,67611,75811,78921,82881,85196,95026,95941,112051,114241,116281,117671,120716,123781,126581,131221,131391,132141,137581,139011,144346,145326,151456,3385,9090,13354,6702,39677,95536,19,622,852,854,928,929,941,946,947,988,30740,30751,30764,30769,30770,30771,30772,30777,30778,31028,31055,31121,32304,32347,33589,34005,52886,52889,33472,55801,73801,30968,30969,31034,31040,31071,31102,32055,34769,52066,111626,120391,129081,49713,145371,5981,74011,114236,126136,31081,33483,153221,23538,30093,1606,15610,2850,46543,22262,44177,34694,15660,12658,16064,10092,30952,30974,31001,160526,546,648,940,941,944,945,206,235,647,860,863,905)

# NOTE: In case you want to comment this filter, you need to comment the same line above on this same file.
#REMOVE_ON_FULL_EXPORT#
AND ( trans.changed >= '#FROM_DATE#' OR o.created >= '#FROM_DATE#' OR o.changed >= '#FROM_DATE#' )
#@REMOVE_ON_FULL_EXPORT#

GROUP BY o.order_id
;

