SELECT topic.nid as topic_nid, term_year.name as year, cs.section, topic.title as topic_title, total_time/1000 as videos_total_duration, total_count as videos_count FROM node topic
INNER JOIN course_stats cs ON cs.type = 'TOPIC' AND topic.nid = cs.nid
INNER JOIN taxonomy_term_data term_year ON term_year.tid = cs.year 
INNER JOIN field_data_field_rcpa_video fd_rcpa_video ON fd_rcpa_video.entity_id = topic.nid AND fd_rcpa_video.entity_type = 'node'
INNER JOIN node v ON v.nid = fd_rcpa_video.field_rcpa_video_target_id
WHERE topic.type = 'rcpa_topic' AND v.type = 'rcpa_video'
GROUP BY cs.nid;




