SET time_zone = '-7:00';


# Helper indexes needed to run this query (created on reporting_general.sql file)
# CREATE INDEX created ON eck_video_history (created, uid, section, percentage_completed);
# CREATE INDEX node_type_uid ON node (type, uid, created);
# CREATE INDEX created2 ON eck_video_history (uid, section, percentage_completed, created);

SELECT uid,
section,
course_type,
COUNT(*) AS videos_watched,
(
  SELECT COUNT(*) FROM eck_video_history WHERE percentage_completed >= '98' AND DATE_FORMAT(FROM_UNIXTIME(created), '%Y%m') = DATE_FORMAT(FROM_UNIXTIME(vh.created), '%Y%m') AND uid = vh.uid AND section = vh.section AND entitlement_product = vh.entitlement_product
) AS videos_completed,
(
   SELECT COUNT(*) FROM node n
   LEFT JOIN field_data_field_topic_reference tr ON tr.entity_id = n.nid AND tr.entity_type = 'node'
   LEFT JOIN field_data_field_course_section cs ON cs.entity_id = tr.field_topic_reference_target_id AND tr.entity_type = 'node'
   LEFT JOIN field_data_field_entitlement_product ent_prod ON ent_prod.entity_id = n.nid AND ent_prod.entity_type = 'node'
   LEFT JOIN taxonomy_term_data td ON td.tid = cs.field_course_section_tid 
   WHERE n.type='topic_notes' AND DATE_FORMAT(FROM_UNIXTIME(created), '%Y%m') = DATE_FORMAT(FROM_UNIXTIME(vh.created), '%Y%m') AND n.uid = vh.uid AND td.name = vh.section AND ent_prod.field_entitlement_product_target_id = vh.entitlement_product
) AS notes,
(
  SELECT COUNT(*) FROM node n
  LEFT JOIN field_data_field_topic_reference tr ON tr.entity_id = n.nid
  LEFT JOIN field_data_field_course_section cs ON cs.entity_id = tr.field_topic_reference_target_id
  LEFT JOIN field_data_field_entitlement_product ent_prod ON ent_prod.entity_id = n.nid AND ent_prod.entity_type = 'node'
  LEFT JOIN taxonomy_term_data td ON td.tid = cs.field_course_section_tid
  WHERE n.type='video_bookmark' AND DATE_FORMAT(FROM_UNIXTIME(created), '%Y%m') = DATE_FORMAT(FROM_UNIXTIME(vh.created), '%Y%m') AND n.uid = vh.uid AND td.name = vh.section AND ent_prod.field_entitlement_product_target_id = vh.entitlement_product
) AS bookmarks,
(
  SELECT COUNT(*) FROM node n
  LEFT JOIN field_data_field_topic_reference tr ON tr.entity_id = n.nid
  LEFT JOIN field_data_field_course_section cs ON cs.entity_id = tr.field_topic_reference_target_id
  LEFT JOIN field_data_field_entitlement_product ent_prod ON ent_prod.entity_id = n.nid AND ent_prod.entity_type = 'node'
  LEFT JOIN taxonomy_term_data td ON td.tid = cs.field_course_section_tid
  WHERE n.type='book_highlight' AND DATE_FORMAT(FROM_UNIXTIME(created), '%Y%m') = DATE_FORMAT(FROM_UNIXTIME(vh.created), '%Y%m') AND n.uid = vh.uid AND td.name = vh.section AND ent_prod.field_entitlement_product_target_id = vh.entitlement_product
) AS highlights,

DATE_FORMAT(FROM_UNIXTIME(vh.created), '%Y%m') AS 'group',

entitlement_product AS entitlement_id,
IFNULL(SUM(TIME_TO_SEC(concat('00:',total_duration))),0) AS video_total_duration,
IFNULL(ROUND(AVG(TIME_TO_SEC(concat('00:',total_duration))), 2),0) AS avg_video_total_duration, 
IFNULL(SUM(last_position)/1000,0) AS watched_time_total, 
IFNULL(SUM(IF(percentage_completed >= '98', last_position, 0))/1000,0) AS watched_time_complete,

MIN(DATE(FROM_UNIXTIME(created, '%Y-%m-%d H:i:s'))) AS first_date,
MAX(DATE(FROM_UNIXTIME(created, '%Y-%m-%d H:i:s'))) AS last_date

FROM eck_video_history vh
WHERE 
1 
#REMOVE_ON_FULL_EXPORT#
 AND vh.created between UNIX_TIMESTAMP('#FROM_DATE#') AND UNIX_TIMESTAMP('#TO_DATE#')
#@REMOVE_ON_FULL_EXPORT#
GROUP BY vh.uid, vh.section, vh.entitlement_product, DATE_FORMAT(FROM_UNIXTIME(vh.created), '%Y%m'), course_type
ORDER BY vh.uid, vh.section DESC;

