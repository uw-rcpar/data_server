SET time_zone = '-7:00';

SELECT
    n.nid,
    n.uid,
    co.order_id,
    date(from_Unixtime(co.created)) as created_date,
    date(from_Unixtime(exp.field_expiration_date_value)) as expiration_date,
    date(from_Unixtime(oexp.field_original_expiration_date_value)) as orig_expiration_date,

    n.status AS status,
    bun.field_bundled_product_value AS bundle,
    ps.field_product_sku_value AS sku,

    tdct.taxonomy_term_data_name AS course_type,

    tdaa.taxonomy_term_data_name AS course_section,
    cd.field_course_duration_value AS default_duration,
    dint.field_default_interval_value AS default_interval,
    de.field_duration_extended_value AS duration_extended,

    xde.field_extended_course_duration_value AS extended_duration,
    eint.field_extended_interval_value AS extended_interval,
    cr.field_content_restricted_value AS content_restricted,
    un.field_unlimited_access_value AS unlimited_access,

    IFNULL(mob.field_mobile_offline_access_value,0) AS mobile_offline_access ,
    hhc.field_hhc_access_value AS hhc_access,
    not IFNULL(fad.field_activation_delayed_value,0) AS cram_active,
    IFNULL(ipq.field_ipq_access_value,0) AS ipq_access,
    pid.field_partner_id_value AS partner_id,

    IFNULL(fad.field_activation_delayed_value, 0) AS activation_delayed,
    sn.field_serial_number_value AS serial_number,
    fainst.field_allowed_installs_value AS allowed_installs,
    GROUP_CONCAT(DISTINCT finsts.field_installs_value SEPARATOR ',') AS install_dates,

    tdev.taxonomy_term_data_name AS exam_version
FROM node n
LEFT JOIN field_data_field_order_id ord on ord.entity_id = n.nid and ord.entity_type = 'node'
LEFT JOIN commerce_order co on ord.field_order_id_value = co.order_id
LEFT JOIN field_data_field_expiration_date exp on exp.entity_id = n.nid
LEFT JOIN field_data_field_course_duration cd on cd.entity_id = n.nid
LEFT JOIN field_data_field_default_interval dint on dint.entity_id = n.nid
LEFT JOIN field_data_field_duration_extended de ON de.entity_id = n.nid
LEFT JOIN field_data_field_extended_course_duration xde ON xde.entity_id = n.nid
LEFT JOIN field_data_field_extended_interval eint on eint.entity_id = n.nid
LEFT JOIN field_data_field_original_expiration_date oexp on oexp.entity_id = n.nid
LEFT JOIN field_data_field_bundled_product bun ON bun.entity_id = n.nid
LEFT JOIN field_data_field_product_sku ps ON ps.entity_id = n.nid
LEFT JOIN field_data_field_course_type_ref ct ON ct.entity_id = n.nid
LEFT JOIN field_data_field_content_restricted cr ON cr.entity_id = n.nid
LEFT JOIN field_data_field_unlimited_access un ON un.entity_id = n.nid
LEFT JOIN field_data_field_mobile_offline_access mob ON mob.entity_id = n.nid
LEFT JOIN field_data_field_hhc_access hhc ON hhc.entity_id = n.nid
LEFT JOIN field_data_field_ipq_access ipq ON ipq.entity_id = n.nid
LEFT JOIN field_data_field_partner_id pid ON pid.entity_id = n.nid
LEFT JOIN field_data_field_exam_version_single_val ev ON ev.entity_id = n.nid

LEFT JOIN field_data_field_activation_delayed fad on fad.entity_id = n.nid
LEFT JOIN field_data_field_serial_number sn on sn.entity_id = n.nid
LEFT JOIN field_data_field_allowed_installs fainst on fainst.entity_id = n.nid
LEFT JOIN field_data_field_installs finsts on finsts.entity_id = n.nid

LEFT JOIN (
    SELECT 
        taxonomy_term_data.name AS taxonomy_term_data_name, 
        taxonomy_term_data.vid AS taxonomy_term_data_vid, 
        taxonomy_term_data.tid AS tid, 
        taxonomy_vocabulary.machine_name AS taxonomy_vocabulary_machine_name, 
        'taxonomy_term' AS field_data_field_fice_code_taxonomy_term_entity_type 
    FROM taxonomy_term_data taxonomy_term_data 
    LEFT JOIN taxonomy_vocabulary taxonomy_vocabulary ON taxonomy_term_data.vid = taxonomy_vocabulary.vid 
    WHERE taxonomy_vocabulary.machine_name IN  ('exam_version') 
) tdev ON tdev.tid = ev.field_exam_version_single_val_tid    

LEFT JOIN (
    SELECT 
        taxonomy_term_data.name AS taxonomy_term_data_name, 
        taxonomy_term_data.vid AS taxonomy_term_data_vid, 
        taxonomy_term_data.tid AS tid, 
        taxonomy_vocabulary.machine_name AS taxonomy_vocabulary_machine_name, 
        'taxonomy_term' AS field_data_field_fice_code_taxonomy_term_entity_type 
    FROM taxonomy_term_data taxonomy_term_data 
    LEFT JOIN taxonomy_vocabulary taxonomy_vocabulary ON taxonomy_term_data.vid = taxonomy_vocabulary.vid 
    WHERE taxonomy_vocabulary.machine_name IN  ('course_type') 
) tdct ON tdct.tid = ct.field_course_type_ref_tid    

LEFT JOIN field_data_field_course_section cs ON cs.entity_id = n.nid    
LEFT JOIN (
    SELECT 
        taxonomy_term_data.name AS taxonomy_term_data_name, 
        taxonomy_term_data.vid AS taxonomy_term_data_vid, 
        taxonomy_term_data.tid AS tid, 
        taxonomy_vocabulary.machine_name AS taxonomy_vocabulary_machine_name, 
        'taxonomy_term' AS field_data_field_fice_code_taxonomy_term_entity_type 
    FROM taxonomy_term_data taxonomy_term_data 
    LEFT JOIN taxonomy_vocabulary taxonomy_vocabulary ON taxonomy_term_data.vid = taxonomy_vocabulary.vid 
    WHERE taxonomy_vocabulary.machine_name IN  ('course_sections') 
) tdaa ON tdaa.tid = cs.field_course_section_tid    

WHERE 
    n.type='user_entitlement_product'
    #REMOVE_ON_FULL_EXPORT# 
     AND ( n.changed >= '#FROM_DATE#' OR n.created >= '#FROM_DATE#' )
    #@REMOVE_ON_FULL_EXPORT#
GROUP BY n.nid;

