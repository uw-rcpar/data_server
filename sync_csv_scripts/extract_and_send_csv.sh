#!/bin/bash

echo "E&S - Starting Extract and send processes"

# Importing Constants
DIR="${BASH_SOURCE%/*}"
if [[ ! -d "$DIR" ]]; then DIR="$PWD"; fi

# Set default variables like DAYS_AGO=3
source "$DIR/exporting_commons.sh"

# The number of days ago can be passed as argument - Default is 3
if [[ "$1" != "" ]]; then
    DAYS_AGO="$1"
fi

# 24hs from now
FROM_TIME=`date +'%s' --date="$DAYS_AGO days ago"`

echo "E&S custom date: $DAYS_AGO days ago"


# general.sql contains queries that might apply to more than one of the other .sql files.
$ENV drush ${SERVER_ALIAS} sqlc < ${LOCAL_FILES_DIR}general.sql

for i in ${!FILES_SRC[@]}
do
  # Create the csv file.
  echo "E&S - Running ${FILES_SRC[$i]} query"
  $ENV cat ${LOCAL_FILES_DIR}${FILES_SRC[$i]} | $ENV sed "s/#FROM_DATE#/${FROM_TIME}/g" > tmp.sql
  $ENV drush ${SERVER_ALIAS} sqlc < tmp.sql | $ENV sed "s/\"/\\\\\"/g;s/'/\'/;s/\t/\",\"/g;s/^/\"/;s/$/\"/;s/\n//g" > ${LOCAL_FILES_DIR}${FILES_DST[$i]}
  rm tmp.sql
  echo "E&S - Creation Completed: ${FILES_DST[$i]}"

  # UW datawarehouse server:
  $ENV scp ${LOCAL_FILES_DIR}${FILES_DST[$i]} $UW_SSH_USER@${UW_DST_SRV}:${UW_DST_PATH}${FILES_DST[$i]}

  rm ${LOCAL_FILES_DIR}${FILES_DST[$i]}
  echo "E&S - Send Completed: ${FILES_DST[$i]}"
done

# Let's process the monthly scripts (if we need to).

FILES_MONTHLY_SRC=( "reporting_lectures_monthly.sql")
FILES_MONTHLY_DST=( "LectureStatistic.csv" ) 

if [ `date +'%d'` = "01" ]
then
  echo "E&S - Running monthly queries."
  FROM_TIME_MONTHLY=`date -d "\`date +%Y%m01\` -1 day" +%Y%m01`
  TO_TIME_MONTHLY=`date -d "\`date +%Y%m01\` -1 day" +%Y%m%d`
  for i in ${!FILES_MONTHLY_SRC[@]}
  do
    # Create the csv file.
    echo "E&S - Running ${FILES_MONTHLY_SRC[$i]} MONTHLY query"
    $ENV cat ${LOCAL_FILES_DIR}${FILES_MONTHLY_SRC[$i]} | $ENV sed "s/#FROM_DATE#/${FROM_TIME_MONTHLY}/g" | $ENV sed "s/#TO_DATE#/${TO_TIME_MONTHLY}/g" > tmp.sql
    $ENV drush ${SERVER_ALIAS} sqlc < tmp.sql | $ENV sed "s/\"/\\\\\"/g;s/'/\'/;s/\t/\",\"/g;s/^/\"/;s/$/\"/;s/\n//g" > ${LOCAL_FILES_DIR}${FILES_MONTHLY_DST[$i]}
    rm tmp.sql
    echo "E&S - Creation Completed: ${FILES_MONTHLY_DST[$i]}"

    # UW datawarehouse server:
    $ENV scp ${LOCAL_FILES_DIR}${FILES_MONTHLY_DST[$i]} $UW_SSH_USER@${UW_DST_SRV}:${UW_DST_PATH}${FILES_MONTHLY_DST[$i]}

    rm ${LOCAL_FILES_DIR}${FILES_MONTHLY_DST[$i]}
    echo "E&S - Send Completed: ${FILES_MONTHLY_DST[$i]}"
  done
fi

# Now the Data analisis helper csv
echo "E&S - Running product_revenue_report report"
#$ENV drush ${SERVER_ALIAS} product_revenue_report 0 0 ${FROM_TIME} ${LOCAL_FILES_DIR}LineItemRevenue.csv
$ENV drush ${SERVER_ALIAS} item_lines_with_revenue_report_csv ${FROM_TIME} ${LOCAL_FILES_DIR}OrderLine.csv

echo "E&S - Copying product_revenue_report csv file to server"
$ENV scp ${LOCAL_FILES_DIR}OrderLine.csv $UW_SSH_USER@${UW_DST_SRV}:${UW_DST_PATH}OrderLine.csv

echo "E&S - Extract and send processes finished"
