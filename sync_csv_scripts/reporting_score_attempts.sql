SET time_zone = '-7:00';

DROP TEMPORARY TABLE IF EXISTS scores_tmp;
CREATE TEMPORARY TABLE scores_tmp (
  `exam_scores_id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'The primary identifier for a(n) exam_scores.',
  `attempt` int(11) NOT NULL DEFAULT '0' COMMENT 'Integer',
  `uid` int(11) NOT NULL DEFAULT '0' COMMENT 'Integer',
  `score_entered` int(11) NOT NULL DEFAULT '0' COMMENT 'Integer',
  `exam_taken` int(11) NOT NULL DEFAULT '0' COMMENT 'Integer',
  `exam_year` varchar(255) NOT NULL DEFAULT '' COMMENT 'Text',
  `score` float NOT NULL DEFAULT '0' COMMENT 'Decimal',
  `pass_fail` varchar(255) NOT NULL DEFAULT '' COMMENT 'The bundle of the entity',
  `type` varchar(255) NOT NULL DEFAULT '' COMMENT 'The bundle of the entity',
  `created` int(11) NOT NULL DEFAULT '0' COMMENT 'Integer',
  `changed` int(11) NOT NULL DEFAULT '0' COMMENT 'Integer',
  `section` varchar(255) NOT NULL DEFAULT '' COMMENT 'Text',
  `country` varchar(255) NOT NULL DEFAULT '' COMMENT 'Text',
  `state` varchar(255) NOT NULL DEFAULT '' COMMENT 'Text',
  `st` varchar(255) NOT NULL DEFAULT '' COMMENT 'Text',
  `market` varchar(255) NOT NULL DEFAULT '' COMMENT 'Text',
  `region` varchar(255) NOT NULL DEFAULT '' COMMENT 'Text',
  `us_college` int(11) NOT NULL DEFAULT '0' COMMENT 'Integer',
  `campus_state` varchar(255) NOT NULL DEFAULT '' COMMENT 'Text',
  `college_name` varchar(255) NOT NULL DEFAULT '' COMMENT 'Text',
  `exam_type` varchar(255) NOT NULL DEFAULT '' COMMENT 'Text',
  PRIMARY KEY (`exam_scores_id`)
);

INSERT INTO scores_tmp(
`exam_scores_id`,
`attempt`,
`uid`,
`score_entered`,
`exam_taken`,
`exam_year`,
`score`,
`pass_fail`,
`section`,
`country`,
`state`,
`st`,
`market`,
`region`,
`us_college`,
`campus_state`,
`college_name`,
`exam_type`
)
SELECT 
  `eck_exam_scores`.`id` AS `exam_scores_id`,
  `eck_exam_scores`.`attempt` AS `attempt`,
  `eck_exam_scores`.`uid` AS `uid`,
  cast(from_unixtime(`eck_exam_scores`.`created`) as date) AS `score_entered`,
  cast(from_unixtime(`eck_exam_scores`.`exam_date`) as date) AS `exam_taken`,
  year(from_unixtime(`eck_exam_scores`.`exam_date`)) AS `exam_year`,
  `eck_exam_scores`.`score` AS `score`,
  if((`eck_exam_scores`.`score` > 74),'Pass','Fail') AS `pass_fail`,
  `eck_exam_scores`.`section` AS `section`,
  `country_olaf`.`COUNTRY` AS `country`,
  if(isnull(`state_olaf`.`State`),'International',`state_olaf`.`State`) AS `state`,
  if(isnull(`state_olaf`.`State`),'Intl',`state_olaf`.`State Abbrev`) AS `st`,
  if(isnull(`territory_olaf`.`Market`),'International',`territory_olaf`.`Market`) AS `market`,
  if(isnull(`territory_olaf`.`Region`),'International',`territory_olaf`.`Region`) AS `region`,
  `did_grad`.`field_did_you_graduate_college_value` AS `us_college`,
  `coll_state_olaf`.`State` AS `campus_state`,
  `t`.`name` AS `college_name`,
  if((from_unixtime(`eck_exam_scores`.`exam_date`) >= '2017-04-01'),'New Exam','Old Exam') AS `exam_type` 
FROM 
    `eck_exam_scores` 
    JOIN `users` `u` on(`u`.`uid` = `eck_exam_scores`.`uid`) 	 
    LEFT JOIN `commerce_order` `o` ON `o`.`order_id` = ( select max(`o2`.`order_id`)  FROM `commerce_order` `o2` WHERE `o2`.`uid` = `u`.`uid` AND `o2`.`status` IN('completed','shipped') )	
    LEFT JOIN `field_data_commerce_customer_billing` `billing_prof` ON `o`.`order_id` = `billing_prof`.`entity_id` AND `billing_prof`.`entity_type` = 'commerce_order'       
    LEFT JOIN `commerce_customer_profile` `prof` ON `prof`.`profile_id` = `billing_prof`.`commerce_customer_billing_profile_id` AND `prof`.`type` = 'billing'
    LEFT JOIN `field_data_commerce_customer_address` `address` ON `address`.`entity_id` = `prof`.`profile_id` AND `address`.`entity_type` = 'commerce_customer_profile'
    LEFT JOIN `country_olaf` ON `address`.`commerce_customer_address_country` = `country_olaf`.`A2 (ISO)`
    LEFT JOIN `state_olaf` ON `address`.`commerce_customer_address_administrative_area` = `state_olaf`.`State Abbrev`
    LEFT JOIN `territory_olaf` ON `address`.`commerce_customer_address_administrative_area` = convert(`territory_olaf`.`State` using utf8)
    LEFT JOIN `field_data_field_did_you_graduate_college` `did_grad` ON `did_grad`.`entity_id` = `u`.`uid` AND `did_grad`.`entity_type` = 'user'
    LEFT JOIN `field_data_field_college_state_list` `coll_state` ON `coll_state`.`entity_id` = `u`.`uid` AND `coll_state`.`entity_type` = 'user'
    LEFT JOIN `state_olaf` `coll_state_olaf` ON `coll_state`.`field_college_state_list_value` = `coll_state_olaf`.`State Abbrev`
    LEFT JOIN `field_data_field_college_state` `coll_name_tid` ON `coll_name_tid`.`entity_id` = `u`.`uid` AND `coll_name_tid`.`entity_type` = 'user'
    LEFT JOIN `taxonomy_term_data` `t` ON `t`.`tid` = `coll_name_tid`.`field_college_state_tid`
WHERE 
      NOT EXISTS
      (
          SELECT 1 FROM (`users_roles` `ur` JOIN `role` `r` ON `r`.`rid` = `ur`.`rid` )
	  WHERE `ur`.`uid` = `u`.`uid` AND `r`.`name` IN('administrator','customer support','editor','field member','IPQ editor','moderator')
      )
      AND from_unixtime(`eck_exam_scores`.`exam_date`) >= '2014-01-01'
      AND EXISTS (SELECT * FROM `eck_exam_scores` sc2 WHERE `eck_exam_scores`.`uid` = sc2.uid 
       #REMOVE_ON_FULL_EXPORT#
       AND (sc2.`created` >= '#FROM_DATE#' OR sc2.`changed` >= '#FROM_DATE#')
       #@REMOVE_ON_FULL_EXPORT#
      )
;

SELECT 
  `uid` AS `uid`,
  cast(min(`exam_taken`) as date) AS `first_exam_taken`,
  cast(max(`exam_taken`) as date) AS `last_exam_taken`,
  round((sum(`score`) / count(`attempt`)),1) AS `avg_score`,
  count(`attempt`) AS `attempts` 
FROM `scores_tmp` 
WHERE (`pass_fail` = 'Pass') group by `uid`;


