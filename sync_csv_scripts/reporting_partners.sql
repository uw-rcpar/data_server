SET time_zone = '-7:00';

SELECT
	n.nid,
	sfacc.salesforce_id AS account_id,
	sfac.title AS account_name,
	n.title AS partner_name,
	pn.field_abbreviated_name_value AS abbreviated_name,
	itd.field_intacct_customer_id_value AS intacct_cid,
	CASE pt.field_partner_type_value
	   when ('1') then 'Firm'
	   when ('2') then 'Normal'
	   when ('3') then 'University'
	   when ('4') then 'Publisher'
	 END AS partner_type,
	pbt.field_billing_type_value AS billing_type,
	CASE concat_ws('-', pt.field_partner_type_value, pbt.field_billing_type_value, act.field_rcpar_cp_ic_access_value)
           when ('1-freetrial-0') then 'FSL Lead'
           when ('1-directbill-0') then 'FDB Lead'
           when ('1-creditcard-0') then 'FCC Lead'
           when ('1-affirm-0') then 'FFIN Lead'
           when ('2-freetrial-0') then 'FreeTrial Lead'
           when ('3-freetrial-0') then 'ASL Lead'
           when ('3-directbill-0') then 'ADB Lead'
           when ('3-creditcard-0') then 'ACC Lead'
           when ('3-affirm-0') then 'AFIN Lead'
           when ('3-freetrial-1') then 'ACT Lead'
           when ('3-directbill-1') then 'ACTDB Lead'
           when ('3-creditcard-1') then 'ACTCC Lead'
           when ('3-affirm-1') then 'ACTFIN Lead'
           when ('4-freetrial-0') then 'PAL Lead'
           when ('4-directbill-0') then 'PDB Lead'
           when ('4-creditcard-0') then 'PCC Lead'
           when ('4-affirm-0') then 'PFIN Lead'
           ELSE 'Organic'
          END AS lead_status,
	pm.field_partner_manager_target_id AS account_executive_uid,
	nb.body_value AS description,
	tm.field_terms_value AS partner_terms,
	act.field_rcpar_cp_ic_access_value AS act_access,
	ac.field_active_partner_value AS monitoring_access,
	ua.field_user_account_target_id AS monitoring_uid,
	acp.field_archive_pid_value AS archive_pid,
	fed.field_fixed_expiration_date_value AS fixed_expiration,

	IF (EXISTS(SELECT * FROM field_data_field_course_sections WHERE entity_type = 'node' AND entity_id = n.nid AND field_course_sections_value = "FULL"), 1, 0) AS course_section_full,
	IF (EXISTS(SELECT * FROM field_data_field_course_sections WHERE entity_type = 'node' AND entity_id = n.nid AND field_course_sections_value = "AUD"), 1, 0) AS course_section_aud,
	IF (EXISTS(SELECT * FROM field_data_field_course_sections WHERE entity_type = 'node' AND entity_id = n.nid AND field_course_sections_value = "BEC"), 1, 0) AS course_section_bec,
	IF (EXISTS(SELECT * FROM field_data_field_course_sections WHERE entity_type = 'node' AND entity_id = n.nid AND field_course_sections_value = "FAR"), 1, 0) AS course_section_far,
	IF (EXISTS(SELECT * FROM field_data_field_course_sections WHERE entity_type = 'node' AND entity_id = n.nid AND field_course_sections_value = "REG"), 1, 0) AS course_section_reg,

	ppp.field_per_part_pricing_options_value AS general_discount,
	ap.field_aud_price_value AS aud_discount,
	bp.field_bec_price_value AS bec_discount,
	fp.field_far_price_value AS far_discount,
	rp.field_reg_price_value AS reg_discount,
	
	IF (EXISTS(SELECT * FROM field_data_field_bundle_package WHERE entity_type = 'node' AND entity_id = n.nid AND field_bundle_package_value = "6month"), 1, 0) AS bundled_6m_ext,
	IF (EXISTS(SELECT * FROM field_data_field_bundle_package WHERE entity_type = 'node' AND entity_id = n.nid AND field_bundle_package_value = "offline"), 1, 0) AS bundled_offline_full,
	IF (EXISTS(SELECT * FROM field_data_field_bundle_package WHERE entity_type = 'node' AND entity_id = n.nid AND field_bundle_package_value = "mobile"), 1, 0) AS bundled_mobile_download,
	IF (EXISTS(SELECT * FROM field_data_field_bundle_package WHERE entity_type = 'node' AND entity_id = n.nid AND field_bundle_package_value = "audio"), 1, 0) AS bundled_audio_full,
	IF (EXISTS(SELECT * FROM field_data_field_bundle_package WHERE entity_type = 'node' AND entity_id = n.nid AND field_bundle_package_value = "flash"), 1, 0) AS bundled_flashcard_full,
	IF (EXISTS(SELECT * FROM field_data_field_bundle_package WHERE entity_type = 'node' AND entity_id = n.nid AND field_bundle_package_value = "cram"), 1, 0) AS bundled_cram_full,
	IF (EXISTS(SELECT * FROM field_data_field_bundle_package WHERE entity_type = 'node' AND entity_id = n.nid AND field_bundle_package_value = "crampart"), 1, 0) AS bundled_cram_part_cert,
	IF (EXISTS(SELECT * FROM field_data_field_bundle_package WHERE entity_type = 'node' AND entity_id = n.nid AND field_bundle_package_value = "flashcard"), 1, 0) AS bundled_flashcard_part,
	IF (EXISTS(SELECT * FROM field_data_field_bundle_package WHERE entity_type = 'node' AND entity_id = n.nid AND field_bundle_package_value = "audiopart"), 1, 0) AS bundled_audio_part,
	pfp.field_full_price_value AS bundled_price,
	uev.field_use_email_validation_value AS email_validation,
	st.field_field_sales_tax_value AS exempt_sales_tax,
	sh.field_flat_rate_shipping_value AS flatrate_shipping,
	fsr.field_full_shipping_rate_value AS shipping_rate,
	phr.field_partial_shipping_rate_value AS partial_shipping_rate,
	inter.field_international_value AS international,
	# NOTE: The values of the showprices.field_show_prices_value field are backwards
	CASE showprices.field_show_prices_value
	   when ('1') then 0
	   when ('0') then 1
	  END AS show_price,
	drpv.field_display_retail_package_val_value AS show_retail_price,
	shipflw.field_shipping_flow_value AS shipping_flow,
	n.status AS status,
	n.uid AS author_uid,
	n.status,
	date(from_Unixtime(n.created)) AS created
FROM node n
	LEFT JOIN field_data_field_partner_profile p ON p.entity_id = n.nid
	LEFT JOIN field_data_field_abbreviated_name pn ON pn.entity_id = n.nid
	LEFT JOIN field_data_field_billing_type pbt ON pbt.entity_id = n.nid
	LEFT JOIN field_data_field_partner_type pt ON pt.entity_id = n.nid
	LEFT JOIN field_data_field_full_price pfp ON pfp.entity_id = n.nid
	LEFT JOIN field_data_field_per_part_pricing_options ppp ON ppp.entity_id = n.nid
	LEFT JOIN field_data_field_aud_price ap ON ap.entity_id = n.nid
	LEFT JOIN field_data_field_far_price fp ON fp.entity_id = n.nid
	LEFT JOIN field_data_field_reg_price rp ON rp.entity_id = n.nid
	LEFT JOIN field_data_field_bec_price bp ON bp.entity_id = n.nid
	LEFT JOIN field_data_field_flat_rate_shipping sh ON sh.entity_id = n.nid
	LEFT JOIN field_data_field_field_sales_tax st ON st.entity_id = n.nid

	LEFT JOIN field_data_field_use_email_validation uev ON uev.entity_id = n.nid AND uev.entity_type = 'node'
	LEFT JOIN field_data_field_partial_shipping_rate phr ON phr.entity_id = n.nid AND phr.entity_type = 'node'
	LEFT JOIN field_data_field_international inter ON inter.entity_id = n.nid AND inter.entity_type = 'node'
	LEFT JOIN field_data_field_show_prices showprices ON showprices.entity_id = n.nid AND showprices.entity_type = 'node'
	LEFT JOIN field_data_field_shipping_flow shipflw ON shipflw.entity_id = n.nid AND shipflw.entity_type = 'node'
	LEFT JOIN field_data_field_display_retail_package_val drpv ON drpv.entity_id = n.nid AND drpv.entity_type = 'node'
	LEFT JOIN field_data_field_full_shipping_rate fsr ON fsr.entity_id = n.nid AND fsr.entity_type = 'node'

	LEFT JOIN field_data_field_intacct_customer_id itd ON itd.entity_id = n.nid
	LEFT JOIN field_data_field_partner_manager pm ON pm.entity_id = n.nid
	LEFT JOIN field_data_field_partner_account fpa ON fpa.entity_type = 'node' AND fpa.entity_id = n.nid
	LEFT JOIN eck_sf_account sfac ON sfac.id = fpa.field_partner_account_target_id
	LEFT JOIN salesforce_mapping_object sfacc ON sfacc.entity_type = 'sf_account' AND sfacc.entity_id = sfac.id
	LEFT JOIN field_data_body nb ON nb.entity_id = n.nid
	LEFT JOIN field_data_field_terms tm ON tm.entity_id = n.nid
	LEFT JOIN field_data_field_rcpar_cp_ic_access act ON act.entity_id = n.nid
    LEFT JOIN field_data_field_active_partner ac ON ac.entity_id = n.nid
    LEFT JOIN field_data_field_user_account ua ON ua.entity_id = n.nid
    LEFT JOIN field_data_field_archive_pid acp ON acp.entity_id = n.nid
    LEFT JOIN field_data_field_fixed_expiration_date fed ON fed.entity_id = n.nid
    
WHERE n.type = 'partners'
#REMOVE_ON_FULL_EXPORT#
 AND (n.created >= '#FROM_DATE#' OR n.changed >= '#FROM_DATE#')
#@REMOVE_ON_FULL_EXPORT#
ORDER BY n.nid ASC
;


