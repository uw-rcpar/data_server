set TIMe_zone = '-7:00';

# We need this index on eck_video_history 
# alter table eck_video_history add index created(created);

DROP TEMPORARY TABLE IF EXISTS tmp_entitlements_to_recalculate;
CREATE TEMPORARY TABLE tmp_entitlements_to_recalculate (
  nid int(10) NOT NULL AUTO_INCREMENT COMMENT 'Unique id',
  PRIMARY KEY (`nid`)
);

INSERT INTO tmp_entitlements_to_recalculate ( nid )
SELECT distinct(vh_subq.entitlement_product)
  FROM eck_video_history vh_subq USE INDEX(created)
  WHERE 1
#REMOVE_ON_FULL_EXPORT#
   AND vh_subq.changed >= '#FROM_DATE#' 
#@REMOVE_ON_FULL_EXPORT#
;

SELECT entitlement_product AS entitlement_nid, sum(last_position) as viewed
FROM eck_video_history vh
INNER JOIN tmp_entitlements_to_recalculate ent_to_recalculate ON vh.entitlement_product = ent_to_recalculate.nid 
WHERE 1 
group by entitlement_product;

